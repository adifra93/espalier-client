import { TypedSet, QFamilyId, allCellIdsInColumnIgnoreErrors, packDate, ViewObjectDump, currentViewObject, getReadableModel, CellId, cellIdLastStep, OSValue, CellHandle, cellIdChild, OldOSValue } from "./data";
import { fixmeAny, fixmeAssertNotNull } from "./lib/typescript";
import { JisonLocation, Jison } from "./extra";
import { OSType, getExistingColumn, ColumnId, SpecialType, commonSupertype, NonRootColumnId, typeIsReference, findCommonAncestorPaths, rootColumnId, notifySomeSchemaCache, getColumn, getColumnType } from "./schema";
import { EJSONKeyedMap, JSONKeyedMap, JSONSet, set, Memo, forall, zip } from "./lib/util";
import { effectiveReferenceDisplayColumn, valueToTextIgnoreErrors, tsetToText, parseDate } from "./data-ui";
import { stringifyColumnRef, stringifyType, objectOrBracketedFieldName, columnLogicalChildrenByName } from "./schema-ui";
import { RuntimeError, staticCheck, runtimeCheck, StaticError } from "./misc";
import { fallback } from "./fallback";
import { Layout } from "./operations";
import { EJSON } from 'ejson';
import { assert, assertNotNull } from "./assert";
import { $$ } from "./tablespace";
import _ = require("underscore");

enum SpecialType {
  // The empty type, subtype of all other types, used for literal empty sets, etc.
  // Not allowed for state columns.
  EMPTY = "empty",

  ERROR = "error",

  // Main primitive types
  TEXT = "text",
  NUMBER = "number",
  BOOL = "bool",
  DATE = "date",

  TOKEN = "_token",  // special primitive, should never be user-visible
  UNIT = "_unit",  // primitive, deprecated but still used in ptc
}

// A smidgen of user documentation:
//
// The current representation of formulas follows the style of Lisp in the format
// of JSON.  A formula is an array in which the first element is a string giving
// the name of a supported operation.  The formats and meanings of any remaining
// elements depend on the operation; they may include subformulas.  Each
// operation is briefly documented in "dispatch" below.
//
// A (sub)formula is evaluated with respect to a mapping of variable names
// (strings) to values.  At this time, the value of a variable or a subformula
// (assuming no runtime evaluation error occurs) is always a finite "typed set",
// though some operations deal exclusively with singleton sets.  The formula of
// a formula column is evaluated once for each cell in the parent column, with
// "this" bound to that parent cell, and the returned set gives the values of
// cells to generate in a "family" in the formula column.  Some operations may
// evaluate subformulas with additional variable bindings.

// Support delayed evaluation of messages that might fail to evaluate in cases
// where the assertion passes.
 

  export type Outcome = {result?: TypedSet, error?: string};
  export interface Formula extends Array<fixmeAny> /* :( */ {
    // TODO: Document the circumstances under which these are set.
    loc?: JisonLocation;
    type?: OSType;
    vars?: VarTypes;
    traces?: EJSONKeyedMap<VarValues, Outcome>;
  }

  // For documentation purposes.  What type should we use?
  export type Unvalidated<T> = fixmeAny;

  /**
   * This is a subset of the API provided by the Model class.
   */
  export interface ReadableModel {
    evaluateFamily(qFamilyId: QFamilyId): null /*evaluation error*/ | TypedSet;
    // Call before using AllFamilies.find.
    evaluateAll(): void;
    typecheckColumn(columnId: ColumnId): OSType;
    typecheckAll(): void;
    isTracing?: boolean;
  }

  // Exported for valueToText.
  export function readFamilyForFormula(model: ReadableModel, qFamilyId: QFamilyId) {
    let tset = model.evaluateFamily(qFamilyId);
    if (tset != null) {
      return tset;
    } else {
      // Includes the case of a newly detected cycle.
      // Future: Specifically state that there was a cycle.

      /* Attempt to convert the object to text for the error message.  When
       * readFamilyForFormula is used during sheet evaluation, we want this call
       * to trigger evaluation of the reference display family if necessary, but
       * we don't want the reference display family to show up as a dependency
       * of qFamilyId.  Currently we can get away with this because there is no
       * dependency tracking, but only a mechanism (intended to break cycles)
       * that treats families currently on the stack as erroneous if we hit them
       * while evaluating the valueToTextIgnoreErrors, and those families are in
       * fact erroneous because they have no way to catch the error from
       * qFamilyId.  If any of these assumptions cease to hold, we'll have to
       * come up with a more robust solution here.
       *
       * However, if qFamilyId is itself the reference display family of the
       * object, then valueToTextIgnoreErrors would call
       * readFamilyForFormula(model, qFamilyId) again and we'd get an infinite
       * recursion trying to generate an error message that would ultimately be
       * discarded by valueToTextIgnoreErrors.  Substituting <?> here is correct
       * and sufficient to break this recursion, ensuring that any call from
       * valueToText to readFamilyForFormula can only recurse to evaluateFamily,
       * where it is subject to the usual cycle-breaking mechanism. */
      let parentCol = getExistingColumn(getExistingColumn(qFamilyId.columnId).parent);
      let objectDesc = (effectiveReferenceDisplayColumn(parentCol) == qFamilyId.columnId) ? "<?>" :
        valueToTextIgnoreErrors(model, parentCol._id, qFamilyId.parentCellId);
      throw new RuntimeError(`Reference to column '${stringifyColumnRef([qFamilyId.columnId, true])}' of ` +
        `object '${objectDesc}', which failed to evaluate`);
    }
  }

  function readColumnTypeForFormula(model: ReadableModel, columnId: ColumnId) {
    let type = model.typecheckColumn(columnId);
    staticCheck(type !== SpecialType.ERROR,
      () => `Reference to column '${stringifyColumnRef([columnId, true])}' of unknown type.  ` +
      "Correct its formula or manually specify the type if needed to break a cycle.");
    staticCheck(type !== SpecialType.TOKEN,
      () => `Attempted to access keys of unkeyed object type '${stringifyColumnRef([columnId, false])}'.`);
    return type;
  }

  // TODO: `what` should probably be lazy.
  export function staticExpectType(what: string, actualType: OSType, expectedType: OSType) {
    staticCheck(commonSupertype(actualType, expectedType) === expectedType,
      () => `${what} has type '${stringifyType(actualType)}', wanted '${stringifyType(expectedType)}'`);
  }

  export function singleElement(set: TypedSet) {
    let elements = set.elements();
    runtimeCheck(elements.length === 1, () => "Expected a singleton");
    return elements[0];
  }

  export class FormulaEngine {
    private goUpMemo: Memo<[ColumnId, ColumnId], number>;

    constructor() {
      this.goUpMemo = new Memo<[ColumnId, ColumnId], number>(([sourceColId, targetColId]) => {
        let [upPath, /*unused downPath*/] = findCommonAncestorPaths(sourceColId, targetColId);
        return upPath.length - 1;
      });
    }

    public calcLevelsUp(sourceColId: ColumnId, targetColId: ColumnId) {
      // FIXME: FormulaEngine invalidation on the client isn't implemented.
      // Looks like this has been broken ever since FormulaEngine was added in
      // 8c1efd2.  We never saw any problems because there's currently no way to
      // move an existing column ID in the hierarchy.  (Besides, this only
      // affects client-side formula evaluation, which only occurs in the
      // formula debugger.)  If we wanted to implement it, we could base it on
      // an observer on the Columns collection.
      //
      // ~ Matt 2017-09-27
      if (notifySomeSchemaCache)
        notifySomeSchemaCache();
      return this.goUpMemo.get([sourceColId, targetColId]);
    }

    public invalidateSchemaCache() {
      this.goUpMemo.clear();
    }
  }

  // Result of stringify on OpHandlers.  The conversion to StringifyInfo is the
  // same for all OpHandlers, so we do it in stringifyFormula.
  interface StringifyDispatchResult {
    str: string;
    outerPrecedence: Precedence;
  }
  // Result of stringifySubformula.  The stringify method of an ArgAdapter
  // generally provides a StringifyInfo in some form for each subformula of the
  // argument.
  interface StringifyInfo {
    formula: Formula;
    strFor(outerPrecedence: Precedence): string;
  }

  // Argument adapters to reduce the amount of duplicated work to validate
  // arguments and evaluate subexpressions.
  // Future: Errors should pinpoint the offending subformula.
  interface ArgAdapter<A, T, E, S> {
    // These need to be declared as properties rather than methods to avoid
    // bivariance of parameters, which can hide mistakes.
    //
    // If `validate` completes without error, then `arg` is of type A.
    validate: (vars: VarSet, arg: Unvalidated<A>) => void;
    typecheck: (model: ReadableModel, vars: VarTypes, arg: A) => T;
    evaluate: (model: ReadableModel, vars: VarValues, arg: A) => E;
    stringify: (model: ReadableModel, vars: VarTypes, arg: A) => S;
    getSubformulas?(arg: A): Formula[];
  }
  // "Simple" means validation doesn't depend on `vars`.
  class SimpleValidationOnlyArgAdapter<A> implements ArgAdapter<A, A, A, A> {
    constructor(private validateImpl: (arg: Unvalidated<A>) => void) {}
    validate(vars: VarSet, arg: Unvalidated<A>) { this.validateImpl(arg); }
    typecheck(model: ReadableModel, vars: VarTypes, arg: A) { return arg; }
    evaluate(model: ReadableModel, vars: VarValues, arg: A) { return arg; }
    stringify(model: ReadableModel, vars: VarTypes, arg: A) { return arg; }
  }
  let EagerSubformula: ArgAdapter<Formula, OSType, TypedSet, StringifyInfo> = {
    // Note, we can't eta-contract these because the functions aren't defined at
    // this point in the file.  Better ideas welcome.
    validate: (vars, arg) => {
      validateSubformula(vars, arg);
    },
    typecheck: (model, vars, arg) => typecheckFormula(model, vars, arg),
    evaluate: (model, vars, arg) => evaluateFormula(model, vars, arg),
    stringify: (model, vars, arg) => stringifySubformula(model, vars, arg),
    getSubformulas: (arg) => [arg]
  };
  let OptionalEagerSubformula: ArgAdapter<null | Formula, null | OSType, null | TypedSet, null | StringifyInfo> = {
    validate: (vars, arg) => {
      if (arg != null) {
        validateSubformula(vars, arg);
      }
    },
    typecheck: (model, vars, arg) => arg != null ? typecheckFormula(model, vars, arg) : null,
    evaluate: (model, vars, arg) => arg != null ? evaluateFormula(model, vars, arg) : null,
    stringify: (model, vars, arg) => arg != null ? stringifySubformula(model, vars, arg) : null,
    getSubformulas: (arg) => arg != null ? [arg] : []
  };
  let EagerSubformulaCells: ArgAdapter<Formula, ColumnId, TypedSet, StringifyInfo> = {
    validate: EagerSubformula.validate,
    typecheck: (model, vars, arg) => {
      let type = typecheckFormula(model, vars, arg);
      // stringifyType(type) fails if type is the root.
      if (!typeIsReference(type))  // narrowing
        throw new StaticError(`Expected a set of cells, got set of '${stringifyType(type)}'`);
      return type;
    },
    evaluate: EagerSubformula.evaluate,
    stringify: EagerSubformula.stringify,
    getSubformulas: EagerSubformula.getSubformulas
  };
  let HomogeneousEagerSubformulaList: ArgAdapter<Formula[], OSType, TypedSet[], StringifyInfo[]> = {
    validate: (vars, arg) => {
      staticCheck(_.isArray(arg), () => "Expected a list of subformulas");
      for (let item of arg) {
        validateSubformula(vars, item);
      }
    },
    typecheck: (model, vars, termFmlas) => {
      let typeSoFar: OSType = SpecialType.EMPTY;
      for (let fmla of termFmlas) {
        let termType = typecheckFormula(model, vars, fmla);
        let newType = commonSupertype(typeSoFar, termType);
        staticCheck(newType !== SpecialType.ERROR,
          () => `Mismatched types in list: '${stringifyType(typeSoFar)}' and '${stringifyType(termType)}'`);
        typeSoFar = newType;
      }
      return typeSoFar;
    },
    evaluate: (model, vars, termFmlas) => termFmlas.map((fmla) => evaluateFormula(model, vars, fmla)),
    stringify: (model, vars, termFmlas) => termFmlas.map((fmla) => stringifySubformula(model, vars, fmla)),
    getSubformulas: (termFmlas) => termFmlas
  };
  let LazySubformula: ArgAdapter<Formula, OSType, Formula, StringifyInfo> = {
    validate: EagerSubformula.validate,
    typecheck: EagerSubformula.typecheck,
    evaluate: (model, vars, fmla) => fmla,
    stringify: EagerSubformula.stringify,
    getSubformulas: EagerSubformula.getSubformulas
  };
  // It might be nicer on the users to not require the extra 2-element array in the
  // input, but for now this goes with our framework.
  type FormulaLambda = [/*varName*/ string, /*body*/ Formula];
  let Lambda: ArgAdapter<FormulaLambda, (argType: OSType) => OSType,
    (arg: TypedSet) => TypedSet, (argType: OSType) => [string, StringifyInfo]> = {
    validate: (vars, arg) => {
      staticCheck(_.isArray(arg) && arg.length === 2, () => "Lambda subformula must be a two-element array");
      let [varName, body] = arg;
      staticCheck(_.isString(varName), () => "Bound variable must be a string");
      // Try to save users from themselves.
      staticCheck(!vars.has(varName), () => `Bound variable '${varName}' shadows an outer variable of the same name`);
      let newVars = vars.shallowClone();
      newVars.add(varName);
      validateSubformula(newVars, body);
    },
    typecheck: (model, vars, [varName, body]) =>
      (argType: OSType) => {
        let newVars = vars.shallowClone();
        newVars.set(varName, argType);
        return typecheckFormula(model, newVars, body);
      },
    evaluate: (model, vars, [varName, body]) => {
      // he he he!
      return (arg: TypedSet) => {
        let newVars = vars.shallowClone();
        newVars.set(varName, arg);
        return evaluateFormula(model, newVars, body);
      };
    },
    stringify: (model, vars, [varName, body]) =>
      (argType: OSType): [string, StringifyInfo] => {
        let newVars = vars.shallowClone();
        newVars.set(varName, argType);
        return [stringifyIdent(varName), stringifySubformula(model, newVars, body)];
      },
    getSubformulas: ([varName, body]) => [body]
  };
  let ColumnId: ArgAdapter<ColumnId, ColumnId, ColumnId, ColumnId> = {
    validate: (vars, arg) => {
      staticCheck(_.isString(arg), () => "Column ID must be a string");
    },
    typecheck: (model, vars, arg) => {
      // XXX: Disallow the root column and add a special case for '$'?
      staticCheck(getColumn(arg) != null, () => `No column exists with ID '${arg}'`);
      return arg;
    },
    evaluate: (model, vars, arg) => arg,
    stringify: (model, vars, arg) => arg
  };
  let Type: ArgAdapter<OSType, OSType, OSType, OSType> = {
    validate: (vars, arg) => {
      staticCheck(_.isString(arg), () => "Type must be a string");
      // Future: Reject unknown primitive types
      //if !typeIsReference(arg) ...
    },
    typecheck: (model, vars, arg) => {
      if (typeIsReference(arg)) {
        fixmeAssertNotNull(ColumnId.typecheck)(model, vars, arg);
      }
      return arg;
    },
    evaluate: (model, vars, arg) => arg,
    stringify: (model, vars, arg) => arg
  };
  let WantValues = new SimpleValidationOnlyArgAdapter<boolean>((arg) => {
    staticCheck(_.isBoolean(arg), () => "wantValues must be a boolean");
  });
  let ViewObjectFieldList: ArgAdapter<[ColumnId, Formula][],
    [ColumnId, OSType][], [ColumnId, TypedSet][], (type: ColumnId) => string> = {
    // Note: Duplicates are checked in viewObjectUrl typecheck.
    validate: (vars, arg) => {
      staticCheck(_.isArray(arg), () => "View object field list must be an array");
      for (let entry of arg) {
        staticCheck(_.isArray(entry) && entry.length === 2,
          () => "View object field entry must be a two-element array");
        let [fieldId, expr] = entry;
        staticCheck(_.isString(fieldId), () => "View object field ID must be a string");
        validateSubformula(vars, expr);
      }
    },
    typecheck: (model, vars, list) =>
      // https://github.com/Microsoft/TypeScript/issues/11152
      list.map<[ColumnId, OSType]>(([fieldId, expr]) => [fieldId, typecheckFormula(model, vars, expr)]),
    evaluate: (model, vars, list) =>
      list.map<[ColumnId, TypedSet]>(([fieldId, expr]) => [fieldId, evaluateFormula(model, vars, expr)]),
    stringify: (model, vars, list) =>
      (type: ColumnId) => "{" +
        list.map(([fieldId, expr]) =>
          // Formula is a hack but should be fine.
          stringifyNavigationTarget("down", model, vars, ["lit", type, [[]]], fieldId, null, true) + ": " +
          stringifySubformula(model, vars, expr).strFor(Precedence.LOWEST)
        ).join(", ") +
      "}",
    getSubformulas: (list) => list.map(([fieldId, expr]) => expr)
  };

  function typecheckUp(model: ReadableModel, vars: VarTypes,
    startCellsType: ColumnId, targetColId: ColumnId, wantValues: boolean) {
    let [upPath, downPath] = findCommonAncestorPaths(startCellsType, targetColId);
    staticCheck(downPath.length === 1,
      () => "Navigation from " + startCellsType + " to " + targetColId + " is not up");
    // Enforce the same rule as parsing.  Otherwise, if the startCellsType changes
    // as a result of a change to another formula, users will be surprised when a
    // formula that we didn't allow them to enter evaluates successfully.
    staticCheck(!wantValues || upPath.length === 1,
      () => "Direct navigation up to a key of a parent object is not allowed.");
    return wantValues ? readColumnTypeForFormula(model, targetColId) : targetColId;
  }

  function typecheckDown(model: ReadableModel, vars: VarTypes,
    startCellsType: ColumnId, targetColId: ColumnId, keysType: null | OSType, wantValues: boolean) {
    let targetCol = getExistingColumn(targetColId);
    staticCheck(targetCol.parent === startCellsType,
      () => "Navigation from " + startCellsType + " to " + targetColId + " is not down");
    staticCheck(!targetCol.isViewObjectRoot, () => "Cannot navigate into a view object type.");
    staticCheck(targetCol.userVisible, () => "Cannot navigate to a non-user-visible column.");
    staticCheck(wantValues || targetCol.isObject,
      () => "Target column has no object type to navigate to.");
    if (keysType != null) {
      staticExpectType("Key set", keysType, readColumnTypeForFormula(model, targetColId));
    }
    return wantValues ? readColumnTypeForFormula(model, targetColId) : targetColId;
  }

  // Precondition: cells.type must be a column and not a primitive type,
  // although it may be a non-object column (we can still treat the cells as
  // objects internally).  This should be about the only place we use a TypedSet
  // of cells in a non-object column.
  function cellsToValues(model: ReadableModel, cells: TypedSet) {
    // XXX: Fail on token columns
    let type = model.typecheckColumn(<ColumnId>cells.type);
    return new TypedSet(type, set((<CellId[]>cells.elements()).map((x) => cellIdLastStep(x))));
  }

  function cellsToValuesIfWanted(model: ReadableModel, wantValues: boolean, cellsTset: TypedSet) {
    return wantValues ? cellsToValues(model, cellsTset) : cellsTset;
  }

  function goUp(model: ReadableModel, vars: null | VarValues,
    startCellsTset: TypedSet, targetColId: ColumnId, wantValues: boolean) {
    // XXX: Can we get here with startCellsTset.type == SpecialType.EMPTY?

    // Go up.
    let numIdStepsToDrop = $$.formulaEngine.calcLevelsUp(<ColumnId>startCellsTset.type, targetColId);
    let s = new JSONSet<OSValue>();
    for (let startCellId of (<JSONSet<CellId>>startCellsTset.set).elements()) {
        // Reject an up-navigation on a nonexistent object, even though we know
        // what the result would be, so that broken references cannot violate
        // developer assumptions that if an object has no existing descendants
        // of type T, then up-navigation on a reference of type T cannot return
        // that object.
      new CellHandle({columnId: <ColumnId>startCellsTset.type, cellId: startCellId}).checkObjectExists();

      let targetCellId = startCellId.slice(0, startCellId.length - numIdStepsToDrop);
      // Duplicates are thrown out.  Future: multisets?
      s.add(targetCellId);
    }
    let targetCellsTset = new TypedSet(targetColId, s);

    return cellsToValuesIfWanted(model, wantValues, targetCellsTset);
  }

  function goDown(model: ReadableModel, vars: null | VarValues,
    startCellsTset: TypedSet, targetColId: ColumnId, keysTset: null | TypedSet, wantValues: boolean) {
    // XXX: Can we get here with startCellsTset.type == SpecialType.EMPTY?

    // Go down.
    let targetCellsSet = new JSONSet<OSValue>();
    for (let cellId of (<JSONSet<CellId>>startCellsTset.set).elements()) {
      for (let value of readFamilyForFormula(model, {
        columnId: /*typechecked*/ <NonRootColumnId>targetColId,
        parentCellId: cellId
      }).elements()) {
        if ((keysTset == null) || keysTset.set.has(value)) {
          targetCellsSet.add(cellIdChild(cellId, value));
        }
      }
    }
    let targetCellsTset = new TypedSet(targetColId, targetCellsSet);

    return cellsToValuesIfWanted(model, wantValues, targetCellsTset);
  }

  function annotateNavigationTarget(model: ReadableModel, vars: VarTypes,
    startCellsFmla: null | Formula, targetName: null | string, keysFmla: null | Formula, expectedFmla: Formula) {
    if (targetName == null) {
      return "(unnamed)";
    } else {
      // Future: Distinguish between "the original interpretation is no longer
      // valid" (in which case the formula should fail to typecheck) and "there are
      // multiple possible interpretations including the original" (in which case
      // the formula should still work).
      try {
        let actualFmla = resolveNavigation(model, vars, startCellsFmla, targetName, keysFmla);
        staticCheck(EJSON.equals(actualFmla, expectedFmla),
          () => "Interpreting the concrete formula did not reproduce the existing abstract formula.");
        return stringifyNavigationStep(targetName);
      } catch (e) {
        // Notice: this happens regularly in the client when column
        // type information is wiped
        return stringifyNavigationStep(targetName) + "(problem)";
      }
    }
  }

  // Used for real navigations and revdown.
  function stringifyNavigationStart(startSinfo: StringifyInfo) {
    return (startSinfo.strFor(Precedence.NAV) === "$" ? "$" :
      startSinfo.strFor(Precedence.NAV) === "this" ? "" :
      startSinfo.strFor(Precedence.NAV) + ".");
  }

  // Used for real navigations and for the field of an revdown.
  function stringifyNavigationTarget(direction: "up" | "down", model: ReadableModel, vars: VarTypes,
    startCellsFormula: Formula, targetColumnId: ColumnId, keysSinfo: null | StringifyInfo, wantValues: boolean) {
    let column = getColumn(targetColumnId);
    let keysFmla = keysSinfo != null ? keysSinfo.formula : null;
    if (column == null) {
      return "(deleted)";
    } else {
      // Reconstruct the current subformula.  XXX: Get it a better way?
      let wantFormula: Formula = [direction, startCellsFormula, targetColumnId, wantValues];
      if (direction === "down") {
        wantFormula.splice(3, 0, keysFmla);
      }
      if (!wantValues) {
        return annotateNavigationTarget(model, vars,
          startCellsFormula, objectOrBracketedFieldName(column), keysFmla, wantFormula);
      } else if (direction === "down" && column.isObject) {
        // Special case: when an object type Bar is added to a leaf column foo, we
        // want down navigations "(...).foo" to start displaying as "(...).Bar.foo"
        // without having to rewrite the abstract syntax of affected formulas.
        // Even if the first navigation in the concrete syntax is ambiguous, we
        // know what we meant and should annotate the second navigation
        // accordingly.
        assert(keysFmla == null);  // checked in validate
        let intermediateFormula = wantFormula.slice(0, 3).concat([null, false]);
        return annotateNavigationTarget(model, vars,
          startCellsFormula, objectOrBracketedFieldName(column), null, intermediateFormula) + "." +
          annotateNavigationTarget(model, vars, intermediateFormula, column.fieldName, null, wantFormula);
      } else {
        return annotateNavigationTarget(model, vars, startCellsFormula, column.fieldName, keysFmla, wantFormula);
      }
    }
  }

  function stringifyNavigation(direction: "up" | "down", model: ReadableModel, vars: VarTypes,
    startCellsSinfo: StringifyInfo, targetColumnId: ColumnId, keysSinfo: null | StringifyInfo, wantValues: boolean) {
    return {
      str: stringifyNavigationStart(startCellsSinfo) +
        stringifyNavigationTarget(direction, model, vars,
          startCellsSinfo.formula, targetColumnId, keysSinfo, wantValues) +
        (keysSinfo != null ? `[${keysSinfo.strFor(Precedence.LOWEST)}]` : ""),
      outerPrecedence: Precedence.NAV
    };
  }

  enum Precedence {
    LOWEST = 1,
    OR = LOWEST,
    AND,
    COMPARE,
    PLUS,
    TIMES,
    NEG,
    POW,
    NAV,
    ATOMIC = NAV
  }

  type Associativity = "left" | "right" | null /* implicit but say it anyway */;
  let ASSOCIATIVITY_LEFT: Associativity = "left";
  let ASSOCIATIVITY_RIGHT: Associativity = "right";
  let ASSOCIATIVITY_NONE: Associativity = null;

  function binaryOperationStringify(symbol: string, precedence: Precedence, associativity: Associativity) {
    return (model: ReadableModel, vars: VarTypes, lhsSinfo: StringifyInfo, rhsSinfo: StringifyInfo) => ({
        str: lhsSinfo.strFor(precedence + (associativity !== ASSOCIATIVITY_LEFT ? 1 : 0)) + " " +
          symbol + " " +
          rhsSinfo.strFor(precedence + (associativity !== ASSOCIATIVITY_RIGHT ? 1 : 0)),
        outerPrecedence: precedence
      });
  }

  // Just enough of a generalization of singletonInfixOperator for '+' string
  // concatenation operator that automatically calls toText.
  function infixOperator(symbol: string, precedence: Precedence, associativity: Associativity,
    lhsExpectedType: OSType, rhsExpectedType: OSType, resultType: OSType,
    evaluateFn: (model: ReadableModel, lhsTset: TypedSet, rhsTset: TypedSet) => OSValue,
    paramNames: [string, string] = ["left", "right"]) {
    return makeOpHandler({
      paramNames: paramNames,
      argAdapters: [EagerSubformula, EagerSubformula],
      typecheck: (model, vars, lhsType, rhsType) => {
        staticExpectType(`Left operand of '${symbol}'`, lhsType, lhsExpectedType);
        staticExpectType(`Right operand of '${symbol}'`, rhsType, rhsExpectedType);
        return resultType;
      },
      evaluate: (model, vars, lhsTset, rhsTset) =>
        new TypedSet(resultType, set([evaluateFn(model, lhsTset, rhsTset)])),
      stringify: binaryOperationStringify(symbol, precedence, associativity)
    });
  }

  // Precondition: L, R are the TypeScript types that represent {l,r}hsExpectedType.
  function singletonInfixOperator<L extends OSValue, R extends OSValue>(
    symbol: string, precedence: Precedence, associativity: Associativity,
    lhsExpectedType: OSType, rhsExpectedType: OSType, resultType: OSType,
    evaluateFn: (lhsTset: L, rhsTset: R) => OSValue, paramNames?: [string, string]) {
    function evaluateFn2(model: ReadableModel, lhs: TypedSet, rhs: TypedSet) {
      return evaluateFn(<L>singleElement(lhs), <R>singleElement(rhs));
    }
    return infixOperator(symbol, precedence, associativity,
      lhsExpectedType, rhsExpectedType, resultType, evaluateFn2, paramNames);
  }

  function sameTypeSetsInfixPredicate(symbol: string, precedence: Precedence, associativity: Associativity,
    evaluateFn: (lhs: JSONSet<OSValue>, rhs: JSONSet<OSValue>) => boolean,
    paramNames: [string, string] = ["left", "right"]) {
    return makeOpHandler({
      paramNames: paramNames,
      argAdapters: [EagerSubformula, EagerSubformula],
      typecheck: (model, vars, lhsType, rhsType) => {
        staticCheck(commonSupertype(lhsType, rhsType) !== SpecialType.ERROR,
          () => `Mismatched types to '${symbol}' operator: ` +
          `'${stringifyType(lhsType)}' and '${stringifyType(rhsType)}'`);
        return SpecialType.BOOL;
      },
      evaluate: (model, vars, lhs, rhs) =>
        new TypedSet(SpecialType.BOOL, new JSONSet([evaluateFn(lhs.set, rhs.set)])),
      stringify: binaryOperationStringify(symbol, precedence, associativity)
    });
  }

  // Usage:
  //   overloaded(paramNames,
  //              [[argument-types...], handler],
  //              [[argument-types...], handler], ...)
  // XXX: Do we want to type this statically? ~ Matt 2018-05-16
  function overloaded(operator: string, paramNames: string[],
    ...alternatives: [/*argtypes*/ OSType[], /*handler*/ fixmeAny][]) {
    let arities = alternatives.map((a) => a[0].length);
    let minArity = Math.min(...arities);
    let maxArity = Math.max(...arities);
    function getHandler(argtypes: OSType[]) {
      for (let [decltypes, handler] of alternatives) {
        if (decltypes.length === argtypes.length &&
          forall(zip(decltypes, argtypes), ([decltype, argtype]) => commonSupertype(decltype, argtype) === decltype)) {
          return handler;
        }
      }
    }
    return <fixmeAny>{
      paramNames: paramNames,
      argAdapters: (_.range(0, minArity).map((i) => EagerSubformula)).concat(
        <fixmeAny>_.range(minArity, maxArity).map((i) => OptionalEagerSubformula)),
      typecheck: (model: ReadableModel, vars: VarTypes, ...argtypes: OSType[]) => {
        let handler = getHandler(argtypes);
        staticCheck(handler != null, () => `No valid alternative of '${operator}' ` +
          `for argument types ${(argtypes.map((t) => "'" + stringifyType(t) + "'")).join(", ")}`);
        return handler.typecheck.apply(handler, (<fixmeAny[]>[model, vars]).concat(argtypes));
      },
      evaluate: (model: ReadableModel, vars: VarValues, ...args: TypedSet[]) => {
        let argtypes = args.map((ts) => type);
        let handler = getHandler(argtypes);
        staticCheck(handler != null, () => `No valid alternative of '${operator}' ` +
          `for argument types ${(argtypes.map((t) => "'" + stringifyType(t) + "'")).join(", ")}`);
        return handler.evaluate.apply(handler, (<fixmeAny[]>[model, vars]).concat(args));
      },
      stringify: (model: ReadableModel, vars: VarTypes, ...sinfos: StringifyInfo[]) => {
        // Does it even make sense to have different stringifies for different alternatives?
        let [, handler] = alternatives[0];
        return handler.stringify.apply(handler, (<fixmeAny[]>[model, vars]).concat(sinfos));
      }
    };
  }

  function compareInfixOperator(symbol: string, precedence: Precedence, associativity: Associativity,
    // evaluateFn is a built-in number comparison operator like "<".  These
    // operators do the right thing on the `packDate` representation of dates.
    evaluateFn: (lhs: number, rhs: number) => boolean) {
    return overloaded(symbol, ["left", "right"],
      [[SpecialType.NUMBER, SpecialType.NUMBER], singletonInfixOperator<number, number>(
        symbol, precedence, associativity, SpecialType.NUMBER, SpecialType.NUMBER, SpecialType.BOOL, evaluateFn)],
      [[SpecialType.DATE, SpecialType.DATE], singletonInfixOperator<number, number>(
        symbol, precedence, associativity, SpecialType.DATE, SpecialType.DATE, SpecialType.BOOL, evaluateFn)]
      );
  }

  function stringifierForFunctionCall(functionName: string) {
    return (model: ReadableModel, vars: VarTypes, ...argSinfos: StringifyInfo[]): StringifyDispatchResult => ({
      str: functionName + "(" + (argSinfos.map((argSinfo) => argSinfo.strFor(Precedence.LOWEST))).join(", ") + ")",
      outerPrecedence: Precedence.ATOMIC
    });
  }

  //let allowNondeterminism = new Meteor.EnvironmentVariable<boolean>();  // default true

  interface OpHandlerUntyped {
    // Must be nonnull if any parameter contains subformulas.
    paramNames?: (null | string)[];
    argAdapters: ArgAdapter<fixmeAny, fixmeAny, fixmeAny, fixmeAny>[];
    validate?: (vars: VarSet, ...adaptedArgs: fixmeAny[]) => void;
    typecheck: (model: ReadableModel, vars: VarTypes, ...adaptedArgs: fixmeAny[]) => OSType;
    evaluate: (model: ReadableModel, vars: VarValues, ...adaptedArgs: fixmeAny[]) => TypedSet;
    stringify: (model: ReadableModel, vars: VarTypes, ...adaptedArgs: fixmeAny[]) => StringifyDispatchResult;
  }
  const OP_HANDLER_CHECKED_BRAND = Symbol();
  interface OpHandlerChecked extends OpHandlerUntyped {
    [OP_HANDLER_CHECKED_BRAND]: {};
  }

  // I tried mapped types and they didn't work, so this bumper crop of type
  // parameters. ~ Matt 2018-05-16

  function makeOpHandler(opHandler: {
    // https://github.com/Microsoft/TypeScript/issues/13126
    paramNames?: never[];
    argAdapters: never[];
    validate?: (vars: VarSet) => void;
    typecheck: (model: ReadableModel, vars: VarTypes) => OSType;
    evaluate: (model: ReadableModel, vars: VarValues) => TypedSet;
    stringify: (model: ReadableModel, vars: VarTypes) => StringifyDispatchResult;
  }): OpHandlerChecked;
  function makeOpHandler<A1, T1, E1, S1>(opHandler: {
    paramNames?: [null | string];
    argAdapters: [ArgAdapter<A1, T1, E1, S1>];
    validate?: (vars: VarSet, a1: A1) => void;
    typecheck: (model: ReadableModel, vars: VarTypes, a1: T1) => OSType;
    evaluate: (model: ReadableModel, vars: VarValues, a1: E1) => TypedSet;
    stringify: (model: ReadableModel, vars: VarTypes, a1: S1) => StringifyDispatchResult;
  }): OpHandlerChecked;
  function makeOpHandler<A1, T1, E1, S1, A2, T2, E2, S2>(opHandler: {
    paramNames?: [null | string, null | string];
    argAdapters: [ArgAdapter<A1, T1, E1, S1>, ArgAdapter<A2, T2, E2, S2>];
    validate?: (vars: VarSet, a1: A1, a2: A2) => void;
    typecheck: (model: ReadableModel, vars: VarTypes, a1: T1, a2: T2) => OSType;
    evaluate: (model: ReadableModel, vars: VarValues, a1: E1, a2: E2) => TypedSet;
    stringify: (model: ReadableModel, vars: VarTypes, a1: S1, a2: S2) => StringifyDispatchResult;
  }): OpHandlerChecked;
  function makeOpHandler<A1, T1, E1, S1, A2, T2, E2, S2, A3, T3, E3, S3>(opHandler: {
    paramNames?: [null | string, null | string, null | string];
    argAdapters: [ArgAdapter<A1, T1, E1, S1>, ArgAdapter<A2, T2, E2, S2>,
      ArgAdapter<A3, T3, E3, S3>];
    validate?: (vars: VarSet, a1: A1, a2: A2, a3: A3) => void;
    typecheck: (model: ReadableModel, vars: VarTypes, a1: T1, a2: T2, a3: T3) => OSType;
    evaluate: (model: ReadableModel, vars: VarValues, a1: E1, a2: E2, a3: E3) => TypedSet;
    stringify: (model: ReadableModel, vars: VarTypes, a1: S1, a2: S2, a3: S3) => StringifyDispatchResult;
  }): OpHandlerChecked;
  function makeOpHandler<A1, T1, E1, S1, A2, T2, E2, S2, A3, T3, E3, S3, A4, T4, E4, S4>(opHandler: {
    paramNames?: [null | string, null | string, null | string, null | string];
    argAdapters: [ArgAdapter<A1, T1, E1, S1>, ArgAdapter<A2, T2, E2, S2>,
      ArgAdapter<A3, T3, E3, S3>, ArgAdapter<A4, T4, E4, S4>];
    validate?: (vars: VarSet, a1: A1, a2: A2, a3: A3, a4: A4) => void;
    typecheck: (model: ReadableModel, vars: VarTypes, a1: T1, a2: T2, a3: T3, a4: T4) => OSType;
    evaluate: (model: ReadableModel, vars: VarValues, a1: E1, a2: E2, a3: E3, a4: E4) => TypedSet;
    stringify: (model: ReadableModel, vars: VarTypes, a1: S1, a2: S2, a3: S3, a4: S4) => StringifyDispatchResult;
  }): OpHandlerChecked;
  function makeOpHandler(opHandler: OpHandlerUntyped): OpHandlerChecked {
    return <OpHandlerChecked>opHandler;
  }

  let dispatch: {[opName: string]: OpHandlerChecked};
  //Meteor.startup(() => dispatch = {  // load order for SpecialType.ERROR
    // ["lit", type ID (string), elements (array)]:
    // A literal set of elements of the specified type.
    // Concrete syntax: 2, {3,4}, {5,6,7,} etc.  The elements may be JSON booleans,
    // numbers, or strings.
    // XXX: Now that we have union, we could change lit to accept only a single
    // element, but that will break existing abstract formulas.  Maybe do it
    // if/when we add data type validation?
    //
    // Using OldOSValue (unpacked dates) here because it's a relatively small
    // hack that lets us keep compatibility with old databases, while
    // implementing the formula repair would be more work.  We can always do the
    // migration later if we want. ~ Matt 2017-12-15
    lit: makeOpHandler({
      argAdapters: [Type, new SimpleValidationOnlyArgAdapter<OldOSValue[]>((list) => {
        staticCheck(_.isArray(list), () => "Set literal must be an array");
        // XXX: Validate actually OldOSValue.
      })],
      validate: (vars, type, list) => {
        // Future: Could go ahead and validate primitive-type literals here.
      },
      typecheck: (model, vars, type, list) => type,
      evaluate: (model, vars, type, list) => {
        let newList = (type == SpecialType.DATE) ? (<Date[]>list).map(packDate) : <OSValue[]>list;
        // XXXXXXX: Validate members of the type.
        return new TypedSet(type, new JSONSet(newList));
      },
      stringify: (model, vars, type, list) => ({
          str: (() => {
            let strOne = (val: OldOSValue) =>
              type == SpecialType.DATE ? `d"${stringifyDate(<Date>val)}"` :
              // Compared to valueToText: quote strings, and may as well keep
              // full precision for numbers.
              JSON.stringify(val);
            // Obviously, if someone manually creates a literal that requires a
            // leading minus or set notation, those constructs will be re-parsed as
            // operators rather than as part of the literal.
            if (type === rootColumnId) {
              // See stringifyNavigationStart.  This shouldn't be exposed anywhere else.
              return "$";
            } else if (list.length === 1) {
              return strOne(list[0]);
            } else {
              // XXX: Canonicalize order?
              return "{" + (list.map(strOne)).join(",") + "}";
            }
          })(),
          outerPrecedence: (() => {
            if (type === SpecialType.NUMBER && list.length === 1 && list[0] < 0) {
              // Should never be reached by parsing concrete syntax.
              return Precedence.NEG;
            } else {
              return Precedence.ATOMIC;
            }
          })()
        })
    //}),
    // ["var", varName (string)]:
    // Gets the value of a bound variable.
    // Concrete syntax: myVar
    "var": makeOpHandler({
      argAdapters: [new SimpleValidationOnlyArgAdapter<string>((varName) => {
        staticCheck(_.isString(varName), () => "Variable name must be a string");
      })],
      validate: (vars, varName) => {
        staticCheck(vars.has(varName), () => `Undefined variable ${varName}`);
      },
      typecheck: (model, vars, varName) => assertNotNull(vars.get(varName)),
      evaluate: (model, vars, varName) => assertNotNull(vars.get(varName)),
      stringify: (model, vars, varName) => ({
          str: (() => {
            if (varName === "this") {
              // A 'this' reference can only occur implicitly in concrete syntax, as
              // the left operand of a navigation.  The following is just a sentinel
              // for stringifyNavigationStart.
              return "this";
            } else {
              return annotateNavigationTarget(model, vars, null, varName, null, ["var", varName]);
            }
          })(),
          outerPrecedence: Precedence.ATOMIC
        })
    }),
    // ["up", startCells, targetColumnId, wantValues (bool)]
    // Concrete syntax: foo, FooCell, (expression).foo, etc.
    up: makeOpHandler({
      paramNames: ["start", null, null],
      argAdapters: [EagerSubformulaCells, ColumnId, WantValues],
      typecheck: typecheckUp,
      evaluate: goUp,
      stringify: (model, vars, startCellsSinfo, targetColumnId, wantValues) =>
        stringifyNavigation("up", model, vars, startCellsSinfo, targetColumnId, null, wantValues)
    }),
    // ["down", startCells, targetColumnId, keys, wantValues (bool)]
    // Currently allows only one step, matching the concrete syntax.  This makes
    // life easier for now.
    // XXX: Eventually the easiest way to provide the refactoring support we'll
    // want is to allow multiple steps.  We can see the issue already if startCells
    // changes type from one ancestor of the target column to another, e.g.,
    // because it is reading from another column whose formula changed.
    // Concrete syntax: foo, FooCell, (expression).foo, ::Bar, etc.
    down: makeOpHandler({
      paramNames: ["start", null, "keys", null],
      argAdapters: [EagerSubformulaCells, ColumnId, OptionalEagerSubformula, WantValues],
      validate: (vars, startCellsFmla, targetColumnId, keysFmla, wantValues) => {
        staticCheck(!(keysFmla != null && wantValues), () => "Can only specify keys when navigating to objects.");
      },
      typecheck: typecheckDown,
      evaluate: goDown,
      stringify: (model, vars, startCellsSinfo, targetColumnId, keysSinfo, wantValues) =>
        stringifyNavigation("down", model, vars, startCellsSinfo, targetColumnId, keysSinfo, wantValues)
    }),
    // ["revdown", startCells, domainFmla, fieldColumnId]
    // startCells.{domain by field}
    // Means union[s : startCells]({o : domain | s in o.field}).
    revdown: makeOpHandler({
      paramNames: ["start", "domain", null],
      argAdapters: [EagerSubformula, EagerSubformulaCells, ColumnId],
      typecheck: (model, vars, startType, domainType, fieldColumnId) => {
        let fieldCol = getExistingColumn(fieldColumnId);
        staticCheck(!fieldCol.isObject, () => "Inverse navigation can only be used on fields.");
        staticCheck(fieldCol.parent == domainType,
          () => "Field for inverse navigation is not a field of the domain type.");
        staticExpectType("Start expression for inverse navigation", startType, getColumnType(fieldCol));
        return domainType;
      },
      evaluate: (model, vars, startTset, domainTset, fieldColumnId) => {
        let startElts = startTset.elements();
        // XXX Use the checked type instead?
        return new TypedSet(domainTset.type, new JSONSet(_.filter(domainTset.set.elements(), (domainObj) => {
          let domainQFamilyId = {columnId: <NonRootColumnId>fieldColumnId,
            parentCellId: /*typechecked*/ <CellId>domainObj};
          let fieldTset = readFamilyForFormula(model, domainQFamilyId);
          return startElts.some((y) => fieldTset.set.has(y));
        })));
      },
      stringify: (model, vars, startSinfo, domainSinfo, fieldColumnId) => ({
        str: stringifyNavigationStart(startSinfo) + "{" +
          // This is unambiguous, but it might be hard to read if domainSinfo
          // has a low-precedence operator.
          domainSinfo.strFor(Precedence.LOWEST) + " by " +
          stringifyNavigationTarget("down", model, vars, domainSinfo.formula, fieldColumnId, null, true) + "}",
        outerPrecedence: Precedence.ATOMIC
      })
    }),
    // ["if", condition, thenFmla, elseFmla]
    // Concrete syntax: if(condition, thenFmla, elseFmla)
    // (Can we think of a better concrete syntax?)
    "if": makeOpHandler({
      paramNames: ["condition", "thenExpr", "elseExpr"],
      argAdapters: [EagerSubformula, LazySubformula, LazySubformula],
      typecheck: (model, vars, conditionType, thenType, elseType) => {
        staticExpectType("if condition", conditionType, SpecialType.BOOL);
        let type = commonSupertype(thenType, elseType);
        staticCheck(type !== SpecialType.ERROR,
          () => `Mismatched types in if branches: '${stringifyType(thenType)}' and '${stringifyType(elseType)}'`);
        return type;
      },
      evaluate: (model, vars, conditionTset, thenFmla, elseFmla) =>
        evaluateFormula(model, vars, singleElement(conditionTset) ? thenFmla : elseFmla),
      stringify: stringifierForFunctionCall("if")
    }),
    count: makeOpHandler({
      paramNames: ["set"],
      argAdapters: [EagerSubformula],
      typecheck: (model, vars, domainType) => SpecialType.NUMBER,
      evaluate: (model, vars, domainTset) =>
        new TypedSet(SpecialType.NUMBER, set([domainTset.elements().length])),
      stringify: stringifierForFunctionCall("count")
    }),
    oneOf: makeOpHandler({
      paramNames: ["set"],
      argAdapters: [EagerSubformula],
      typecheck: (model, vars, domainType) => domainType,
      evaluate: (model, vars, domainTset) => {
        runtimeCheck(domainTset.elements().length > 0, () => "oneOf on empty set.");
        return new TypedSet(domainTset.type, set([domainTset.elements()[0]]));
      },
      stringify: stringifierForFunctionCall("oneOf")
    }),
    // ["filter", domain (subformula), [varName, predicate (subformula)]]:
    // For each cell in the domain, evaluates the predicate with varName bound to
    // the domain cell, which must return a singleton boolean.  Returns the set of
    // domain cells for which the predicate returned true.
    // Concrete syntax: {x : expr | predicate}
    filter: makeOpHandler({
      paramNames: ["set", "predicate"],
      argAdapters: [EagerSubformula, Lambda],
      typecheck: (model, vars, domainType, predicateLambda) => {
        let predicateType = predicateLambda(domainType);
        staticExpectType("Predicate", predicateType, SpecialType.BOOL);
        return domainType;
      },
      evaluate: (model, vars, domainTset, predicateLambda) => {
        // XXX Use the checked type instead?
        return new TypedSet(domainTset.type, new JSONSet(_.filter(domainTset.set.elements(), (x: OSValue) => {
          // Future: Figure out where to put this code once we start duplicating it.
          let tset = new TypedSet(domainTset.type, new JSONSet([x]));
          return /*typechecked*/ <boolean>singleElement(predicateLambda(tset));
        })));
      },
      stringify: (model, vars, domainSinfo, predicateLambda) => {
        // XXX Wasteful
        let predicateSinfo = predicateLambda(tryTypecheckFormula(model, vars, domainSinfo.formula));
        return {
          str: `{${predicateSinfo[0]} : ${domainSinfo.strFor(Precedence.LOWEST)} ` +
            `| ${predicateSinfo[1].strFor(Precedence.LOWEST)}}`,
          outerPrecedence: Precedence.ATOMIC
        };
      }
    }),
    sum: makeOpHandler({
      paramNames: ["domain", "function"],
      argAdapters: [EagerSubformula, Lambda],
      typecheck: (model, vars, domainType, addendLambda) => {
        let addendType = addendLambda(domainType);
        staticExpectType("Element of 'sum'", addendType, SpecialType.NUMBER);
        return SpecialType.NUMBER;
      },
      evaluate: (model, vars, domainTset, addendLambda) => {
        let res = 0;
        for (let x of domainTset.elements()) {
          let tset = new TypedSet(domainTset.type, new JSONSet([x]));
          res += /*typechecked*/ <number>singleElement(addendLambda(tset));
        }
        return new TypedSet(SpecialType.NUMBER, new JSONSet([res]));
      },
      stringify: (model, vars, domainSinfo, addendLambda) => {
        let addendSinfo = addendLambda(tryTypecheckFormula(model, vars, domainSinfo.formula));
        return {
          str: `sum[${addendSinfo[0]} : ${domainSinfo.strFor(Precedence.LOWEST)}]` +
            `(${addendSinfo[1].strFor(Precedence.LOWEST)})`,
          outerPrecedence: Precedence.ATOMIC
        };
      }
    }),
    min: makeOpHandler({
      paramNames: ["set"],
      argAdapters: [EagerSubformula],
      typecheck: (model, vars, domainType) => {
        staticExpectType("Element of 'min'", domainType, SpecialType.NUMBER);
        return SpecialType.NUMBER;
      },
      evaluate: (model, vars, domainTset) =>
        new TypedSet(SpecialType.NUMBER,
          // Make min({}) return {} because Infinity is not valid JSON. :/
          set(domainTset.set.size() == 0 ? [] :
            [Math.min(.../*typechecked*/ <number[]>domainTset.elements())])),
      stringify: stringifierForFunctionCall("min")
    }),
    max: makeOpHandler({
      paramNames: ["set"],
      argAdapters: [EagerSubformula],
      typecheck: (model, vars, domainType) => {
        staticExpectType("Element of 'max'", domainType, SpecialType.NUMBER);
        return SpecialType.NUMBER;
      },
      evaluate: (model, vars, domainTset) =>
        new TypedSet(SpecialType.NUMBER,
          // Make max({}) return {} because -Infinity is not valid JSON. :/
          set(domainTset.set.size() == 0 ? [] :
            [Math.max(.../*typechecked*/ <number[]>domainTset.elements())])),
      stringify: stringifierForFunctionCall("max")
    }),
    // Predicates on two sets of the same type.
    //"=": sameTypeSetsInfixPredicate("=", Precedence.COMPARE, ASSOCIATIVITY_NONE, EJSON.equals),
    "!=": sameTypeSetsInfixPredicate("!=", Precedence.COMPARE, ASSOCIATIVITY_NONE, (x, y) => !EJSON.equals(x, y)),
    "in": sameTypeSetsInfixPredicate("in", Precedence.COMPARE, ASSOCIATIVITY_NONE,
      ((x, y) => y.hasAll(x)), ["needle", "haystack"]),
    // Unary minus.
    "neg": makeOpHandler({
      paramNames: ["expr"],
      argAdapters: [EagerSubformula],
      typecheck: (model, vars, argType) => {
        staticExpectType("Operand of unary '-'", argType, SpecialType.NUMBER);
        return SpecialType.NUMBER;
      },
      evaluate: (model, vars, arg) =>
        new TypedSet(SpecialType.NUMBER, set([-singleElement(arg)])),
      stringify: (model, vars, argSinfo) => ({
          str: `-${argSinfo.strFor(Precedence.NEG)}`,
          outerPrecedence: Precedence.NEG
        })
    }),
    // XXX Since we look at one binary operation at a time and '+' is left
    // associative, "foo" + 3 + 5 is "foo35" but 3 + 5 + "foo" is "8foo".  Java
    // is the same way, but we could do better for users who are unaware of this
    // subtlety by making '+' variadic in the abstract syntax.  This is a rare
    // case because string concatenations will usually include a delimiter.  Or
    // we could just use a different operator for string concatenation.
    //
    // XXX SpecialType.ERROR is a misnomer in this context: it means we accept tsets of
    // any valid type.  (There's no way to write a subexpression that actually
    // returns SpecialType.ERROR; instead it will cause a FormulaValidationError.)
    //
    // tslint:disable:max-line-length : Breaking these lines significantly reduces readability.
    "+": overloaded("+", ["left", "right"],
      [[SpecialType.NUMBER, SpecialType.NUMBER],
        singletonInfixOperator("+", Precedence.PLUS, ASSOCIATIVITY_LEFT, SpecialType.NUMBER, SpecialType.NUMBER, SpecialType.NUMBER, (x: number, y: number) => x + y)],
      [[SpecialType.ERROR, SpecialType.ERROR],
        infixOperator("+", Precedence.PLUS, ASSOCIATIVITY_LEFT, SpecialType.ERROR, SpecialType.ERROR, SpecialType.TEXT, (model, tsetX, tsetY) => tsetToText(model, tsetX) + tsetToText(model, tsetY))]),
    "-": singletonInfixOperator("-", Precedence.PLUS, ASSOCIATIVITY_LEFT, SpecialType.NUMBER, SpecialType.NUMBER, SpecialType.NUMBER, (x: number, y: number) => x - y),
    "*": singletonInfixOperator("*", Precedence.TIMES, ASSOCIATIVITY_LEFT, SpecialType.NUMBER, SpecialType.NUMBER, SpecialType.NUMBER, (x: number, y: number) => x * y),
    "/": singletonInfixOperator("/", Precedence.TIMES, ASSOCIATIVITY_LEFT, SpecialType.NUMBER, SpecialType.NUMBER, SpecialType.NUMBER, (x: number, y: number) => x / y),
    "^": singletonInfixOperator("^", Precedence.POW, ASSOCIATIVITY_RIGHT, SpecialType.NUMBER, SpecialType.NUMBER, SpecialType.NUMBER, Math.pow, ["base", "exponent"]),
    "<": compareInfixOperator("<", Precedence.COMPARE, ASSOCIATIVITY_NONE, (x, y) => x < y),
    "<=": compareInfixOperator("<=", Precedence.COMPARE, ASSOCIATIVITY_NONE, (x, y) => x <= y),
    ">": compareInfixOperator(">", Precedence.COMPARE, ASSOCIATIVITY_NONE, (x, y) => x > y),
    ">=": compareInfixOperator(">=", Precedence.COMPARE, ASSOCIATIVITY_NONE, (x, y) => x >= y),
    // TODO: Short circuit?
    "&&": singletonInfixOperator("&&", Precedence.AND, ASSOCIATIVITY_LEFT, SpecialType.BOOL, SpecialType.BOOL, SpecialType.BOOL, (x: boolean, y: boolean) => x && y),
    "||": singletonInfixOperator("||", Precedence.OR, ASSOCIATIVITY_LEFT, SpecialType.BOOL, SpecialType.BOOL, SpecialType.BOOL, (x: boolean, y: boolean) => x || y),
    // tslint:enable:max-line-length
    "!": makeOpHandler({
      paramNames: ["condition"],
      argAdapters: [EagerSubformula],
      typecheck: (model, vars, argType) => {
        staticExpectType("Operand of '!'", argType, SpecialType.BOOL);
        return SpecialType.BOOL;
      },
      evaluate: (model, vars, arg) =>
        new TypedSet(SpecialType.BOOL, set([!singleElement(arg)])),
      stringify: (model, vars, argSinfo) => ({
          str: `!${argSinfo.strFor(Precedence.NEG)}`,
          outerPrecedence: Precedence.NEG
        })
    }),
    // ["union", list of subformulas]
    // Union of a fixed number of sets.
    union: makeOpHandler({
      paramNames: ["part"],  // Will be expanded by getSubformulaTree
      argAdapters: [HomogeneousEagerSubformulaList],
      typecheck: (model, vars, termsType) => termsType,
      evaluate: (model, vars, terms) => {
        let res = new TypedSet();
        for (let term of terms) {
          res.addAll(term);
        }
        return res;
      },
      stringify: (model, vars, termSinfos) => ({
          str: "{" + (termSinfos.map((termSinfo) => termSinfo.strFor(Precedence.LOWEST))).join(", ") + "}",
          outerPrecedence: Precedence.ATOMIC
        })
    }),
    dummy: makeOpHandler({
      paramNames: [],
      argAdapters: [],
      typecheck: (model, vars) => SpecialType.EMPTY,
      evaluate: (model, vars) => new TypedSet(),
      stringify: (model, vars) => ({
          // DUMMY_FORMULA is treated specially in the formula bar.  The following is
          // in case it shows up somewhere else in the system.
          str: "dummy",
          outerPrecedence: Precedence.ATOMIC
        })
    }),
    toText: makeOpHandler({
      paramNames: ["expr"],
      argAdapters: [EagerSubformula],
      typecheck: (model, vars, argType) => SpecialType.TEXT,
      evaluate: (model, vars, arg) =>
        new TypedSet(SpecialType.TEXT, set([tsetToText(model, arg)])),
      stringify: stringifierForFunctionCall("toText")
    }),
    // ["now"]
    now: makeOpHandler({
      argAdapters: [],
      typecheck: (model, vars) => SpecialType.DATE,
      evaluate: (model, vars) => {
        runtimeCheck(fallback(allowNondeterminism.get(), true),
          () => "Time dependency is not allowed here.");
        return new TypedSet(SpecialType.DATE, new JSONSet([packDate(new Date())]));
      },
      stringify: stringifierForFunctionCall("now")
    }),
    find: makeOpHandler({
      paramNames: ["needle", "haystack"],
      argAdapters: [EagerSubformula, EagerSubformula],
      typecheck: (model: ReadableModel, vars: VarTypes, needleType: OSType, haystackType: OSType) => {
        staticExpectType("'needle' argument to 'find'", needleType, SpecialType.TEXT);
        staticExpectType("'haystack' argument to 'find'", haystackType, SpecialType.TEXT);
        return SpecialType.NUMBER;
      },
      evaluate: (model: ReadableModel, vars: VarValues, needleTset: TypedSet, haystackTset: TypedSet) => {
        let needle = /*typechecked*/ <string>singleElement(needleTset);
        let haystack = /*typechecked*/ <string>singleElement(haystackTset);
        let pos = haystack.indexOf(needle);
        return new TypedSet(SpecialType.NUMBER, set(pos < 0 ? [] : [pos]));
      },
      stringify: stringifierForFunctionCall("find")
    }),
    upper: makeOpHandler({
      paramNames: [SpecialType.TEXT],
      argAdapters: [EagerSubformula],
      typecheck: (model: ReadableModel, vars: VarTypes, argType: OSType) => {
        staticExpectType("Argument to 'upper'", argType, SpecialType.TEXT);
        return SpecialType.TEXT;
      },
      evaluate: (model: ReadableModel, vars: VarValues, argTset: TypedSet) => {
        let arg = /*typechecked*/ <string>singleElement(argTset);
        return new TypedSet(SpecialType.TEXT, set([arg.toUpperCase()]));
      },
      stringify: stringifierForFunctionCall("upper")
    }),
    lower: makeOpHandler({
      paramNames: [SpecialType.TEXT],
      argAdapters: [EagerSubformula],
      typecheck: (model: ReadableModel, vars: VarTypes, argType: OSType) => {
        staticExpectType("Argument to 'lower'", argType, SpecialType.TEXT);
        return SpecialType.TEXT;
      },
      evaluate: (model: ReadableModel, vars: VarValues, argTset: TypedSet) => {
        let arg = /*typechecked*/ <string>singleElement(argTset);
        return new TypedSet(SpecialType.TEXT, set([arg.toLowerCase()]));
      },
      stringify: stringifierForFunctionCall("lower")
    }),
    viewObjectUrl: makeOpHandler({
      // XXX: We'd prefer to show the actual field names, but that's not trivial.
      paramNames: [null, "field"],
      argAdapters: [ColumnId, ViewObjectFieldList],
      typecheck: (model, vars, type, fieldTypes) => {
        let col = getExistingColumn(type);
        staticCheck(col.isViewObjectRoot,
          () => "viewObjectUrl view object type is not in fact a view object root");
        let fieldsSeen = new Set<ColumnId>();
        for (let [fieldId, fieldType] of fieldTypes) {
          staticCheck(!fieldsSeen.has(fieldId),
            () => `viewObjectUrl: Column ${stringifyColumnRef([fieldId, true])} specified more than once`);
          fieldsSeen.add(fieldId);
          let fieldCol = getExistingColumn(fieldId);
          staticCheck(fieldCol.parent == type && !fieldCol.isObject,
            () => `viewObjectUrl: Column ${stringifyColumnRef([fieldId, true])} ` +
            `is not a field of the view object type`);
          staticExpectType("Content of field " + stringifyColumnRef([fieldId, true]),
            fieldType, getColumnType(fieldCol));
        }
        return SpecialType.TEXT;
      },
      evaluate: (model, vars, type, fields) => {
        let dump: ViewObjectDump = {};
        for (let [fieldId, tset] of fields) {
          dump[fieldId] = tset.elements();
        }
        // NOTE: The following depends on the view object query parameters.
        // Right now, the only way to change them is to remove and re-add the
        // request on the model, which does an invalidateDataCache.  If we make
        // formulas incremental in the future, we'll need to track the
        // dependency properly.
        //
        // If the master data set calls viewObjectUrl, we have no way to retain
        // query parameters.  That's a limitation of the current design.
        let ctx = currentViewObject.get();
        let url = Layout.uiUrl(type, dump, (ctx != null) ? ctx.queryParams : {});
        return new TypedSet(SpecialType.TEXT, set([url]));
      },
      stringify: (model, vars, type, fieldsStrFun) => ({
        str: "viewObjectUrl(" + stringifyType(type) + fieldsStrFun(type) + ")",
        outerPrecedence: Precedence.ATOMIC
      })
    }),
  });

  // Catches syntax errors, references to nonexistent bound variables, and
  // variable shadowing, but not anything related to schema, data, or types.
  //
  // This is just different enough from dispatchSubformula not to use it.
  // Specifically, the validate method of the operation is optional, and it
  // receives the original arguments (the adapters do not return values).
  export type VarSet = JSONSet<string>;
  function validateSubformula(vars: VarSet, formula: Unvalidated<Formula>) {
    staticCheck(_.isArray(formula), () => "Subformula must be an array.");
    staticCheck(_.isString(formula[0]), () => "Subformula must begin with an operation name (a string).");
    let opName: string = formula[0];
    staticCheck(dispatch.hasOwnProperty(opName), () => `Unknown operation '${opName}'`);
    let d = dispatch[opName];
    let args = formula.slice(1);
    staticCheck(args.length === d.argAdapters.length,
      () => `Wrong number of arguments to '${opName}' (required ${d.argAdapters.length}, got ${args.length})`);
    d.argAdapters.forEach((adapter, i) => {
      adapter.validate(vars, args[i]);
    });
    if (d.validate != null) {
      d.validate(vars, ...args);
    }
  }

  export function validateFormula(formula: Unvalidated<Formula>) {
    try {
      validateSubformula(new JSONSet(["this"]), formula);
    } catch (e) {
      // XXX: Want to do this here?
      if (e instanceof StaticError) {
        throw new Meteor.Error("invalid-formula", "Invalid formula: " + e.message);
      } else {
        throw e;
      }
    }
  }

  function dispatchFormula(action: "typecheck" | "evaluate" | "stringify",
    formula: Formula, ...contextArgs: fixmeAny[]) {
    let d = dispatch[formula[0]];
    let args = formula.slice(1);
    let adaptedArgs = d.argAdapters.map((adapter, i) =>
      (<fixmeAny>adapter)[action] != null
        ? (<fixmeAny>adapter)[action].apply(adapter, contextArgs.concat([args[i]]))
        : args[i]);
    return (<fixmeAny>d)[action].apply(d, contextArgs.concat(adaptedArgs));
  }

  // Assumes formula has passed validation.  Returns type.
  export type VarTypes = JSONKeyedMap<string, OSType>;
  export function typecheckFormula(model: ReadableModel, vars: VarTypes, formula: Formula): OSType {
    formula.vars = vars;
    return formula.type = dispatchFormula("typecheck", formula, model, vars);
  }

  // Helper for use in stringify, where we want to do our best rather than crash if
  // the formula is ill-typed.
  function tryTypecheckFormula(model: ReadableModel, vars: VarTypes, formula: Formula) {
    try {
      return typecheckFormula(model, vars, formula);
    } catch (e) {
      return SpecialType.EMPTY;
    }
  }

  // Assumes formula has passed typechecking.
  export type VarValues = JSONKeyedMap<string, TypedSet>;
  function saveTrace(formula: Formula, vars: VarValues, outcome: Outcome) {
    if (formula.traces == null) {
      formula.traces = new EJSONKeyedMap<VarValues, Outcome>();
    }
    formula.traces.set(vars, outcome);
  }
  export function evaluateFormula(model: ReadableModel, vars: VarValues, formula: Formula) {
    let result: TypedSet;
    try {
      result = dispatchFormula("evaluate", formula, model, vars);
    } catch (e) {
      if (e instanceof RuntimeError && model.isTracing) {
        saveTrace(formula, vars, {error: e.message});
      }
      throw e;
    }
    if (model.isTracing) {
      saveTrace(formula, vars, {result: result});
    }
    return result;
  }

  export function evaluateFormulaDeterministic(model: ReadableModel, vars: VarValues, formula: Formula) {
    return allowNondeterminism.withValue(false, () => evaluateFormula(model, vars, formula));
  }

  // TODO: Separate out the determination of the initial variable values so we can
  // reuse the rest for formulas in procedures, etc.
  export function traceColumnFormula(formula: Formula, columnId: NonRootColumnId) {
    let origModel = getReadableModel();
    // This looks awkward, but if we made isTracing an environment variable,
    // then we'd have to unset it when demanding another family (or else assume
    // we run only on fully evaluated sheets and that never happens).  Maybe it
    // isn't so bad after all.
    let tracingModel: ReadableModel = {
      evaluateFamily: (qFamilyId) => origModel.evaluateFamily(qFamilyId),
      evaluateAll: () => origModel.evaluateAll(),
      typecheckColumn: (colId) => origModel.typecheckColumn(colId),
      typecheckAll: () => origModel.typecheckAll(),
      isTracing: true
    };
    // Here we really do want to ignore erroneous families in the parent column
    // because there is nothing to trace for them.
    let parentColumnId = getExistingColumn(columnId).parent;
    allCellIdsInColumnIgnoreErrors(parentColumnId).forEach((cellId) => {
      try {
        let vars = new JSONKeyedMap([["this", new TypedSet(parentColumnId, new JSONSet([cellId]))]]);
        evaluateFormulaDeterministic(tracingModel, vars, formula);
      } catch (e) {
        if (e instanceof RuntimeError) {
          // Ignore; already traced.
        } else {
          throw e;
        }
      }
    });
  }

  export let DUMMY_FORMULA: Formula = ["dummy"];
  //Meteor.startup(() => {  // Load order for SpecialType.EMPTY
  //  DUMMY_FORMULA.type = SpecialType.EMPTY;  // Action bar looks for type field.
  //});

  // BELOW: Concrete syntax support.

  function validateAndTypecheckFormula(model: ReadableModel, vars: VarTypes, formula: Formula) {
    validateSubformula(new JSONSet(vars.keys()), formula);
    return typecheckFormula(model, vars, formula);
  }

  // Handle startCellsFmla.targetName or targetName by itself
  // (startCellsFmla == null), which may also be a variable reference.
  // Returns the new formula with the call to "up" or "down" added around
  // startCellsFmla.
  function resolveNavigation(model: ReadableModel, vars: VarTypes,
    startCellsFmla: null | Formula, targetName: string, keysFmla: null | Formula): Formula {
    let interpretations: Formula[] = [];
    if (startCellsFmla == null) {
      if (vars.get("this") != null) {
        staticCheck(targetName !== "this",
          () => "Explicit \"this\" is not allowed in concrete syntax.  " + "Please use the object name for clarity.");
        if (vars.get(targetName) != null) {
          interpretations.push(["var", targetName]);
        }
        startCellsFmla = ["var", "this"];
        // Fall through to navigation interpretations.
      } else {  // i.e., in procedures
        staticCheck(vars.get(targetName) != null, () => `Undefined variable '${targetName}'`);
        // Currently this can only happen in procedures.
        staticCheck(vars.get(targetName) !== SpecialType.ERROR,
          () => `Variable '${targetName}' cannot be read because ` +
          "it does not have a known type at this point.");
        return ["var", targetName];
      }
    }

    // XXX: This is a lot of duplicate work reprocessing subtrees.
    let startCellsType = validateAndTypecheckFormula(model, vars, startCellsFmla);
    staticCheck(typeIsReference(startCellsType),
      () => `Expected a set of cells, got set of '${stringifyType(startCellsType)}'`);

    // Check logical ancestor objects (no keys).
    // Note, it's impossible to navigate to the root column since it has no field name or
    // object name.
    let [upPath, /*down path should be empty*/] = findCommonAncestorPaths(<ColumnId>startCellsType, rootColumnId);
    for (let upColumnId of upPath) {
      if (objectOrBracketedFieldName(getExistingColumn(upColumnId)) === targetName) {
        interpretations.push(["up", startCellsFmla, upColumnId, false]);
      }
    }

    // Check logical children.
    for (let [[columnId, isValues], direction] of columnLogicalChildrenByName(<ColumnId>startCellsType, targetName)) {
      interpretations.push([direction, startCellsFmla, columnId, isValues]);
    }

    // Future: Enforce uniqueness of interpretations in any scope?
    staticCheck(interpretations.length === 1,
      () => `${interpretations.length} possible interpretations for ` +
      `<type ${stringifyType(startCellsType)}>.${targetName}, wanted one.`);
    let formula = interpretations[0];

    if (formula[0] === "down") {
      // typecheckDown checks the rest of the requirements for subscripting.
      formula.splice(3, 0, keysFmla);
    } else {
      // We have to check this here so we don't silently ignore the subscript.
      // XXX: When navigating to a key column, this error message is inconsistent
      // with the user-facing model, in which such a navigation is down.
      staticCheck(keysFmla == null, () => "Only down navigations can be subscripted with keys.");
    }

    // If Bar is an object type with key foo, "(...).Bar.foo" parses as
    // ['up', ['down', ..., fooID, null, false], fooID, true].  Convert to
    // ['down', ..., fooID, null, true] so it displays as "(...).foo" if the object
    // type is removed.  (Converse of the case in stringifyNavigation.)
    if (formula[0] === "up" && formula[3] && formula[1][0] === "down" &&
      formula[1][2] === formula[2] && (formula[1][3] == null) && !formula[1][4]) {
      formula = ["down", formula[1][1], formula[2], null, true];
    }

    return formula;
  }

  // Reused by parseProcedure. :/
  type StartToken = "ENTRY_FORMULA" | "ENTRY_PROCEDURE";
  interface ParserHelper {
    vars: VarTypes;
    startToken: StartToken;
    bindVar: (varName: string, formula: Formula) => void;
    unbindVar: (varName: string) => void;
    navigate: (startCellsFmla: Formula, targetName: string, keysFmla: Formula) => Formula;
    // Special for revdown
    resolveField: (startCellsFmla: Formula, targetName: string) => ColumnId;
    parseDate: (text: string) => Date;
    parseError: (err: string, hash: fixmeAny) => never;
    resolveViewObject: (typeName: string, entries: [string, Formula][]) => Formula;
  }
  export function setupParserCommon(startToken: StartToken, vars: VarTypes) {
    let parser = new Jison.Parsers.language.Parser();
    let parser_yy: ParserHelper = parser.yy;
    parser_yy.vars = vars.shallowClone();
    parser_yy.startToken = startToken;
    parser_yy.bindVar = function(varName, formula) {
      // Don't check shadowing here, because the rules for procedures are
      // complicated.  It will be done later by the validate method.
      this.vars.set(varName, validateAndTypecheckFormula(getReadableModel(), this.vars, formula));
    };
    parser_yy.unbindVar = function(varName) {
      this.vars.delete(varName);
    };
    parser_yy.navigate = function(startCellsFmla, targetName, keysFmla) {
      return resolveNavigation(getReadableModel(), this.vars, startCellsFmla, targetName, keysFmla);
    };
    parser_yy.resolveField = function(startCellsFmla, targetName) {
      let fmla = resolveNavigation(getReadableModel(), this.vars, startCellsFmla, targetName, null);
      staticCheck(fmla[0] == "down", () => "Expected a field");
      return fmla[2];
    };
    parser_yy.parseDate = (text) => {
      try {
        return parseDate(text);
      } catch (e) {
        if (e instanceof Error) {
          // Invalid date.  XXX: Decide on a more specific type for the error
          // thrown by parseDate.
          throw new StaticError(e.message);
        } else {
          throw e;
        }
      }
    };
    parser_yy.parseError = (err, hash) => {
      throw new SyntaxError(err, hash);
    };
    parser_yy.resolveViewObject = function(typeName, entries) {
      // Resolve the type.  Use resolveNavigation for error checking.
      let typeFmla = resolveNavigation(getReadableModel(), this.vars, ["lit", "_root", [[]]], typeName, null);
      assert(typeFmla[0] == "down");
      let type = typeFmla[2];
      // Resolve the fields.
      let resolvedEntries = entries.map(([fieldName, expr]) => {
        let fieldFmla = resolveNavigation(getReadableModel(), this.vars, ["lit", type, [[]]], fieldName, null);
        assert(fieldFmla[0] == "down");  // There shouldn't be a way to go up.
        return [fieldFmla[2], expr];
      });
      return ["viewObjectUrl", type, resolvedEntries];
    };
    return parser;
  }

  export function parseFormula(thisType: OSType, fmlaString: string): Formula {
    // XXX: If we are changing a formula so as to introduce a new cyclic type
    // checking dependency, we use the old types of the other columns to interpret
    // navigations in the new formula.  However, as soon as we save, all the
    // columns in the cycle will change to SpecialType.ERROR and the navigations we just
    // interpreted will become invalid.  This behavior is weird but not worth
    // fixing now.

    let parser = setupParserCommon("ENTRY_FORMULA", new JSONKeyedMap([["this", thisType]]));

    try {
      return parser.parse(fmlaString);
    } catch (e) {
      if (e instanceof SyntaxError) {
        throw new StaticError(e.message);
      } else {
        throw e;
      }
    }
  }

  function stringifyIdentCommon(entryPoint: "ENTRY_IDENT" | "ENTRY_NAVIGATION_STEP", ident: string) {
    for (let str of [ident, `\`${ident}\``]) {
      let parser = new Jison.Parsers.language.Parser();
      parser.yy.startToken = entryPoint;
      try {
        if (parser.parse(str) === ident) {
          return str;
        }
      } catch (e) {
        // fall through
      }
    }
    // Currently I think this only happens if the identifier contains `, but it's
    // nice for the code to be future-proof. ~ Matt 2015-10-16
    throw new StaticError(`Cannot stringify identifier '${ident}'`);
  }

  // Special version that won't unnecessarily backquote the [key] fallback object
  // name syntax.
  // XXX: I guess this lets people define a variable named [foo] and then refer to
  // it without backquotes in some (but not all) contexts.
  function stringifyNavigationStep(ident: string) {
    return stringifyIdentCommon("ENTRY_NAVIGATION_STEP", ident);
  }

  function stringifyIdent(ident: string) {
    return stringifyIdentCommon("ENTRY_IDENT", ident);
  }

  function stringifySubformula(model: ReadableModel, vars: VarTypes, formula: Formula): StringifyInfo {
    let res = dispatchFormula("stringify", formula, model, vars);
    return {
      // Save original: used by stringifyNavigation.  (Might not be the best design.)
      formula: formula,
      strFor: (lowestSafePrecedence: Precedence) =>
        res.outerPrecedence >= lowestSafePrecedence ? res.str : `(${res.str})`
    };
  }

  export function stringifyFormula(thisType: OSType, formula: Formula) {
    return stringifySubformula(getReadableModel(),
      new JSONKeyedMap([["this", thisType]]), formula).strFor(Precedence.LOWEST);
  }

  export interface SubformulaTree {
    formula: Formula;
    children: {paramName: string, node: SubformulaTree}[];
  }
  export function getSubformulaTree(formula: Formula): SubformulaTree {
    let d = dispatch[formula[0]];
    let args = formula.slice(1);
    let children: {paramName: string, node: SubformulaTree}[] = [];
    d.argAdapters.forEach((adapter, i) => {
      if (adapter.getSubformulas != null) {
        let paramName = fixmeAssertNotNull(fixmeAssertNotNull(d.paramNames)[i]);
        let childNodes = adapter.getSubformulas(args[i]).map((f: Formula) => getSubformulaTree(f));
        children.push(...(childNodes.length !== 1
          ? childNodes.map((n: SubformulaTree, j: number) => ({  // union, others?
            paramName: `${paramName}${j + 1}`,
            node: n
          }))
          : [{
            paramName: paramName,
            node: childNodes[0]
          }]
        ));
      }
    });
    return {
      formula: formula,
      children: children
    };
  }

  // Ideally FormulaInternals would be a namespace, but if the items are
  // exported under the original names, then they shadow the originals and we'd
  // have to define aliases to be able to refer to the originals and suppress
  // the linter too.  Forget it. ~ Matt 2016-10-12
  export type FormulaInternals_ParserHelper = ParserHelper;
  export let FormulaInternals = {
    EagerSubformula: EagerSubformula,
    stringifyIdent: stringifyIdent,
    tryTypecheckFormula: tryTypecheckFormula,
    PRECEDENCE_LOWEST: Precedence.LOWEST,
    cellsToValuesIfWanted: cellsToValuesIfWanted,
    goUp: goUp,
    goDown: goDown
  };

