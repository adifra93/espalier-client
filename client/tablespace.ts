import assert from "assert";
import { assertNotNull } from "../assert";
import ReactiveVar from 'meteor-standalone-reactive-var';
import { newLocalCollection, ReadOnlyCollectionWrapper, SimpleReadOnlyCollection } from "../lib/collections";
import { LayoutsHardcodedMeta, layoutsHardcodedMetaDocId, SheetCollection, SheetCollectionName, sheetCollectionNames, sheetCollectionSpecs, SheetCollectionTypes, Tablespace } from "../tablespace";
import { fixmeAny } from "../lib/typescript";
import { ColumnId, Column } from "../schema";

  // See the overview comment at the top of src/tablespace.ts.

  const DEBUG_TRANSACTIONAL_PUBLISHING_CLIENT = false;
  const Objsheets = {};

  // Disable update batching in the DDP client to guarantee that our observers
  // are notified in the order the updates were received over the DDP
  // connection.  Note, when the DDP client calls Collection.beginUpdate with
  // batchSize = 1, the collection doesn't do pauseObservers, so the single
  // update is sent directly to our observer without reexecuting and diffing the
  // query (which would take quadratic time for a series of updates).
  //
  // This should be good enough for now; we may need a different solution if new
  // requirements arise in the future.
  //(<fixmeAny>Meteor.connection)._bufferedWritesInterval = 0;

  // The client doesn't need anything special for the Transactions collection.
  //tx.setTransactionsCollection(new Mongo.Collection<TransactionDoc>("transactions"));

  // Currently ClientTablespace pretty much assumes it is managed only by
  // ClientAppHelpers.  If and when the need arises, we can extend the
  // ClientTablespace API so that multiple callers can call `open` on the same
  // ClientTablespace with their own open callbacks, etc.
  export class ClientTablespace extends Tablespace {

    private static instances = new Map<string, ClientTablespace>();

    public static get(id: string) {
      let ts = ClientTablespace.instances.get(id);
      if (ts == null) {
        ts = new ClientTablespace(id);
        ClientTablespace.instances.set(id, ts);
      }
      return ts;
    }

    private constructor(id: string) {
      super(id);
    }

    private mirrorCollections = <{[N in SheetCollectionName]: Mongo.Collection<SheetCollectionTypes[N]>}>{};
    // If someone tries to write to a collection on the client, hopefully we get
    // a type error.  But if it happens in untyped code, it will be really
    // confusing.  So go ahead and put in a runtime check.
    private readOnlyMirrorCollections =
      <{[N in SheetCollectionName]:Map<ColumnId, Column> }>{};
    public getCollection(name: SheetCollectionName): SheetCollection<fixmeAny> {
      assert(this.initialized);
      // See "Wrinkle" comment on Tablespace.getCollection.
      return <fixmeAny>this.readOnlyMirrorCollections[name];
    }

    private startedOpen = false;
    private allSubscriptionsRequested = false;
    private subscriptions: Meteor.SubscriptionHandle[] = [];

    private setupMirrorCollection<T extends Mongo.DocWithStringId>(
      originalCollection: SimpleReadOnlyCollection<T>): Mongo.Collection<T> {
      let mirror = newLocalCollection<T>(originalCollection._name);
      originalCollection.find().observe({
        added: (document) => {
          if (DEBUG_TRANSACTIONAL_PUBLISHING_CLIENT)
            console.log("queue add", originalCollection._name, document._id);
          this.queueChange(() => mirror.insert(document));
        },
        changed: (newDocument, oldDocument) => {
          if (DEBUG_TRANSACTIONAL_PUBLISHING_CLIENT)
            console.log("queue change", originalCollection._name, newDocument._id);
          this.queueChange(() => mirror.update(newDocument._id, newDocument));
        },
        removed: (oldDocument) => {
          if (DEBUG_TRANSACTIONAL_PUBLISHING_CLIENT)
            console.log("queue remove", originalCollection._name, oldDocument._id);
          this.queueChange(() => mirror.remove(oldDocument._id));
        }
      });
      return mirror;
    }

    public open() {
      // Make this API more general later if necessary.
      assert(!this.startedOpen);
      this.startedOpen = true;
      for (let cn of sheetCollectionNames) {
          //let origCollection = sheetCollectionSpecs[cn].qualify(this.id).makeMongoCollection();
          //this.mirrorCollections[cn] = this.setupMirrorCollection<fixmeAny>(origCollection);
          this.readOnlyMirrorCollections[cn] = new Map<ColumnId, Column>();
      }
      //this.createTxCursors();
      //let txPendingCollection = Tablespace.txPendingCollectionSpec.qualify(this.id).makeMongoCollection();
      // txPendingCollection.find().observe({
      //   added: (document) => {
      //     if (DEBUG_TRANSACTIONAL_PUBLISHING_CLIENT)
      //       console.log("add pending transaction");
      //     assert(!this.inTransaction);
      //     this.inTransaction = true;
      //   },
      //   removed: (oldDocument) => {
      //     if (DEBUG_TRANSACTIONAL_PUBLISHING_CLIENT)
      //       console.log("remove pending transaction");
      //     assert(this.inTransaction);
      //     this.inTransaction = false;
      //     this.maybeDrainQueue();
      //   }
      // });

      //this.subscribeAll();
    }
    // private subscribeAll() {
    //   for (let pub of [Tablespace.tablespacePub,
    //     Tablespace.txUndoablePub, Tablespace.txRedoablePub]) {
    //     this.subscriptions.push(pub.subscribe(this.id, {
    //       onReady: () => {
    //         this.maybeDrainQueue();
    //       },
    //       onStop: (error: Error) => {
    //         if (error) {
    //           ClientAppHelpers.errorCallback(error);
    //         }
    //       }
    //     }));
    //   }
    //   this.allSubscriptionsRequested = true;
    //   this.maybeDrainQueue();
    // }

    private initialized = false;
    private inTransaction = false;
    private queue: (() => void)[] = [];
    private isConsistent() {
      // Anything that can cause this to become true should call
      // maybeDrainQueue.  XXX: We could probably use Tracker, but I'm not
      // touching it now. ~ Matt 2017-02-03
      return this.allSubscriptionsRequested &&
        this.subscriptions.every((s) => s.ready()) &&
        !this.inTransaction;
    }
    private maybeDrainQueue() {
      if (this.isConsistent()) {
        if (DEBUG_TRANSACTIONAL_PUBLISHING_CLIENT)
          console.log("drain queue");
        // Check this because pauseObservers/resumeObservers is expensive and
        // doesn't have a special case for when there are no intervening
        // changes.
        if (this.queue.length > 0) {
          for (let cn of sheetCollectionNames)
            this.mirrorCollections[cn]._collection.pauseObservers();
          for (let op of this.queue)
            op();
          this.queue = [];
          for (let cn of sheetCollectionNames)
            this.mirrorCollections[cn]._collection.resumeObservers();
        }
        if (!this.initialized) {
          this.formulaEngine_ = new FormulaEngine();
          this.initialized = true;
          ClientAppHelpers.openCallback();
        }
      }
    }
    public queueChange(op: () => void) {
      if (this.isConsistent()) {
        console.warn("Received a data update outside of a transaction!  " +
          "Applying it immediately.  The inconsistent state may break readers.");
        op();
      } else {
        this.queue.push(op);
      }
    }

    private onReconnect() {
      /* Normally when the client reconnects to the server, after the
       * onReconnect hook, the DDP client reestablishes the previously existing
       * subscriptions, waits for them to be ready ("quiescence"), and then
       * updates the client collections in bulk, pretending to the application
       * that it was just a normal subscription update.  This won't work for us
       * because we'd need to treat the tablespace as inconsistent during the
       * bulk update, but we have no clean way to detect the end of the update.
       * So instead, stop and re-request the subscriptions ourselves so that we
       * get a new set of ready callbacks.
       *
       * In terms of DDP, we are just doing the same thing the DDP client would
       * do on our behalf.  The only additional overhead is that if some
       * tablespace data hasn't been received by the time quiescence is reached,
       * it will be removed from the underlying client collections and then
       * added back, which queues extra changes to the mirror collections that
       * have no net effect when we drain the queue again.  As of 2017-02-03,
       * this never seems to happen because Meteor has internal subscriptions
       * that get re-requested after onReconnect requests the tablespace
       * subscriptions, and the server processes the subscriptions in order, so
       * the Meteor-internal subscriptions will always block quiescence until
       * after the tablespace subscriptions are ready.  In fact, the "re-request
       * all subscriptions" code sends duplicate "sub" messages for
       * subscriptions requested during onReconnect, but fortunately the server
       * ignores the duplicate messages.
       *
       * onReconnect is actually called on the first connection too (onConnect
       * might have been a better name).  If the client happens to load an app
       * far enough to call `open` before it finishes connecting to DDP, the
       * onReconnect will cancel the subscription requests from the `open`
       * before they are even sent to the server and then issue them again.
       * This is harmless.
       */
      if (DEBUG_TRANSACTIONAL_PUBLISHING_CLIENT)
        console.log("onReconnect");
      this.allSubscriptionsRequested = false;
      for (let sub of this.subscriptions) {
        sub.stop();
      }
      this.subscriptions = [];
      this.subscribeAll();
    }
    public static onReconnect() {
      for (let ts of ClientTablespace.instances.values()) {
        ts.onReconnect();
      }
    }

    public layoutsAreHardcoded() {
      return assertNotNull(LayoutsHardcodedMeta.findOne(layoutsHardcodedMetaDocId)).layoutsHardcoded;
    }

    private numOperationsInProgress_ = new ReactiveVar(0);
    public numOperationsInProgress() {
      return this.numOperationsInProgress_.get();
    }
    // CAUTION: Be careful not to call something that can throw an exception
    // between wrapCallbackForProgressIndicator and the actual server call.  This
    // is a design weakness that I may eventually address. ~ Matt 2018-08-11
    public wrapCallback<R>(cb: MeteorCallback<R>): MeteorCallback<R> {
      assert(Tracker.currentComputation == null);
      this.numOperationsInProgress_.set(this.numOperationsInProgress_.get() + 1);
      return (error, result) => {
        this.numOperationsInProgress_.set(this.numOperationsInProgress_.get() - 1);
        cb(error, result);
      };
    }

    static instance: undefined | ClientTablespace;
  }

  Tablespace.getCurrent = () => assertNotNull(ClientTablespace.instance);

  // Shortcut variable available to client-only files that refers to the same
  // object as $$ but provides access to client-only APIs.
  export declare var $$client: ClientTablespace;
  Object.defineProperty(Objsheets, "$$client", {
    get: () => assertNotNull(ClientTablespace.instance)
  });

  //(<fixmeAny>DDP).onReconnect(ClientTablespace.onReconnect);
