import { fixmeAssertNotNull, fixmeAny, fixmeUndefinable } from "./lib/typescript";
import { Column, rootColumnId, SpecialType } from "./schema";
import { FamilyDoc } from "./data";
import { Layout } from "./operations";
import { FormulaEngine } from "./formulas";
import { SimpleReadOnlyCollection } from "./lib/collections";
import { MeteorCollectionSpec } from "./lib/meteor-typescript";
import { ViewDoc } from "./view";
import { Tree } from "./lib/util";





/* A "tablespace" is the representation of a single named spreadsheet hosted
  * on the server.  The tablespace.ts files contain various data management
  * features:
  *
  * - Automatic population of new sheets from dumps containing example data
  * sets (see ServerTablespace.loadDumpIfExists).
  *
  * - The ability to publish an entire tablespace to the client (see
  * ClientTablespace.open).  (This is pretty crude but is easy for now; we'll
  * have to do something better when performance becomes a problem or we want
  * real read access control.)
  *
  * - A transaction processing mechanism on the server (ServerTablespace.run),
  * which:
  *
  * -- Serializes transactions using a per-tablespace lock (assuming only one
  * server replica is used, as with the Meteor development server).
  *
  * -- Defers complete sheet evaluation until the end of a transaction.
  *
  * -- Runs each transaction using temporary collections and commits the diff
  * to the original collections when a transaction completes successfully (see
  * ServerCollectionHandler).
  *
  * -- Records transaction history using a lightly modified version of the
  * babrahams:transactions package to support undo and redo commands.
  *
  * - A "transactional publishing" mechanism that ensures that the client code
  * only sees states of the tablespace between transactions, so we don't have
  * to clutter the client code with checks for various data inconsistencies.
  * This mechanism is pretty involved, so we give an overview here.  The client
  * code runs against a set of "mirror" collections, which are updated
  * atomically by the ClientTablespace each time a transaction completes.  The
  * ClientTablespace achieves this by watching for pending transactions in the
  * `Transactions` collection of the babrahams:transactions package (this was
  * convenient, but we could easily set up our own signaling mechanism if we
  * needed to) and queuing the data changes during a transaction until it
  * completes.  For this approach to work, the client needs to see updates to
  * `Transactions` and the actual tablespace collections in the order they
  * occur on the server, which Meteor normally does not guarantee.  We are able
  * to achieve this property by having the server publish from Minimongo
  * collections (see SynchronousObserveCollection), which perform observe
  * callbacks synchronously so the updates are enqueued on the DDP connection
  * in the original order, and disabling batching of DDP updates on the client.
  */

export type TablespaceId = string;
export const Objsheets: ObjsheetsGlobal = {};

// Interface for collections in the tablespace.  The server build augments
// this interface to make the collections writable.
export interface SheetCollection<T extends Mongo.DocWithStringId> extends SimpleReadOnlyCollection<T> {
  //findOne(arg0: { parent: import("./schema").ColumnId; fieldName: string; }): import("./schema").NonRootColumn;
}

export class PerTablespaceCollectionSpec<T extends Mongo.DocWithStringId> {
  constructor(public name: string) {}

  qualify(tsid: TablespaceId) {
    return new MeteorCollectionSpec<T>(tsid + ":" + this.name);
  }
}

// The symbolic and physical names and element types of the collections that
// make up the representation of a sheet.  We have some boilerplate here, but
// we avoid duplicating this info in the rest of the app as much as possible
// given TypeScript limitations.
//
// Unfortunately, we can't get rid of this level of indirection because a few
// spots in the code add these constants to a list and later use them to index
// into mapped types.
export namespace SheetCollections {
  // The code that sets up the global collection variables assumes they have
  // the same names as these constants.  Shadowing is harmless here.
  // tslint:disable:no-shadowed-variable
  export const Columns: "columns" = "columns";
  // For compatibility with old dumps, StateFamilies keeps the physical name
  // "families" from before the StateFamilies/AllFamilies split.  A hack, but
  // it's no harder to change later if we really want to.
  export const StateFamilies: "families" = "families";
  export const AllFamilies: "all-families" = "all-families";
  export const Views: "views" = "views";
  export const Procedures: "procedures" = "procedures";
  export const Layouts: "layouts" = "layouts";
  export const LayoutsHardcodedMeta: "layouts-hardcoded" = "layouts-hardcoded";
  // tslint:enable:no-shadowed-variable
}
export interface SheetCollectionTypes {
  [SheetCollections.Columns]: Column;
  [SheetCollections.StateFamilies]: FamilyDoc;
  [SheetCollections.AllFamilies]: FamilyDoc;
  [SheetCollections.Views]: ViewDoc;
  [SheetCollections.Procedures]: ProcedureDoc;
  [SheetCollections.Layouts]: Layout.LayoutDoc<never>;  // XXX Layout.Layout is something different.
  [SheetCollections.LayoutsHardcodedMeta]: LayoutsHardcodedMetaDoc;
}
export type SheetCollectionName = keyof SheetCollectionTypes;
export let sheetCollectionNames: SheetCollectionName[] = [];
export let sheetCollectionSpecs:
  {[N in SheetCollectionName]: PerTablespaceCollectionSpec<SheetCollectionTypes[N]>} = <fixmeAny>{};
let nickname: keyof typeof SheetCollections;
for (nickname in SheetCollections) {
  let name = SheetCollections[nickname];
  sheetCollectionNames.push(name);
  // As of TypeScript 2.8.1, statically typing this makes a bigger mess than
  // it's worth.
  sheetCollectionSpecs[name] = new PerTablespaceCollectionSpec<fixmeAny>(name);
}

// I'd like to define these using accessor syntax, but that's only supported
// in classes and "A namespace declaration cannot be in a different file from
// a class or function with which it is merged". ~ Matt 2016-03-28
// Or "const", but would that let the compiler incorrectly assume that the
// values don't change? ~ Matt 2016-10-11
declare interface ObjsheetsGlobal {
  $$?: Tablespace;
}
/*export declare let $$: Tablespace;
Object.defineProperty(Objsheets, "$$", {
  get: () => Tablespace.getCurrent()
});*/

var tmp: Column =
{
  _id: rootColumnId,
  parent: null,
  children: null,
  fieldName: null,
  specifiedType: SpecialType.TOKEN,
  isObject: true,
  objectName: null,
  formula: null,
  referenceDisplayColumn: null,
  view: null,
  singular: false,
  isViewObjectRoot: false,
  viewObjectType: null,
  userVisible: true,
  cachedType: SpecialType.TOKEN,
  cachedTypecheckError: null,
  cachedIsReferenced: false,
};

let dummy: {[N in keyof typeof SheetCollections]:
  SheetCollection<SheetCollectionTypes[(typeof SheetCollections)[N]]>};
export declare let Columns: typeof dummy.Columns;
export declare let StateFamilies: typeof dummy.StateFamilies;
export declare let AllFamilies: typeof dummy.AllFamilies;
export declare let Views: typeof dummy.Views;
export declare let Procedures: typeof dummy.Procedures;
export declare let Layouts: typeof dummy.Layouts;
export declare let LayoutsHardcodedMeta: typeof dummy.LayoutsHardcodedMeta;
for (nickname in SheetCollections) {
  let nickname1 = nickname;
  Object.defineProperty(Objsheets, nickname, {
    get: () => Objsheets.$$.getCollection(SheetCollections[nickname1])
  });
}

export const layoutsHardcodedMetaDocId = "meta";
export type LayoutsHardcodedMetaDoc = {
  _id: typeof layoutsHardcodedMetaDocId;
  layoutsHardcoded: boolean;
};

export const txPendingDocId = "pending";
export type TxPendingDoc = {
  _id: typeof txPendingDocId;
};

export abstract class Tablespace {
  //Columns: any;
  constructor(public id: string) {
    console.log(`created Tablespace[${this.id}]`);
  }

  // For strictNullChecks.  I don't see an obvious better way. ~ Matt 2018-08-02
  protected formulaEngine_: undefined | FormulaEngine;
  public get formulaEngine() { return fixmeAssertNotNull(this.formulaEngine_); }

  /* We'd like to say:
    *   public abstract getCollection<N extends SheetCollectionName>(name: N):
    *     SheetCollection<SheetCollectionTypes[N]>;
    * to require that the collection has the right element type, but TypeScript
    * reports lots of type errors in code that uses a variable like "N extends
    * SheetCollectionName" with a concrete mapped type, which may be because we
    * have no way to state that N is a singleton type.  By the time we cast
    * around all the type errors, there seems to be no practical advantage over
    * the following simpler signature.  TODO: File TypeScript bugs/RFEs to get
    * this to work the way we want it to.
    *
    * Wrinkle: SheetCollection is intended to be read-only on the client, but
    * when the client code is compiled in the all-in-one project,
    * SheetCollection is writable.  We can cast to SheetCollection even though
    * it's a lie from the point of view of the all-in-one project; I don't have
    * a better idea. ~ Matt 2017-01-26
    */
  public abstract getCollection(name: SheetCollectionName): SheetCollection<fixmeAny>;

  // To be defined separately on client and server.
  public static getCurrent: () => Tablespace;

  protected transactionContext() { return {sheet: this.id}; }

  //protected static readonly tablespacePub = new MeteorPublication1<TablespaceId>("tablespace");
  protected static readonly txPendingCollectionSpec =
    new PerTablespaceCollectionSpec<TxPendingDoc>("tx-pending");
  //public static readonly viewObjectPub =
    //new MeteorPublication3<TablespaceId, /*clientViewObjectId*/ string, ViewObjectRequest>("view-object");
  //protected static readonly txUndoablePub = new MeteorPublication1<TablespaceId>("tx-undoable");
 // protected static readonly txRedoablePub = new MeteorPublication1<TablespaceId>("tx-redoable");

  protected txUndoableCursor: fixmeUndefinable<Mongo.Cursor<fixmeAny>>;  // not TransactionDoc: fields are filtered
  protected txRedoableCursor: fixmeUndefinable<Mongo.Cursor<fixmeAny>>;
  public canUndo(): boolean { return fixmeAssertNotNull(this.txUndoableCursor).count() > 0; }
  public canRedo(): boolean { return fixmeAssertNotNull(this.txRedoableCursor).count() > 0; }

  /*
  protected createTxCursors() {
    // The server's cursors have to return the fields that the client needs to
    // evaluate these selectors.  For now, just return everything except
    // "items" on both sides.
    //
    // We'd like "limit: 1", but on the server, cursor publishing does an
    // unordered observe, which SynchronousObserveCollection (backed by
    // minimongo) doesn't support when a limit is specified.  Remove the
    // limit unless/until we need the performance back.
    this.txUndoableCursor = tx.Transactions.find(
      _.extend({context: this.transactionContext()}, tx.undoableSelector),
      {fields: {items: 0}});
    this.txRedoableCursor = tx.Transactions.find(
      _.extend({context: this.transactionContext()}, tx.redoableSelector),
      {fields: {items: 0}});
  }
  */

  public abstract layoutsAreHardcoded(): boolean;
}

//export namespace MeteorMethods {
//  export let undo = new MeteorMethod1<TablespaceId, void>("undo");
//  export let redo = new MeteorMethod1<TablespaceId, void>("redo");
//}


