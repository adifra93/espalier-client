import { fixmeAny } from "./lib/typescript";

// Type declarations for various external APIs used by Object Spreadsheets in
// addition to the declarations we get from upstream sources.  Additional
// declarations for a library for which we have an upstream declaration file can
// go in this file or as modifications to the original declaration file,
// whatever seems easier to understand.

interface ObjectConstructor {
  // Copied from TypeScript lib.es2015.core.d.ts.  I verified that our target
  // runtimes all have it, but I don't know if they have all of
  // lib.es2015.core.d.ts to add it to our library set in tsconfig.json.
  // ~ Matt 2017-02-10
  setPrototypeOf(o: any, proto: any): any;
}

// Stubs for now.

// I didn't find a type definition for Jison at all.  We could start one.
// ~ Matt 2016-02-28, rechecked 2016-09-19
export declare var Jison: fixmeAny;
export declare interface JisonLocation {
  first_line: number;
  last_line: number;
  first_column: number;
  last_column: number;
}

// I didn't find a type definition. ~ Matt 2016-11-30
/*declare var tx: fixmeAny;
type TransactionDoc = fixmeAny;

declare module Meteor {
  var EnvironmentVariable: EnvironmentVariableStatic;
  interface EnvironmentVariableStatic {
    new<T>(): EnvironmentVariable<T>;
  }
  interface EnvironmentVariable<T> {
    get(): undefined | T;
    withValue<R>(value: T, func: () => R): R;
  }

  // "this" for Meteor method implementations, as per the documentation.
  interface MethodInvocation {
    userId: string;
    setUserId: (userId: string) => void;
    isSimulation: boolean;
    unblock: () => void;
    connection: Connection;
  }

  // Client only.  DDP.DDPStatic is a pretty bad name for "connection from the
  // client's point of view". ~ Matt 2017-02-02
  var connection: DDP.DDPStatic;
}

// So we can start typing bits of our own code more precisely.
// TODO: Better name.  Meteor is not the only system using this style of
// callback.
// TODO: Integrate this properly into meteor.d.ts (at least for our own use,
// maybe later for upstreaming.) ~ Matt 2016-03-17
//
// XXX: We can't express that `result` is passed if `error` is null.  So force
// most implementations to explicitly cast `result` non-undefined. :(
type MeteorCallback<R> = (error: null | Error, result?: R) => void;

declare module Blaze {
  function _reportException(e: Error, msg: string): void;
}

declare module Mongo {
  interface Collection<T extends Doc> {
    _name: string;
    // The minimongo LocalCollection or the wrapper object generated in
    // RemoteCollectionDriver.open.  Not the same as rawCollection.
    _collection: fixmeAny;

    _dropCollection(): void;
  }
}

// datejs library modifies the global Date object.
// https://code.google.com/archive/p/datejs/wikis/APIDocumentation.wiki
// Is there a currently maintained copy of the documentation?
declare interface Date {
  toString(format: string): string;
}
declare interface DateConstructor {
  // Format can be an array, but we don't use that.
  parseExact(dateString: string, format: string): null | Date;
}

// minimongo package
declare var LocalCollection: fixmeAny;
declare namespace LocalCollection {
  type _IdMap<T> = fixmeAny;
}
//type LocalCollection<T> = fixmeAny;

// diff-sequence package
declare var DiffSequence: fixmeAny;

// iron-location package et al.
declare var Iron: fixmeAny;

// The React typing needs this single DOM declaration that is missing from
// TypeScript's lib.dom.d.ts, presumably due to version skew.  We could
// include React's global.d.ts, but this is easy enough. ~ Matt 2018-01-03
declare interface HTMLWebViewElement extends HTMLElement {}

// react-meteor-data package
declare var ReactMeteorData: fixmeAny;*/
