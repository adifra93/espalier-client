import { Flatten } from "./lib/collections";
import { ColumnId, rootColumnId, Column } from "./schema";
import { TablespaceId, Views, SheetCollectionName, SheetCollectionTypes } from "./tablespace";
import { fixmeAssertNotNull } from "./lib/typescript";
import { Tree } from "./lib/util";
import { Token } from "./lib/type-funcs";

export enum RootColumnId { ROOT = "_root" }

enum SpecialType {
  // The empty type, subtype of all other types, used for literal empty sets, etc.
  // Not allowed for state columns.
  EMPTY = "empty",

  ERROR = "error",

  // Main primitive types
  TEXT = "text",
  NUMBER = "number",
  BOOL = "bool",
  DATE = "date",

  TOKEN = "_token",  // special primitive, should never be user-visible
  UNIT = "_unit",  // primitive, deprecated but still used in ptc
}

export const tmp: Column =
{
  _id: RootColumnId.ROOT,
  parent: null,
  children: null,
  fieldName: null,
  specifiedType: SpecialType.TOKEN,
  isObject: true,
  objectName: null,
  formula: null,
  referenceDisplayColumn: null,
  view: null,
  singular: false,
  isViewObjectRoot: false,
  viewObjectType: null,
  userVisible: true,
  cachedType: SpecialType.TOKEN,
  cachedTypecheckError: null,
  cachedIsReferenced: false,
};
class Table {
  Columns : Map<ColumnId, Column>

  constructor(id: ColumnId, data: Column){
    this.Columns = new Map<ColumnId, Column>().set(id, data); 
  }

  get(id: ColumnId): Column {
    return this.Columns.get(RootColumnId.ROOT);
  
}

getDocument<N extends SheetCollectionName>(collectionName: N, id: string):
      undefined | SheetCollectionTypes[N] {
        return getCollection(collectionName).findOne(id);
}
}
export let $$: Table = new Table(rootColumnId, tmp);

 
  export type ViewDef = {
    layout: Tree<ColumnId>;
  };
  export type ViewDoc = Flatten<ViewDef & {_id: string}>;

  // Server write API is in server/view.ts.  (The only write called directly
  // from the client is reorderColumn, and it's a direct Meteor method call; I
  // don't see value in providing a wrapper in this case. ~ Matt 2017-01-24)
  export class View {
    // `id = null` refers to the default view.
    constructor(public id: null | string) {}

    public def(): ViewDef {
      return this.id != null ? Views.findOne(this.id) || {
        layout: new Tree<ColumnId>(rootColumnId)
      } : {
        layout: View.rootLayout()
      };
    }

    public static rootLayout() {
      return fixmeAssertNotNull(this.drillDown(rootColumnId).filter((x) => this.ownerOf(x) == null));
    }

    public static drillDown(startingColumnId: ColumnId): Tree<ColumnId> {
      let startingCol = $$.Columns.get(startingColumnId);
      let children = (startingCol != null ? startingCol.children : null) || [];
      return new Tree(startingColumnId, children.map((child) => this.drillDown(child)));
    }

    public static ownerOf(columnId: ColumnId) {
      let col = $$.Columns.get(columnId);
      return col != null ? col.view : null;
    }
  }

  export namespace MeteorMethods {
    //export let viewReorderColumn = new MeteorMethod4<TablespaceId, string, ColumnId, number, void>("viewReorderColumn");
  }


