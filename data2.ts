import { ReadableModel } from "./formulas";
import { FamilyHandle, TypedSet, $$, CellHandle } from "./data";
import { SheetCollections } from "./tablespace";
import { assertNotNull } from "./assert";
import { getExistingColumn } from "./schema";


  let liteModel: ReadableModel = {
    evaluateFamily: (qFamilyId) => {
      let fam = new FamilyHandle(qFamilyId);
      let doc = $$.getDocument(SheetCollections.AllFamilies, fam.docId());
      return (doc != null && doc.values != null) ? new TypedSet(fam.type(), set(doc.values)) : null;
    },
    evaluateAll: () => {},
    // The one place it's OK to read cachedType on the client.  (getColumnType
    // calls this.)
    typecheckColumn: (columnId) => assertNotNull(getExistingColumn(columnId).cachedType),
    typecheckAll: () => {}
  };
  export var getReadableModel = liteModel;

  export interface FamilyHandle {
    //new_Client(callback: MeteorCallback<OSValue>): void;
  }

  export interface CellHandle {
    //add_Client(callback: MeteorCallback<void>): void;
    //setValue_Client(newValue: OSValue, callback: MeteorCallback<void>): void;
    //remove_Client(callback: MeteorCallback<void>): void;
  }

  function getCurrentClientViewObjectId() {
    let ctx = currentViewObject.get();
    if (ctx == null) return null;
    let pfx = (Meteor.connection as fixmeAny)._lastSessionId + ":";
    assert(ctx.id.startsWith(pfx));
    return ctx.id.substring(pfx.length);
  }
  //export let autoCleanup = new Meteor.EnvironmentVariable<boolean>();

  // Meteor.startup(() => {  // load order for CellHandle, FamilyHandle

  //   // The client assumes a fully evaluated sheet.
  //   FamilyHandle.prototype.read = function() {
  //     let doc = $$.getDocument(SheetCollections.AllFamilies, this.docId());
  //     return runtimeCheckNotNull(doc, () => "Read of family of nonexistent/erroneous object");
  //   };

    FamilyHandle.prototype.new_Client = function(callback) {
      MeteorMethods.addCellRecursive.call($$.id,
        {
          columnId: this.columnId,
          ancestorQCellId: this.parent().id(),
          value: null,  // new
          newColumnType: null,
          viewId: null,  // unused when newColumnType == null
          clientViewObjectId: getCurrentClientViewObjectId(),
          // fallback: Yuck.  Meteor should let us specify a default value for
          // an environment variable.  Submit a pull request?
          cleanup: fallback(autoCleanup.get(), false)
        },
        (error, result) => callback(error, result == null ? undefined : result.value)
      );
    };

    CellHandle.prototype.add_Client = function(callback) {
      if (this.columnId === rootColumnId)  // narrowing
        throw new Error("CellHandle.add_Client on root");
      MeteorMethods.addCellRecursive.call($$.id,
        {
          columnId: this.columnId,
          ancestorQCellId: fixmeAssertNotNull(this.parent()).id(),
          value: this.value(),
          newColumnType: null,
          viewId: null,  // unused when newColumnType == null
          clientViewObjectId: getCurrentClientViewObjectId(),
          cleanup: fallback(autoCleanup.get(), false)
        },
        // The result carries no new information, so discard it.
        (error, result) => callback(error, undefined)
      );
    };

    CellHandle.prototype.setValue_Client = function(newValue, callback) {
      MeteorMethods.replaceCell.call($$.id, this.id(), newValue, getCurrentClientViewObjectId(),
        fallback(autoCleanup.get(), false), callback);
    };

    CellHandle.prototype.remove_Client = function(callback) {
      MeteorMethods.recursiveDeleteCell.call($$.id, this.id(), getCurrentClientViewObjectId(),
        fallback(autoCleanup.get(), false), callback);
    };

  });


