import $ from 'jquery';
import { ClientView, defaultSheetOptions } from './sheet';
import { ClientTablespace } from './client/tablespace';
import { View } from './view';


function main() {
    ClientTablespace.instance = ClientTablespace.get('whatever');
    ClientTablespace.instance.open();
    var clientView =
        new ClientView(null, new View(null), 
            defaultSheetOptions, document.querySelector('#View'));

    
    // for development
    Object.assign(window, {clientView});
}

$(main);

// for development
Object.assign(window, {ClientTablespace, ClientView, defaultSheetOptions, View});