import { assertNotNull } from "../assert";

// Use to suppress TypeScript errors in code for which we have not yet figured
// out the proper typing, so we can maintain zero errors and improve the typing
// later as desired. ~ Matt 2016-02-28, revised 2017-09-20
//
// This must be outside the Objsheets namespace because it's referenced by some
// typing files.
//
// tslint:disable-next-line:no-any
export type fixmeAny = any;

interface ObjectConstructor {
  // Special typing of Object.assign when  we want to assign to properties
  // already in the target type, not add more properties to the target to give
  // it a new type.
  assignTo<T>(target: T, source: Partial<T>): T;
}
// @ts-ignore
Object.assignTo = Object.assign;


  export function isNotNull<T>(value: T): value is NonNullable<T> {
    return value != null;
  }

  export function undefinedToNull<T>(value: T | undefined) {
    return (value === undefined) ? null : value;
  }

  export function indexOfSupertype<T extends U, U>(arr: T[], needle: U): number {
    const upcastArr: U[] = arr;
    return upcastArr.indexOf(needle);
  }
  export function arrayContainsGuard<T extends U, U>(arr: T[], needle: U): needle is T {
    return indexOfSupertype(arr, needle) >= 0;
  }

  // Use to get an element from an array that might be undefined.
  export function arrayGet<T>(arr: T[], index: number): undefined | T {
    return arr[index];
  }

  // strictNullChecks transitional stuff:

  // A null check that we might be able to remove with better typings.
  // Analogous to fixmeAny.  The threshold for plausibility of removal to use
  // fixmeAssertNotNull rather than assertNotNull is rather vague.
  export const fixmeAssertNotNull = assertNotNull;

  // Something that is currently nullable but might merit redesign to make it
  // non-nullable.
  export type fixmeNullable<T> = null | T;
  export type fixmeUndefinable<T> = undefined | T;

  // To stop an interface from being covariant in one or more type parameters as
  // a result of TypeScript's bivariant treatment of method parameters, have it
  // extend NotCovariant of those type parameters.  (For a class, define an
  // interface that merges with it.)
  const NOT_COVARIANT_DUMMY = Symbol();
  export interface NotCovariant<T1 = {}, T2 = {}, T3 = {}, T4 = {}, T5 = {}, T6 = {}> {
    [NOT_COVARIANT_DUMMY]?: (t1: T1, t2: T2, t3: T3, t4: T4, t5: T5, t6: T6) => void;
  }

  export type Brander<B> = <T>(arg: T) => T & B;
  export function makeBrander<B>(): Brander<B> {
    return (arg) => <typeof arg & B>arg;
  }

