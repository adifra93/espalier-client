import { ColumnId, getExistingColumn, rootColumnId, Column, typeIsReference, OSType, typeIsSpecial, NonRootColumnId, NonRootColumn, getColumn } from "./schema";
import { fallback } from "./fallback";
import { Columns } from "./tablespace";
import { SemanticError } from "./misc";

  export function columnDepth(columnId: ColumnId): number {
    return columnId === rootColumnId ? 0 : 1 + columnDepth(getExistingColumn(columnId).parent);
  }

  // All readers should handle null or document why they don't.
  export function objectOrBracketedFieldName(col: Column): null | string {
    return fallback(col.objectName, (col.fieldName != null ? `[${col.fieldName}]` : null));
  }

  export function fieldNameForUI(col: Column): string {
    return fallback(col.fieldName, "(unnamed)");
  }
  export function objectNameForUI(col: Column): string {
    return (col._id == rootColumnId) ? "(top level of sheet)" :
      fallback(objectOrBracketedFieldName(col), "(unnamed)");
  }
  export function fullObjectNameForUI(columnId: ColumnId): string {
    return (columnId == rootColumnId) ? "(top level of sheet)" :
      stringifyColumnRef([columnId, false]);
  }

  // All readers should handle null or document why they don't.
  export function objectOrFieldName(col: Column): null | string {
    return fallback(col.objectName, col.fieldName);
  }

  export function nextAvailableColumnName(prefix: string): string {
    let i = 1;
    while (Columns.find({
        $or: [
          {
            fieldName: `${prefix}${i}`
          }, {
            objectName: `${prefix}${i}`
          }
        ]
      }).count() > 0) {
      i++;
    }
    return `${prefix}${i}`;
  }

  type LogicalChildInfo = [ColumnRef, /* "up" or "down" */ string];

  // This implements the user-facing conceptual model in which a key is a child of the object.
  // Precondition: getColumn(id).isObject == true
  // Having direction here is a little awkward but lets us reuse this in resolveNavigation.
  export function columnLogicalChildrenByName(id: ColumnId, wantName: string): LogicalChildInfo[] {
    let col = getExistingColumn(id);
    let ret: LogicalChildInfo[] = [];
    function visit(name: null | string, descriptor: LogicalChildInfo) {
      if (name === wantName) {
        ret.push(descriptor);
      }
    }
    visit(col.fieldName, [[id, true], "up"]);
    for (let physChildCol of Columns.find({
      parent: id
    }).fetch()) {
      if (physChildCol.isObject) {
        visit(objectOrBracketedFieldName(physChildCol), [[physChildCol._id, false], "down"]);
      } else {
        visit(physChildCol.fieldName, [[physChildCol._id, true], "down"]);
      }
    }
    return ret;
  }

  export function stringifyType(type: OSType): string {
    return typeIsReference(type) ? stringifyColumnRef([type, false]) : type;
  }

  export function parseTypeStr(s: string): OSType {
    return typeIsSpecial(s) ? s : parseObjectTypeRef(s);
  }

  export function parseObjectTypeRef(s: string): ColumnId {
    let colId2 = parseColumnRef(s);
    if (colId2[1]) {
      throw new SemanticError(`'${s}' refers to a value column, not an object type.`);
    }
    return colId2[0];
  }

  export function parseFieldRef(s: string): NonRootColumnId {
    let colId2 = parseColumnRef(s);
    if (!colId2[1]) {
      throw new SemanticError(`'${s}' refers to an object type, not a value column.`);
    }
    return <NonRootColumnId>colId2[0];
  }

  // Compare to resolveNavigation.
  type ColumnRef = [ColumnId, /* isValues */ boolean];
  export function parseColumnRef(s: string): ColumnRef {
    if (!s) {
      // What we currently want for user-facing usage.  load-sample-data has to
      // override this behavior. :/
      throw new SemanticError("We currently do not support references to the root column.");
    }
    let colId2: ColumnRef = [rootColumnId, false];
    for (let n of s.split(":")) {
      if (colId2[1]) {
        throw new SemanticError(`Looking up child '${n}' of a value column.`);
      }
      let interpretations = columnLogicalChildrenByName(colId2[0], n);
      if (interpretations.length !== 1) {
        throw new SemanticError(`${interpretations.length} interpretations for ` +
          `${stringifyColumnRef(colId2)}:${n}, wanted one.`);
      }
      colId2 = interpretations[0][0];
    }
    return colId2;
  }

  // Compare to stringifyNavigation.
  export function stringifyColumnRef([columnId, isValues]: ColumnRef): string {
    if (columnId === rootColumnId) {
      // XXX Consider reenabling the error after more testing. ~ Matt 2015-11-25
      //throw new SemanticError('We currently do not support references to the root column.')
      return "$";
    }
    let names: string[] = [];
    while (columnId !== rootColumnId) {
      let col: undefined | NonRootColumn = getColumn(columnId);
      if (col == null) {
        return "(deleted)";
      }
      let name = isValues ? col.fieldName : objectOrBracketedFieldName(col);
      let logicalParent: ColumnId = isValues && col.isObject ? columnId : col.parent;
      if (name != null) {
        if (columnLogicalChildrenByName(logicalParent, name).length !== 1) {
          name += "(ambiguous)";
        }
      } else {
        name = "(unnamed)";
      }
      names.unshift(name);
      isValues = false;
      columnId = logicalParent;
    }
    return names.join(":");
  }

