import _ from 'underscore';
import $ from 'jquery';
import * as EJSON from 'ejson';
import ReactiveVar from 'meteor-standalone-reactive-var';

import Handsontable from 'handsontable/dist/handsontable.full';
import "handsontable/dist/handsontable.full.css";

import { assert } from './assert';
import { fixmeAssertNotNull, arrayGet, fixmeNullable, fixmeAny } from './lib/typescript';
import { fallback } from './fallback';
import { $$client } from './client/tablespace';
import { DUMMY_FORMULA } from "./formulas";
import { standardServerCallback } from './client/ui';
import { JSONSet, JSONKeyedMap } from "./lib/util";

import { getColumnType, OSType, Column, SpecialType, typeIsReference,
         ColumnId, getExistingColumn, rootColumnId, NonRootColumnId, getColumn,
         columnIsPotentiallyEditable } from "./schema";
import { Columns } from "./tablespace";
import { CellId, FamilyHandle, QCellId, OSValue, cellIdChild, getReadableModel, CellHandle } from "./data";
import { objectOrBracketedFieldName, nextAvailableColumnName } from "./schema-ui";
import { Tree } from "./lib/util";
import { valueToText, parseValue } from "./data-ui";
import { ViewCell, gridVertExtend, gridBottomRow, gridVertStretch, gridMergedCell, ViewGrid, gridHorizExtend, gridRepeatedCell, gridBottomRowWithColNums } from "./grid";
import { Router } from './typings/iron-router/main';
import { View } from './view';



  interface SpreadsheetTemplateData {
    sheet: string;
    viewId?: string;
    printable: boolean;
  }
/*  Router.route("/:sheet", function(this: fixmeAny) {
    let data: SpreadsheetTemplateData = {
      sheet: this.params.sheet,
      printable: this.params.query.printable != null
    };
    this.render("Spreadsheet", {data: data});
  });
  Router.route("/:sheet/views/:_id", function(this: fixmeAny) {
    let data: SpreadsheetTemplateData = {
      sheet: this.params.sheet,
      viewId: this.params._id,
      printable: this.params.query.printable != null
    };
    this.render("Spreadsheet", {data: data});
  });*/

  // Object that can be used as ViewCell.value or ViewHlist.value to defer the
  // resolution of the target cell ID to a row number.  I'm a terrible person for
  // taking advantage of heterogeneous fields in JavaScript... ~ Matt
  export class CellReference {
    constructor(public qCellId: QCellId, public display: string) {}
  }
  export type ViewValue = string | CellReference;

  export function stringifyTypeForSheet(type: OSType) {
    let col: undefined | Column, name: null | string;
    if (type === SpecialType.UNIT) {
      return "X";
    } else if (!typeIsReference(type)) {
      return type;
    } else if (typeIsReference(type) &&
      ((col = Columns.findOne(type)) != null) && ((name = objectOrBracketedFieldName(col)) != null)) {
      // XXX May be ambiguous.
      return name;
    } else {
      return "<?>";
    }
  }

  export function markDisplayClassesForType(type: OSType) {
    return type === SpecialType.UNIT ? ["centered"] : [];
  }

  class LayoutNode {
    // Additional columns to the right that will be part of this layout node
    public spareColumns = 0;
    // Whether a single item in a vlist (family) stretches to the height of the
    // entire family.
    public stretchV = true;
    public blankRowsClass = "dataPadding";

    constructor(public columnId: ColumnId) { }
  }

  class ViewVlist {
    constructor(public parentCellId: CellId, public minHeight: number,
      public hlists: null /*error*/ | ViewHlist[], public numPlaceholders = 0,
      public error: null | string = null) {}
  }

  class ViewHlist {
    constructor(public cellId: CellId, public minHeight: number,
      public value: null /*error*/ | ViewValue, public error: null | string, public vlists: ViewVlist[]) {}
  }

  class ViewSection {
    public columnId: ColumnId;
    public col: Column;
    public showBullets: boolean;
    public width: number;
    public subsections: ViewSection[];
    // headerHeightBelow and headerMinHeight refer to the expanded header.
    public headerHeightBelow: number;
    public headerMinHeight: number;

    constructor(public template: SpreadsheetTemplateInstance,
      public layoutTree:Tree<LayoutNode>, public sheetOptions: SheetOptions) {
      this.columnId = this.layoutTree.root.columnId;
      this.col = getExistingColumn(this.columnId);
      // Future: Consider rendering _unit with isObject = true specially to save
      // space, e.g., a single column of hollow bullets.  We'd need to figure out
      // how to make this not confusing.
      this.showBullets = this.col.isObject &&
        (sheetOptions.showBullets || this.layoutTree.subtrees.length == 0);
      this.width = (getColumnType(this.col) !== SpecialType.TOKEN ? 1 : 0) + (this.showBullets ? 1 : 0);
      this.subsections = [];
      this.headerHeightBelow = 2;  // fieldName, type
      this.layoutTree.subtrees.forEach((sublayout, i) => {
        let subsection = new ViewSection(template, sublayout, sheetOptions);
        this.subsections.push(subsection);
        this.width += subsection.width;
        this.headerHeightBelow = Math.max(this.headerHeightBelow, subsection.headerMinHeight);
        if (this.haveTableSeparatorAfter(i)) {
          this.width++;
        }
      });
      this.width += this.layoutTree.root.spareColumns;
      this.headerMinHeight = (this.col.isObject && this.columnId !== rootColumnId ? 1 : 0) + this.headerHeightBelow;
      if (this.col.isObject) {
        // Affects empty sheet.
        this.headerMinHeight = Math.max(this.headerMinHeight, 3);
      }
    }

    private haveTableSeparatorAfter(subsectionI: number) {
      // This is a little fiddly but the alternatives seem to be more invasive. ~ Matt 2016-09-28
      return this.columnId == rootColumnId &&
        (subsectionI < this.layoutTree.subtrees.length - 1 ||
        this.layoutTree.root.spareColumns > 0);
    }

    public prerenderVlist(parentCellId: CellId): ViewVlist {
      let qFamilyId = {
        columnId: <NonRootColumnId>this.columnId,
        parentCellId: parentCellId
      };
      let ce = new FamilyHandle(qFamilyId).read();
      if (ce.values != null) {
        let hlists = ce.values.map((value: OSValue) => this.prerenderHlist(cellIdChild(parentCellId, value), value));
        let minHeight = 0;
        for (let hlist of hlists) {
          minHeight += hlist.minHeight;
        }
        // Don't add any placeholders automatically: it's looking ridiculous.  Once
        // we know which columns are plural, we can reconsider adding extra rows.
        let numPlaceholders = EJSON.equals(qFamilyId, this.template.placeholderFamily.get()) ? 1 : 0;
        minHeight += numPlaceholders;
        return new ViewVlist(parentCellId, minHeight, hlists, numPlaceholders, null);
      } else {
        // We should only ever look at fully evaluated sheets.
        assert(ce.error != null);
        return new ViewVlist(parentCellId, 1, null, undefined, ce.error);
      }
    }

    public prerenderHlist(cellId: CellId, value: OSValue): ViewHlist {
      let minHeight = 1, displayValue, error: null | string;
      try {
        let displayString = valueToText(getReadableModel(), getColumnType(this.col), value);
        let type = getColumnType(this.col);
        if (typeIsReference(type)) {
          displayValue = new CellReference({
            columnId: type,
            cellId: <CellId>value  // because typeIsReference(this.col.type)
          }, displayString);
        } else {
          displayValue = displayString;
        }
        error = null;
      } catch (e) {
        displayValue = null;
        error = e.message;
      }
      let vlists = this.subsections.map((subsection: ViewSection) => subsection.prerenderVlist(cellId));
      minHeight = Math.max(1, ...vlists.map((vlist) => vlist.minHeight));
      return new ViewHlist(cellId, minHeight, displayValue, error, vlists);
    }

    public renderVlist(vlist: ViewVlist, height: number) {
      let ancestorQCellId = {
        columnId: this.col.parent!,
        cellId: vlist.parentCellId
      };
      let grid: ViewCell[][];
      if (vlist.hlists != null) {
        grid = [];
        for (let hlist of vlist.hlists) {
          gridVertExtend(grid, this.renderHlist(hlist, ancestorQCellId, hlist.minHeight));
        }
        for (let cell of gridBottomRow(grid)) {
          cell.cssClasses.push("vlast");
        }
        for (let i = 0; i < vlist.numPlaceholders; i++) {
          // This can occur for leaves and state keyed objects.
          let gridPlaceholder = this.renderHlist(null, ancestorQCellId, 1, "dataPadding");
          gridVertExtend(grid, gridPlaceholder);
        }
        // Fill down to 'height', either by stretching or with blank rows
        if (grid.length < height) {
          // In theory, this could now be based on this.col.singular, but the
          // old UI is in maintenance mode. ~ Matt 2018-01-30
          if (grid.length === 1 && this.layoutTree.root.stretchV) {
            gridVertStretch(grid, height);
          } else {
            gridVertExtend(grid, this.renderBlankRows(height - grid.length, grid, ancestorQCellId));
          }
        }
      } else {
        grid = gridMergedCell(height, this.width, "error", ["dataError"]);
        grid[0][0].fullText = "Error: " + vlist.error;
      }
      return grid;
    }

    public markDisplayClasses() {
      return markDisplayClassesForType(getColumnType(this.col));
    }

    // Only applicable if @col.isObject.

    public objectSymbol() {
      return this.col._id === rootColumnId ? "" : getColumnType(this.col) === SpecialType.TOKEN ? "•" : "◦";
    }

    public findTypesToColor(typesToColor: JSONSet<ColumnId>) {
      let type = getColumnType(this.col);
      if (typeIsReference(type)) {
        typesToColor.add(type);
      }
      for (let subsection of this.subsections) {
        subsection.findTypesToColor(typesToColor);
      }
    }

    public assignTypeColors(nextColor: number, typesToColor: JSONSet<ColumnId>,
      typeColors: JSONKeyedMap<ColumnId, number>) {
      if (typesToColor.has(this.columnId)) {
        typeColors.set(this.columnId, nextColor++);
      }
      for (let subsection of this.subsections) {
        nextColor = subsection.assignTypeColors(nextColor, typesToColor, typeColors);
      }
      return nextColor;
    }

    // Can be called with hlist == null for an empty row.
    // In that case, supply paddingCssClass (should be either "dataPadding" or "spareRow")
    // and ancestorQCellId.
    public renderHlist(hlist: null | ViewHlist, ancestorQCellId: null | QCellId, height: number,
      paddingCssClass?: string) {
      // --noImplicitAny missed the lack of an annotation here:
      // https://github.com/Microsoft/TypeScript/issues/531#issuecomment-279160623
      let grid: ViewGrid = _.range(0, height).map((i) => []);
      let qCellId = (hlist == null) ? null : {
        columnId: this.columnId,
        cellId: hlist.cellId
      };
      if (this.showBullets) {
        // Object
        let gridObject = gridMergedCell(height, 1,
          hlist == null ? "" : this.objectSymbol(),
          [hlist == null ? fixmeAssertNotNull(paddingCssClass) : "centered"]);
        gridObject[0][0].ancestorQCellId = ancestorQCellId;
        gridObject[0][0].addColumnId = (this.columnId === rootColumnId) ? null : this.columnId;
        gridObject[0][0].qCellId = qCellId;
        gridObject[0][0].isObjectCell = true;
        // For debugging and calling canned transactions from the console.
        /*
        if (hlist!=null){
          gridObject[0][0].fullText = 'Object ID: ' + JSON.stringify(hlist.cellId);
        }
        */
        if (getColumnType(this.col) !== SpecialType.TOKEN) {
          gridObject[0][0].cssClasses.push("rsKeyedObject");
        }
        gridHorizExtend(grid, gridObject);
      }
      if (getColumnType(this.col) !== SpecialType.TOKEN) {
        // Value
        let gridValue = gridMergedCell(height, 1,
          hlist == null ? "" : fallback(hlist.value, "<?>"),
          hlist == null ? [fixmeAssertNotNull(paddingCssClass)] : []);
        gridValue[0][0].ancestorQCellId = ancestorQCellId;
        gridValue[0][0].addColumnId = (this.columnId === rootColumnId) ? null : this.columnId;
        gridValue[0][0].qCellId = qCellId;
        if (hlist != null) {
          // The key of a keyed object should not have dotted borders to the
          // keys of sibling objects.  If we were really emulating
          // single-element key families, we would want to put vlast on the key,
          // but this is easier.
          if (this.subsections.length === 0 && !this.col.isObject) {
            gridValue[0][0].cssClasses.push("leaf");
          }
          if (hlist.value != null) {
            for (let displayClass of this.markDisplayClasses()) {
              gridValue[0][0].cssClasses.push(displayClass);
            }
            if (typeIsReference(getColumnType(this.col))) {
              gridValue[0][0].cssClasses.push("reference");
            }
          }
          if (hlist.error != null) {
            gridValue[0][0].cssClasses.push("dataError");
            gridValue[0][0].fullText = "Error converting to text: " + hlist.error;
          }
        }
        gridHorizExtend(grid, gridValue);
      }
      // Subsections
      this.subsections.forEach((subsection, i) => {
        let subsectionGrid =
          (hlist == null)
            ? subsection.renderHlist(null, ancestorQCellId, height, paddingCssClass)
            // Establishes qCellId as the new ancestorQCellId for the vlist.
            : subsection.renderVlist(hlist.vlists[i], height);
        gridHorizExtend(grid, subsectionGrid);
        if (this.haveTableSeparatorAfter(i)) {
          let separatorCol = gridMergedCell(height, 1, "", ["tableSeparator"]);
          gridHorizExtend(grid, separatorCol);
        }
      });
      gridHorizExtend(grid, this.renderSpareColumns(height,
        // Note: the analogue of this fallback for real columns is the
        // conditional call to renderVlist.
        fixmeAssertNotNull(fallback(qCellId, ancestorQCellId))));
      return grid;
    }

    private renderSpareColumns(height: number, ancestorQCellId: QCellId) {
      return gridRepeatedCell(height, this.layoutTree.root.spareColumns, "",
        ["spareColumn"],
        {ancestorQCellId: ancestorQCellId, parentColumnId: this.columnId});
    }

    private renderBlankRows(height: number, grid: ViewCell[][], ancestorQCellId: QCellId) {
      let blank = _.range(0, height).map(() =>
        this.renderHlist(null, ancestorQCellId, 1, this.layoutTree.root.blankRowsClass)[0]);
      // "Add to object above" feature: a cell in the first blank row can be
      // used to add to the family directly above if that family's own cells are
      // already full and the family is descended from the current object, which
      // is the nearest existing ancestor of the blank cell and would have
      // received the data in the absence of this feature.  If the blank row is
      // actually the top row of the current object, added values should not be
      // "captured" by a previous sibling.  But since "grid" only contains data
      // descended from the current object, this approach doesn't need a
      // separate check for the nearest existing ancestor, unlike Matt's
      // previous implementation that looked at the whole sheet.  Clever!
      //
      // There is no need to exclude computed columns.  Currently, it makes no
      // difference because the code will mark the cells non-editable, but if in
      // the future view-update is supported and enabled on a column, it should
      // be treated the same way as a state column.
      //
      // Future: Consider drawing diagonal lines in the first blank row to make
      // the cells look like they belong to the families above that they will
      // add to:
      //   o Room1   o Occupant1   Val1
      //           \____________ \_____
      // This probably requires dropping the condition that the family above be
      // full.  If we update object region highlighting to include the
      // "down-hanging" cells, we may no longer need ancestorQCellIdForAdd as a
      // separate ViewCell field.
      if (grid.length > 0 && blank.length > 0)
        gridBottomRowWithColNums(grid).forEach(([cellAbove, j]) => {
          if (cellAbove.ancestorQCellId == null)
            // Erroneous computed families.  (Any other cases?)
            return;
          let blankCell = blank[0][j];
          // "Add to object above" applies only to value cells.
          if (blankCell.isObjectCell)
            return;
          // Don't process spare columns: the normal case would never apply and
          // we choose not to apply the first-field case even if the spare
          // column would become the first field of the parent object type.
          if (blankCell.addColumnId == null)
            return;
          // Trigger only on cells in blank space 2 or more levels below the
          // nearest existing ancestor, not on direct child cells in its
          // existing families.
          if (getExistingColumn(blankCell.addColumnId).parent == ancestorQCellId.columnId)
            return;
          // First-field case: if the family above represents the first field of
          // an existing object (regardless of whether the family is nonempty),
          // assume we want to create another object.
          if (cellAbove.addColumnId ==
            getExistingColumn(cellAbove.ancestorQCellId.columnId).children[0])
            // Why is this fixmeAssertNotNull valid?  Every row of `grid`
            // belongs to an element of a vlist, which is non-root.  `blankCell`
            // is at least 2 levels below the vlist owner, so at least 1 level
            // below the element, so it has the vlist element as an existing
            // proper ancestor.
            blankCell.ancestorQCellIdForAdd = fixmeAssertNotNull(
              new CellHandle(cellAbove.ancestorQCellId).parent()).id();
          else if (cellAbove.qCellId != null)
            // Normal case: add to the family above if its own cells are full.
            blankCell.ancestorQCellIdForAdd = cellAbove.ancestorQCellId;
          // But if we wouldn't be able to add, then forget it.  Given that the
          // user can usually tell whether the cell above is editable from its
          // style, adding this complexity to the rule seems a lesser evil than
          // having random cells in the first blank row styled as non-editable.
          if (blankCell.ancestorQCellIdForAdd != null &&
            !StateEdit.canAdd(blankCell.addColumnId, blankCell.ancestorQCellIdForAdd))
            blankCell.ancestorQCellIdForAdd = null;
        });
      return blank;
    }

    // If !expanded, then the requested height should always be 3.  Leaves render
    // at height 2 anyway.

    public renderHeader(expanded: boolean, height: number, depth: number, typeColors: JSONKeyedMap<string, number>) {
      // Part that is always the same.
      let myDepthClass = "rsHeaderDepth" + this.colorIndexForDepth(this.col.isObject ? depth : depth - 1);
      // Currently matching-colored header cells don't depend on depth.  You could
      // argue we should generate two classes and let the CSS deal with it.
      let matchIdx = typeColors.get(this.columnId);
      let myColorClass = matchIdx != null ? "rsHeaderMatch" + this.colorIndexForMatch(matchIdx) : myDepthClass;
      let grid: ViewGrid = [[], []];  // c.f. renderHlist
      if (this.showBullets) {
        let fieldNameCell = new ViewCell("", 1, 1,
          ["rsHeaderFieldNameObject", "hiddenTopBorder"].concat(
            (getColumnType(this.col) !== SpecialType.TOKEN ? ["rsHeaderFieldNameKeyedObject"] : []), [myColorClass]));
        fieldNameCell.columnId = this.columnId;
        fieldNameCell.isObjectHeader = true;
        let typeCell = new ViewCell(this.objectSymbol(), 1, 1,
          ["rsHeaderTypeObject"].concat(
            (getColumnType(this.col) !== SpecialType.TOKEN ? ["rsHeaderTypeKeyedObject"] : []),
            ["centered"], [myColorClass]));
        typeCell.columnId = this.columnId;
        typeCell.isObjectHeader = true;
        if (getColumnType(this.col) === SpecialType.TOKEN) {
          // There is no value UI-column, so certain functionality that would
          // normally be on the value UI-column is on the object UI-column instead.
          fieldNameCell.kind = "tokenObject-below";
          typeCell.kind = "tokenObject-type";
          //typeCell.fullText = 'Column ID ' + @columnId + ' (token)'
        } else {
          fieldNameCell.kind = "keyedObject-below";
          typeCell.kind = "keyedObject-type";
        }
        gridHorizExtend(grid, [[fieldNameCell], [typeCell]]);
      }
      if (getColumnType(this.col) !== SpecialType.TOKEN) {
        let fieldMatchIdx = typeColors.get(getColumnType(this.col));
        let myFieldColorClass = fieldMatchIdx != null
          ? "rsHeaderMatch" + this.colorIndexForMatch(fieldMatchIdx)
          : myDepthClass;
        let fieldNameCell = new ViewCell(fallback(this.col.fieldName, ""), 1, 1,
          [(this.col.isObject ? "rsHeaderFieldNameKey" : "rsHeaderFieldNameLeaf"), myFieldColorClass]);
        fieldNameCell.columnId = this.columnId;
        fieldNameCell.kind = "below";
        let typeName = stringifyTypeForSheet(getColumnType(this.col));
        // The type is essential to interpret values in the column.  The rest of
        // the attributes are no more important than the formula itself, which we
        // currently show only in the action bar, so don't show them here.
        let typeCell = new ViewCell(typeName, 1, 1,
          [(this.col.isObject ? "rsHeaderTypeKey" : "rsHeaderTypeLeaf"), myFieldColorClass].concat(
            this.markDisplayClasses()));
        typeCell.columnId = this.columnId;
        typeCell.kind = "type";
        gridHorizExtend(grid, [[fieldNameCell], [typeCell]]);
      }

      if (this.col.isObject) {
        grid = this.renderFieldHeaders(grid, expanded, height, depth, typeColors, myColorClass);
      } else {
        // Special case: spare column attached to a value column.  In this case,
        // we shouldn't need padding or anything.
        gridHorizExtend(grid, this.renderSpareColumnsHeader(myColorClass));
      }
      return grid;
    }

    private renderFieldHeaders(grid: ViewGrid, expanded: boolean,
      height: number, depth: number, typeColors: JSONKeyedMap<string, number>, colorClass: string) {
      // At this point, height should be at least 3.
      let currentHeight = 2;  // should always be 2 or height
      // "Corner" here is the upper left corner cell, which actually spans all the
      // way across in some cases (indicated by isFinal).
      let makeCorner = (isFinal: boolean) => {
        let classes = ["bottomAtObjectName"];
        if (depth > 1) {
          classes.push("hasOutsideLeftBorder");
        }
        if (!isFinal) {
          classes.push("rsHeaderNonfinal");
        }
        classes.push(colorClass);
        let corner = gridMergedCell(height - 2, grid[0].length, fallback(this.col.objectName, ""), classes);
        if (grid[0].length > 0) {
          corner[0][0].columnId = this.columnId;
          corner[0][0].isObjectHeader = true;
          corner[0][0].kind = "top";
        }
        gridVertExtend(corner, grid);
        currentHeight = height;
        return corner;
      };

      let padIfNecessary = (subsectionGrid: ViewCell[][], drawRightBorder: boolean) => {
        if (subsectionGrid.length < currentHeight) {
          let cssClasses = [colorClass];
          if (subsectionGrid.length == 2) {
            cssClasses.push("bottomAtObjectName");
          }
          if (!drawRightBorder) {
            cssClasses.push("rsHeaderNonfinal");
          }
          // XXX Fragile assumption that we'll never get a zero-height array?
          // Time to introduce a 2D-array class?
          let paddingGrid = gridMergedCell(
            currentHeight - subsectionGrid.length, subsectionGrid[0].length, "", cssClasses);
          gridVertExtend(paddingGrid, subsectionGrid);
          subsectionGrid = paddingGrid;
        }
        return subsectionGrid;
      };

      if (this.columnId === rootColumnId) {
        // Close off the corner unconditionally.  If the first column is a
        // global value column, letting the corner span across it would
        // introduce a corner case (ha ha) we'd just rather not deal with, but
        // we still suppress the border between the corner and the padding cell
        // by marking the corner non-final.
        //
        // This is adequate for now but isn't the last word on the rendering of
        // the root column.  We may be able to get rid of it altogether if the
        // spare columns can replace all of its functionality. ~ Matt 2016-09-15
        grid = makeCorner(this.subsections.length == 0 || this.subsections[0].col.isObject);
      }

      this.subsections.forEach((subsection, i) => {
        let subHeight = expanded ? this.headerHeightBelow : 3;
        let subsectionGrid = subsection.renderHeader(expanded, subHeight, depth + 1, typeColors);
        if (currentHeight === 2 && subsectionGrid.length > 2) {
          grid = makeCorner(false);  // may increase currentHeight so next condition holds
        }
        // If this.columnId === rootColumnId, then the padding cell should
        // have a right border because there will be a table separator column.
        let drawRightBorder = this.columnId === rootColumnId ||
          (i == this.subsections.length - 1 && this.layoutTree.root.spareColumns == 0);
        subsectionGrid = padIfNecessary(subsectionGrid, drawRightBorder);
      gridHorizExtend(grid, subsectionGrid);
        if (this.haveTableSeparatorAfter(i)) {
          // There should be no way we can get here without already making the corner.
          let separatorCol = gridMergedCell(height, 1, "", ["tableSeparator", "hiddenTopBorder"]);
          gridHorizExtend(grid, separatorCol);
        }
      });
      let spareColumnsGrid = this.renderSpareColumnsHeader(colorClass);
      spareColumnsGrid = padIfNecessary(spareColumnsGrid, true);
      gridHorizExtend(grid, spareColumnsGrid);
      if (currentHeight === 2) {
        grid = makeCorner(true);
      }
      return grid;
    }

    private renderSpareColumnsHeader(colorClass: string) {
      let spareColumnsGrid: ViewCell[][] = [[], []];
      for (let i = 0; i < this.layoutTree.root.spareColumns; i++) {
        // C.f. code for value columns in renderHeader above.
        let nonfinalClass = (i < this.layoutTree.root.spareColumns - 1) ? ["rsHeaderSpareNonfinal"] : [];
        let fieldNameCell = new ViewCell("", 1, 1, ["rsHeaderFieldNameSpare", colorClass].concat(nonfinalClass));
        let typeCell = new ViewCell("", 1, 1, ["rsHeaderTypeSpare", colorClass].concat(nonfinalClass));
        gridHorizExtend(spareColumnsGrid, [[fieldNameCell], [typeCell]]);
      }
      return spareColumnsGrid;
    }

    public colorIndexForDepth(depth: number) {
      switch (this.sheetOptions.palette) {
        case "rainbow":
          return depth % 6;
        case "alternating":
          return depth % 2;
        default:
          return 0;
      }
    }

    public colorIndexForMatch(matchIdx: number) {
      // The cost example uses 8 so it repeats colors.  If we use more different
      // colors, they will start to look similar; would it still be worth doing
      // compared to repeating colors?
      switch (this.sheetOptions.palette) {
        case "alternating":
          return matchIdx % 5;
        default:
          return 0;
      }
    }
  }

  type CellPredicate = (c: ViewCell) => boolean;
  function postSelectionPredicate(template: SpreadsheetTemplateInstance, predicate: CellPredicate) {
    if (template.sheetView != null && template.sheetView.selectMatchingCell(predicate))
      template.pendingSelectionPredicate = null;
    else
    template.pendingSelectionPredicate = predicate;
  }

  export class StateEdit {
    public static parseValueUi(type: OSType, text: string) {
      try {
        return parseValue(type, text);
      } catch (e) {
        alert("Invalid value: " + e.message);
        throw e;
      }
    }

    /**
     * Parses enteredValue and forwards the call to addCellRecursive on the server to add a new
     * cell to the model having that value.
     * See doc of Model.addCellRecursive for a description of the parameters.
     */
    public static addCell(template: SpreadsheetTemplateInstance, addColumnId: NonRootColumnId, ancestorQCellId: QCellId,
      enteredValue: string | null, newColumnType: null | OSType, viewId: null | string,
      callback: MeteorCallback<void>) {
      let newValue = (enteredValue == null) ? null :
        this.parseValueUi(fallback(newColumnType, getColumnType(getExistingColumn(addColumnId))), <string>enteredValue);
      // If there is a placeholder that is still being edited, assume that's
      // where we're adding and mark it being saved.  Don't do this until after
      // we successfully parsed the value and we know we will actually attempt
      // the save.
      let consumePlaceholder = (template.placeholderFamily.get() != null && !template.placeholderSaving);
      if (consumePlaceholder)
        template.placeholderSaving = true;
      MeteorMethods.addCellRecursive.call($$.id, {
        columnId: addColumnId,
        ancestorQCellId,
        value: newValue,
        newColumnType,
        viewId,
        cleanup: false
      }, $$client.wrapCallback((error, response) => {
        // This should be redundant because the beforeChange callback will
        // trigger a rebuild anyway.
        if (consumePlaceholder) {
          template.placeholderSaving = false;
          template.placeholderFamily.set(null);
        }
        if (error == null) {
          response = response!;
          // TODO: Make FamilyHandle EJSONable.  Wait until I clean up the style of
          // the whole class? ~ Matt 2016-10-05
          let fam = new FamilyHandle(response.qFamilyId);
          // Try to move the selection to the added cell, once it shows up.
          let predicate: CellPredicate;
          {
            let child = fam.cell(response.value);
            predicate = (c) => EJSON.equals(c.qCellId, child.id());
          }
          postSelectionPredicate(template, predicate);
        }
        callback(error, undefined);
      }));
    }

    public static modifyCell(qCellId: QCellId, enteredValue: string, callback: MeteorCallback<void>) {
      let cel = new CellHandle(qCellId);
      let newValue = this.parseValueUi(getColumnType(getExistingColumn(cel.columnId)), enteredValue);
      cel.setValue_Client(newValue, $$client.wrapCallback(callback));
    }

    public static removeCell(qCellId: QCellId, callback: MeteorCallback<void>) {
      new CellHandle(qCellId).remove_Client($$client.wrapCallback(callback));
    }

    public static typeIsEditable(columnId: ColumnId) {
      let col = getColumn(columnId);
      // May as well not let the user try to edit _unit.
      return (col != null) && ( getColumnType(col) !== SpecialType.TOKEN && getColumnType(col) !== SpecialType.UNIT);
    }

    public static canModifyOrRemove(qCellId: QCellId) {
      return new CellHandle(qCellId).containingFamily().isEditable();
    }

    // For ancestorQCellId, pass ViewCell.ancestorQCellIdForAdd if that's what
    // you're about to use.
    public static canAdd(addColumnId: NonRootColumnId, ancestorQCellId: QCellId) {
      assert(addColumnId != ancestorQCellId.columnId);
      let col = getExistingColumn(addColumnId);
      while (col.parent != ancestorQCellId.columnId) {
        // We don't know whether the editability conditions of the intermediate
        // columns will be satisfied.  Let the user attempt an add unless an
        // intermediate column has no editability formula at all.  This still
        // sticks out as wrong in some cases (e.g., PaperView:assignedReviewers
        // in the conf example), but I don't see anything easy we can do about
        // it short of completely shutting off recursive adds for computed data,
        // which I don't want to do.  (I guess if the editability formula makes
        // no reference to "this" except to navigate up to ancestors that
        // already exist, then we could evaluate it, and that would handle the
        // PaperView:assignedReviewers case, but that's more code than I want to
        // write right now.)
        //
        // TODO: Would this be a good place to check whether we're going to try
        // and fail to "new" a state keyed object, or more generally, whether a
        // "new" trigger is defined?  Not as part of the editability change.
        // ~ Matt 2017-02-25
        if (!columnIsPotentiallyEditable(col))
          return false;
        col = getExistingColumn(<NonRootColumnId>col.parent);
      }
      let topFamily = new FamilyHandle({columnId: col._id, parentCellId: ancestorQCellId.cellId});
      return topFamily.isEditable();
    }
  }

  function insertBlankColumn(parentId: ColumnId, index: number, isObject: boolean, view: View) {
    // Obey the restriction on a state column as child of a formula column.
    // Although changeColumnFormula allows this to be bypassed anyway... :(
    let formula = getExistingColumn(parentId).formula != null ? DUMMY_FORMULA : null;
    let viewId = view.id;
    if (isObject && (formula == null)) {
      MeteorMethods.insertUnkeyedStateObjectTypeWithField.call($$.id, {
        parentId,
        index,
        // Minor bug: if we are implicitly promoting the parent column to an
        // object column, the inserted column will take a lower number than its
        // parent, which looks odd.
        objectName: nextAvailableColumnName("Object"),
        fieldName: nextAvailableColumnName("value"),
        specifiedType: DEFAULT_STATE_FIELD_TYPE,
        viewId
      }, $$client.wrapCallback(standardServerCallback));
    } else {
      MeteorMethods.defineColumn.call($$.id, {
        parentId,
        index,
        fieldName: nextAvailableColumnName("value"),
        specifiedType: formula != null ? null : DEFAULT_STATE_FIELD_TYPE,
        isObject,
        objectName: null,  // when applicable, [fieldName] is OK
        formula,
        viewId,
        singular: false,
        isViewObjectRoot: false,
        userVisible: true
      }, $$client.wrapCallback(standardServerCallback));
    }
  }

  let defaultSheetOptions = {
      // Show type row in header
      // Currently shown, otherwise users too often forget to set the type.
      // Consider turning this off when we guess the type based on entered data.
      // ~ Matt 2015-12-03
      showTypes: true,
      // Pretty self-explanatory...
      headerInitiallyExpanded: true,
      canToggleHeaderExpansion: true,
      // 'boring' for grey, 'alternating' for two greys, 'rainbow' for dazzling colors
      palette: "alternating",
      // Matching colors for fields of reference type and their target object columns.
      colorReferences: true,
      // If true, show bullets for every object type.  If false, hide bullets except
      // for object types with no children (for UI prototyping; not all functionality
      // works in this mode).
      showBullets: true,
      // Spare rows and columns.  The "printable" mode hides these.
      rootSpareRows: 10,
      spareColumns: true,
      // Developers only (print some logs with timestamps)
      profile: false,

      // We've stopped maintaining these options because they weren't worth
      // the complexity they added to the already very complex rendering code.
      // They are now fixed at the values shown below.

      // Separator column between every pair of adjacent incomparable columns
      // (except ones that are in separate tables when separateTables is on).
      //
      // Will be more workable once we have column plurality data.  I still
      // feel that separator columns have merit and we should consider turning
      // them back on the future if we don't come up with something better.
      // ~ Matt 2016-09-15
      //sepcols: false,
      // Show children of the root as separate tables.
      //separateTables: true,
    };

  // We're not doing anything complicated; this should work fine.
  type SheetOptions = typeof defaultSheetOptions;

  interface ContextMenuCommand {
    name: string;
    callback: () => void;
  }
  interface AddCommands {
    commands: ContextMenuCommand[];
    allowEnter: boolean;
  }
  type GridCoords = {row: number, col: number};

  class ClientViewRevision {
    public layoutTree: Tree<LayoutNode>;
    public mainSection: ViewSection;
    public qCellIdToGridCoords: JSONKeyedMap<QCellId, GridCoords>;
    public grid: ViewGrid;
    public colClasses: (null | string)[];  // one per column
    public cellClasses: string[][][];  // multiple per cell
    public updatableHotConfig: ht.Options & {data: string[][]};

    public refId(qCellId: QCellId) {
      //if qCellId.columnId == rootColumnId
      //  "root"
      //else
      let loc = this.qCellIdToGridCoords.get(qCellId);
      return loc != null ? `${loc.row}-${loc.col}` : null;
    }

    constructor(cv: ClientView) {
      this.layoutTree = cv.buildLayoutTree(cv.view.def().layout);
      this.mainSection = new ViewSection(cv.template, this.layoutTree, cv.sheetOptions);

      if (cv.sheetOptions.profile) console.log(`[${stamp()}]  ---  preparing grid started  --- `);

      cv.rebuildCount++;
      // Take a dummy dependency so the beforeChange callback can trigger a
      // rebuild by setting this var.  This way, Blaze can schedule the rebuild
      // for an appropriate time and coalesce it with other changes requiring a
      // rebuild.
      cv.rebuildRequester.get();
      cv.pending = [];

      // Display the root column for completeness.  However, it doesn't have a real
      // value.
      let hlist = this.mainSection.prerenderHlist([], "");
      // XXX This is in addition to any placeholders.  Desirable?
      hlist.minHeight += cv.sheetOptions.rootSpareRows;
      let typeColors = new JSONKeyedMap<ColumnId, number>();
      if (cv.sheetOptions.colorReferences) {
        let typesToColor = new JSONSet<ColumnId>();
        this.mainSection.findTypesToColor(typesToColor);
        this.mainSection.assignTypeColors(0, typesToColor, typeColors);
      }
      let grid = this.mainSection.renderHeader(
        cv.headerExpanded.get(), cv.headerExpanded.get() ? this.mainSection.headerMinHeight : 3, 0, typeColors);
      for (let row of grid) {
        for (let cell of row) {
          cell.cssClasses.push("htBottom", "rsHeader");  // easiest to do here
        }
      }
      let headerHeight = grid.length;
      if (!cv.sheetOptions.showTypes) {  // HACK: Delete the last header row
        grid.pop();
        grid.forEach((row, i) => {
          for (let cell of row) {
            if (cell.rowspan > grid.length - i) {
              cell.rowspan = grid.length - i;
            }
          }
        });
      }
      let gridData = this.mainSection.renderHlist(hlist, null, hlist.minHeight);
      gridVertExtend(grid, gridData);

      //gridCaption = []
      if (cv.sheetOptions.canToggleHeaderExpansion) {
        if (headerHeight > 2 && this.mainSection.showBullets) {
          let toggleHtml = `<svg class="toggleHeaderExpanded" style="height: 11px; width: 10px">\n` +
            `  <path style="stroke: black; fill: black" ` +
            `d="${cv.headerExpanded.get() ? "M 1 4 l 8 0 l -4 4 z" : "M 3 1 l 4 4 l -4 4 z"}"/>\n</svg>`;
          grid[0][0].value = toggleHtml;
          grid[0][0].cssClasses.push("rsRoot");
        }
      }

      if (!cv.sheetOptions.showTypes) {  // HACK: Same
        //gridCaption.pop()
        headerHeight = headerHeight - 1;
      }

      // Resolve cell cross-references.
      // @ notation disabled; relevant code commented out. ~ Matt 2015-11-10
      this.qCellIdToGridCoords = new JSONKeyedMap<QCellId, GridCoords>();
      grid.forEach((rowCells, i) => {
        rowCells.forEach((cell, j) => {
          if ((cell.qCellId != null) && cell.isObjectCell) {
            this.qCellIdToGridCoords.set(cell.qCellId, {
              row: i,
              col: j
            });
          }
        });
      });
      for (let row of grid) {
        for (let cell of row) {
          if (cell.value instanceof CellReference) {
            cell.referent = cell.value.qCellId;
            cell.display = cell.value.display;
          }
        }
      }

      this.grid = grid;

      if (cv.sheetOptions.profile) console.log(`[${stamp()}]  ---  preparing grid finished  --- `);

      this.colClasses = _.range(0, grid[0].length).map((col) => {
        let colCls = null;
        for (let row = 0; row < grid.length; row++) {
          for (let cls of grid[row][col].cssClasses || []) {
            if (cls === "rsCaption" || cls === "rsRoot" || cls === "tableSeparator" || cls == "spareColumn") {
              // assert (!colCls? || colCls == cls)
              colCls = cls;
            }
          }
        }
        return colCls;
      });

      this.cellClasses = grid.map((dataRow, row) => dataRow.map((cell, col) => {
        let refc: null | string;
        let classes: string[] = [];
        if ((cell.qCellId != null) && cell.isObjectCell && ((refc = this.refId(cell.qCellId)) != null)) {
          classes.push(`ref-${refc}`);
        }
        // Object region highlighting doesn't include spare rows and columns.
        // (Spare rows will become irrelevant if we hide the root object.)
        if (cell.cssClasses.indexOf("spareRow") == -1 && cell.cssClasses.indexOf("spareColumn") == -1) {
          let ancestors =
            cell.qCellId != null ? new CellHandle(cell.qCellId).ancestors() :
            cell.ancestorQCellId != null ? new CellHandle(cell.ancestorQCellId).ancestors() :
            [];
          for (let ancestor of ancestors) {
            if ((refc = this.refId(ancestor.id())) != null) {
              classes.push(`ancestor-${refc}`);
            }
          }
        }
        let editable;
        if (cell.kind != null) {
          // Only column header "top" and "below" cells can be edited,
          // for the purpose of changing the objectName and fieldName respectively.
          editable = (cell.kind == "top" || cell.kind == "below") && cell.columnId !== rootColumnId;
        } else if (cell.qCellId != null) {
          // Check !isObject instead of !cell.isObjectCell: disallow "editing"
          // the key of a keyed object because that will delete the object and
          // insert a new one, losing all descendant data, which probably
          // isn't what the user expects.
          editable = !getExistingColumn(cell.qCellId.columnId).isObject
            && StateEdit.typeIsEditable(cell.qCellId.columnId)
            && StateEdit.canModifyOrRemove(cell.qCellId);
        } else if (cell.addColumnId != null) {
          // We don't have state columns as descendants of formula columns, so
          // if we can edit addColumnId, we'll also be able to insert the
          // ancestors.
          editable = !cell.isObjectCell && StateEdit.typeIsEditable(cell.addColumnId)
            && StateEdit.canAdd(cell.addColumnId,
              fixmeAssertNotNull(fallback(cell.ancestorQCellIdForAdd, cell.ancestorQCellId)));
        } else if (cell.parentColumnId != null) {
          // Spare columns: currently always editable.
          editable = true;
        } else {
          editable = false;
        }
        if (editable)
          classes.push("editable");
        return cell.cssClasses.concat(classes);
      }));

      this.updatableHotConfig = {
        data: grid.map((row) => row.map((cell) =>
          // If cell.value were a CellReference, then cell.display should have
          // been set.  XXX: Untangle this.
          fallback(cell.display, <string>cell.value))),
        // Future: Fixing the ancestors of the leftmost visible column would be
        // clever, though with carefully designed individual views, we may never
        // need it.  We may also want to fix the header for large data sets.
        //fixedColumnsLeft: 1  # Caption removed
        colWidths: _.range(0, this.grid[0].length).map((i) => {  // no way grid can be empty
          switch (this.colClasses[i]) {
            case "tableSeparator":
              return 20;
            case "rsRoot":
              return 18;
            case "spareColumn":
              // Handsontable default.  XXX: Reference from there?
              return 50;
            default:
              // NOTE!  Auto column size only works if this is undefined, not null.
              return undefined;
          }
        }),
        rowHeights: (() => {
          // Specify all the row heights (24 pixels is the Handsontable default),
          // otherwise the fixed clone of the left column sometimes reduced the
          // objectName row to zero height because it wasn't constrained by the
          // content of the real table.  We can look out for any similar glitches.
          let fieldNameRow = headerHeight - (1 + (cv.sheetOptions.showTypes ? 1 : 0));
          let objectNameRow = fieldNameRow - 1;
          return _.range(0, this.grid.length).map((i) =>
            i < objectNameRow ? 11 :
            i == objectNameRow ? 23 :  // Remove a pixel for the missing bottom border.
            i == fieldNameRow ? 25 :  // Add a pixel for the top border.
            24);
        })(),
        mergeCells: (() => {
          let entries: ht.MergedCellEntry[] = [];
          grid.forEach((row, i) => row.forEach((cell, j) => {
            if (cell.rowspan !== 1 || cell.colspan !== 1) {
              entries.push({
                row: i,
                col: j,
                rowspan: cell.rowspan,
                colspan: cell.colspan
              });
            }
          }));
          return entries;
        })(),
      };   // Handsontable config object
    }
  }

  export class ClientView {
    public hot: Handsontable;
    // null: table has just been rebuilt, so onSelection should run unconditionally.
    // undefined | ht.SelectionCoords: result of this.hot.getSelected()
    public savedSelection: null | undefined | ht.SelectionCoords;
    public rev: ClientViewRevision;
    public pending: Array<String>;
    public rebuildCount = 0;
    public rebuildRequester = new ReactiveVar(new Object());

    constructor(public template: SpreadsheetTemplateInstance,
      public view: View, public sheetOptions: SheetOptions, domElement: HTMLElement) {
      this.headerExpanded = new ReactiveVar(sheetOptions.headerInitiallyExpanded);
      this.savedSelection = undefined;
      this.pending = [];

      this.rev = new ClientViewRevision(this);

      let initialHotConfig: ht.Options = {
        cells: (row, col, prop) => {
          let clsRow = arrayGet(this.rev.cellClasses, row);
          let classes = clsRow != null ? arrayGet(clsRow, col) : null;
          if (!classes) {
            return {};  // may occur if grid is changing
          }

          if (this.pending.indexOf(`${row}-${col}`) >= 0) {
            // must copy classes because at this point it aliases an element of this.cellClasses
            classes = classes.concat("pending");
          }
          return {
            renderer:
              col === 0 && row === 0 ? "html" :
              _.contains(classes, "hasOutsideLeftBorder") ? this.textWithOutsideLeftBorderRenderer.bind(this) :
              "text",
            className: classes.join(" "),
            // Edge case: renaming the column whose formula is currently being edited could change
            // the string representation of the original formula, which would trigger a reactive
            // update that would lose the unsaved changes.
            // XXX Remove when we have better handling of changes to the original formula in general.
            //
            // Make this nonreactive: Handsontable will requery it when the user
            // starts editing, and we don't want to rebuild the table when it
            // changes.  Don't use readOnly because that would dim the cells, which
            // we think is more than is appropriate.
            /*  editor: Tracker.nonreactive(() =>
              ActionBar.hasUnsavedData(actionBarOfSheet(this.template)))
              ? false : "text",  */
            readOnly: classes.indexOf("editable") == -1
          };
        },
        autoColumnSize: {
          // I saw glitches with the asynchronous sizing on the cost sheet.  Rather
          // than investigate, I'm happy to go back to synchronous for now.
          // ~ Matt 2015-11-21
          syncLimit: "100%"
        },
        // Disable partial rendering.  Our highlighting code doesn't work
        // properly with partial rendering, and there are unsolved issues that
        // force full rendering anyway in many cases, so we are liable to add
        // other code that fails with partial rendering and not notice.
        //
        // The issues that currently force full rendering in many cases:
        //
        // - Rows: The rendering rectangle expands to not split any merged
        // cells, and the merged cell for the root object spans almost all rows.
        //
        // - Columns: The rendering calculation uses the minimum column widths
        // and doesn't see the larger widths from auto column sizing (it's
        // unclear why), so it thinks more columns fit in the available space.
        //
        // ~ Matt 2017-03-14
        viewportRowRenderingOffset: Infinity,
        viewportColumnRenderingOffset: Infinity,
        // We don't have a principled design for how the selection should move
        // after pressing Enter, and for now, the default behavior is causing
        // surprises when it moves the selection to the padding cell at the bottom
        // of a family, which is mapped by the "select similar cell" code to the
        // first cell in the family.
        enterMoves: {
          row: 0,
          col: 0
        },
        // Seems more helpful to the user (e.g., when scrolling the browser window).
        // See if we have trouble with the user needing to "escape" from the table.
        outsideClickDeselects: false,
        afterDeselect: () => {
          this.onSelection();
        },
        afterSelection: (r1, c1, r2, c2) => {
          this.onSelection();
        },
        beforeKeyDown: (event) => {
          this.onKeyDown(event);
        },
        beforeChange: (changes, source) => {
          if (!source) return true;   // Run this handler only for interactive edits

          /* We want to allow pending cell edits to remain in the table until
           * the server call completes (successfully or not) or the table has to
           * be rebuilt for some other reason (e.g., concurrent changes).
           * Normally we achieve this by adding the affected cells to
           * this.pending and returning true, so the render that Handsontable
           * performs after the edit shows the new data and styles the cells as
           * pending.  Then, our server callback is responsible for triggering a
           * rebuild (which regenerates the table from the committed data and
           * clears the styling) if this hasn't already been done for some other
           * reason.  As a special case, if beforeChange gets an exception
           * (meaning that some cell edits may have been rejected before a
           * server call was made) or a synchronous call to a server callback
           * (can that even happen?), then it returns false, so the table is not
           * changed by the edit itself, but any successful data changes will
           * cause the table to be rebuilt.
           *
           * Matt's cleanup and documentation of Shachar's original
           * implementation, 2017-05-09. */
          let savedRebuildCount = this.rebuildCount;
          let beforeChangeHasReturned = false, applyEdit = true;
          let newPendingCells = [];
          for (let [row, col, oldVal, newVal] of changes) {
            if (oldVal === newVal) continue;

            let cell = this.rev.grid[row][col];
            let callback: MeteorCallback<void> = (error, result) => {
              if (beforeChangeHasReturned) {
                if (applyEdit && this.rebuildCount == savedRebuildCount)
                  this.rebuildRequester.set(new Object());
              } else {
                applyEdit = false;
              }
              standardServerCallback(error, result);
            };
            newPendingCells.push(`${row}-${col}`);

            try {
              // One of these cases should apply...
              let name: string;
              if (cell.kind === "top") {
                name = newVal === "" ? null : newVal;
                MeteorMethods.changeColumnObjectName.call($$.id, fixmeAssertNotNull(cell.columnId), name,
                $$client.wrapCallback(callback));
              } else if (cell.kind === "below") {
                name = newVal === "" ? null : newVal;
                MeteorMethods.changeColumnFieldName.call($$.id, fixmeAssertNotNull(cell.columnId), name,
                $$client.wrapCallback(callback));
              } else if ((cell.qCellId != null) && !cell.isObjectCell) {
                // XXX Once we validate values, we should replace the hard-coded
                // check for 'text' with an attempt to validate the input.
                if (newVal || getColumnType(getExistingColumn(cell.qCellId.columnId)) == SpecialType.TEXT) {
                  StateEdit.modifyCell(cell.qCellId, newVal, callback);
                } else {
                  StateEdit.removeCell(cell.qCellId, callback);
                }
              } else if (cell.parentColumnId != null) {
                // Adding data to a spare column.
                StateEdit.addCell(this.template,
                  cell.parentColumnId,
                  // The spare column is empty, but if it's the first (i.e.,
                  // only) field of its parent object type, then
                  // cell.ancestorQCellIdForAdd can be triggered by an existing
                  // parent object.  A weird situation that users shouldn't
                  // normally get into, but let's be consistent.
                  fixmeAssertNotNull(fallback(cell.ancestorQCellIdForAdd, cell.ancestorQCellId)),
                  newVal,
                  DEFAULT_STATE_FIELD_TYPE, this.view.id, callback);
              } else {
                // Adding data not in a spare column.  May be a spare column, a
                // placeholder, or a plain old internal blank cell.
                StateEdit.addCell(this.template,
                  fixmeAssertNotNull(cell.addColumnId),
                  fixmeAssertNotNull(fallback(cell.ancestorQCellIdForAdd, cell.ancestorQCellId)),
                  newVal,
                  null, this.view.id, callback);
              }
            } catch (e) {
              console.error(e.stack);
              applyEdit = false;
            }
          }

          beforeChangeHasReturned = true;
          if (applyEdit)
            this.pending.push(...newPendingCells);
          return applyEdit;
        },
        contextMenu: {
          build: (): fixmeAny => {
            let ci: ColumnId,
              deleteCommand: null | ContextMenuCommand, demoteCommand: null | ContextMenuCommand;
            if (ActionBar.hasUnsavedData(actionBarOfSheet(this.template))) {
              return false;
            }

            let c = fallback(this.getSingleSelectedCell(), <fixmeAny>{});

            let items = <fixmeAny>{};

            if ((ci = c.columnId) != null) {
              let col = getExistingColumn(ci);
              let objectName = objectNameForUI(col);
              let fieldName = fieldNameForUI(col);
              if (!col.isObject) {
                items.promote = {
                  name: col.formula == null
                    ? `Wrap '${fieldName}' values in objects`
                    : `Generate objects for '${fieldName}' values`,
                  callback: () => {
                    MeteorMethods.changeColumnIsObject.call($$.id, ci, true,
                      $$client.wrapCallback(standardServerCallback));
                  }
                };
              }
              // If !col.isObject, then the defineColumn (or the first defineColumn
              // of the insertUnkeyedStateObjectTypeWithField) will automatically
              // promote col.
              let addFieldItem = {
                name: ci === rootColumnId ? "Add global value column to sheet" :
                  col.isObject ? `Add value column to '${objectName}'` : "...and add another value column",
                callback: () => {
                  let index = col.children.length;
                  insertBlankColumn(ci, index, false, this.view);
                }
              };
              let addObjectTypeItem = {
                name: ci === rootColumnId ? "Add object column to sheet" :
                  col.isObject ? `Add nested object column to '${objectName}'` : "...and add a nested object column",
                callback: () => {
                  let index = col.children.length;
                  insertBlankColumn(ci, index, true, this.view);
                }
              };
              if (ci === rootColumnId) {  // order tweak for common case
                items.addObjectTypeItem = addObjectTypeItem;
                items.addField = addFieldItem;
              } else {
                items.addField = addFieldItem;
                items.addObjectTypeItem = addObjectTypeItem;
              }
              if ((demoteCommand = this.getDemoteCommandForColumn(col))) {
                items.demote = demoteCommand;
              }

              // Don't allow a keyed object column and its key column to be deleted
              // with a single command, since I couldn't find a label for the
              // command that wasn't confusing.  Hopefully it's clear that "Remove
              // generated objects" is the first step toward deleting a keyed
              // object column.
              if (ci !== rootColumnId && col.children.length === 0 &&
                (!col.isObject || getColumnType(col) === SpecialType.TOKEN)) {
                items.delete = {
                  name: col.isObject ? `Delete '${objectName}' object column` : `Delete '${fieldName}' value column`,
                  callback: () => {
                    this.hot.deselectCell();  // <- Otherwise changeColumn form gets hosed.
                    MeteorMethods.deleteColumn.call($$.id, ci, $$client.wrapCallback(standardServerCallback));
                  }
                };
              }
            } else {
              if (c.referent != null) {
                const coords = this.rev.qCellIdToGridCoords.get(c.referent);
                if (coords != null) {
                  items.jumpToReferent = {
                    name: `Jump to object '${c.display}'`,
                    callback: () => {
                      this.selectSingleCell(coords);
                    }
                  };
                }
              }
              let addCmds = this.getAddCommandsForCell(c);
              addCmds.commands.forEach((cmd, index) => { items["add" + index] = cmd; });
              if ((deleteCommand = this.getDeleteCommandForCell(c)) != null) {
                items.delete = deleteCommand;
              }
            }

            function isEmpty(o: fixmeAny) {
              for (let _k in o) {
                return false;
              }
              return true;
            }
            if (isEmpty(items)) {
              items.nothing = {
                name: "No actions available here",
                disabled: () => true
              };
            }

            return {
              items: items
            };
          }    // end of build callback
        },    // contextMenu

        ...this.rev.updatableHotConfig
      };

      this.hot = new Handsontable(domElement, initialHotConfig);
      $(domElement).addClass(`pal-${this.sheetOptions.palette}`);
      if (this.sheetOptions.showTypes) {
        $(domElement).addClass("showTypes");
      }
      // Monkey patch: Don't let the user merge or unmerge cells.
      this.hot.mergeCells.mergeOrUnmergeSelection = (cellRange: fixmeAny) => {};
    }

    // Resizes the sheet to the available space without reloading.
    public redraw() {
      this.hot.render();
      this.updateHighlights();
    }

    public buildLayoutTree(columnIds: Tree<ColumnId>): Tree<LayoutNode> {
      let t = columnIds.map((columnId) => new LayoutNode(columnId));
      if (this.sheetOptions.spareColumns)
        t.root.spareColumns = 10;   // magic number?
      // Children of the root do not stretch, and their extra rows are treated
      // as spare rows.
      t.subtrees.forEach((s, i) => {
        s.root.stretchV = false;
        s.root.blankRowsClass = "spareRow";
      });
      // Add a spare column to the parent object type of the rightmost existing
      // column.  Exception: if the rightmost existing column is a value column
      // C that is a child of the root, then attach the spare column to C
      // itself.  When the spare column is filled, Model.addCellRecursive will
      // promote C to an object column first.
      if (this.sheetOptions.spareColumns && t.subtrees.length > 0) {
        let s = t.subtrees[t.subtrees.length - 1];
        while (s.subtrees.length > 0) {
          let s2 = s.subtrees[s.subtrees.length - 1];
          if (!getExistingColumn(s2.root.columnId).isObject)
            break;
          s = s2;
        }
        // The only operation that spare columns currently support is entry of
        // state data, and we can't add state data to a computed object type (or
        // a computed global value column that will become one) or a view object
        // type, so omit the spare column in those cases.  Reconsider once the
        // action bar supports setting the column name and formula.
        let sRootCol = getExistingColumn(s.root.columnId);
        if (sRootCol.formula == null && sRootCol.viewObjectType == null)
          s.root.spareColumns = 1;
      }
      t.forEach((s) => {
        let col = getColumn(s.columnId);
        // Objects do not stretch.
        if (col && col.isObject) s.stretchV = false;
      });
      return t;
    }

    // https://docs.handsontable.com/0.20.1/demo-custom-renderers.html
    textWithOutsideLeftBorderRenderer(hot: Handsontable, td: HTMLTableDataCellElement,
        row: number, col: number /* , ... */) {
      (<fixmeAny>Handsontable.renderers.TextRenderer)(...arguments);
      let markerDiv = document.createElement("div");
      markerDiv.className = "outsideLeftBorderMarker";
      td.insertBefore(markerDiv, td.firstChild);
      let borderDiv = document.createElement("div");
      borderDiv.className = "outsideLeftBorder";
      // Chrome rounds the heights of table cells in a special way when the zoom
      // is not 100%, and using "height: 100%;" on the divs is the only way to
      // pick up the rounded height.  But then we need to set td.style.height in
      // order for "height: 100%;" to work in Firefox.
      //
      // Note: td.rowSpan is not set yet; mergeCells.applySpanProperties is
      // called in an afterRenderer hook.
      td.style.height = (11 * (this.rev.grid[row][col].rowspan - 1) + 23) + "px";
      markerDiv.appendChild(borderDiv);
    }

    public rebuild() {
      // @savedSelection is not meaningful after we update the table.
      this.savedSelection = null;
      this.rev = new ClientViewRevision(this);
      this.hot.updateSettings({
        colWidths: this.rev.updatableHotConfig.colWidths,
        rowHeights: this.rev.updatableHotConfig.rowHeights,
        mergeCells: this.rev.updatableHotConfig.mergeCells
      }, false);
      /*
       * Tried this as an optimization. Didn't seem to be faster (sometimes slower)
       * than just running loadData. Might be different on larger spreadsheets though,
       * so I'm leaving it here for reference.   ~ Shachar, 2-24-2016
       *
      cfg.data.forEach((dataRow, row) =>
        dataRow.forEach((dataElem, col) => {
          if (dataElem !== this.hot.getDataAtCell(row, col)) {
            this.hot.setDataAtCell(row, col, dataElem);
          }
        })
      )
      */
      this.hot.loadData(this.rev.updatableHotConfig.data);
    }

    public getSelected = () => {
      let s: undefined | number[];
      if ((s = this.hot.getSelected()) != null) {
        let [r1, c1, r2, c2] = s;
        [r1, r2] = [Math.min(r1, r2), Math.max(r1, r2)];
        [c1, c2] = [Math.min(c1, c2), Math.max(c1, c2)];
        return [r1, c1, r2, c2];
      } else {
        return null;
      }
    }

    public getSingleSelectedCell() {
      let s = this.getSelected();
      if (s == null) {
        // This can happen if no selection was made since page was loaded
        return null;
      }
      let [r1, c1, r2, c2] = s;
      let cell = this.rev.grid[r1][c1];
      if (r2 === r1 + cell.rowspan - 1 && c2 === c1 + cell.colspan - 1) {
        return cell;
      } else {
        return null;
      }
    }

    public getMultipleSelectedCells() {
      let cells: ViewCell[] = [];
      for (let coord of this.hot.getSelectedRange().getAll()) {
        let cell = this.rev.grid[coord.row][coord.col];
        // Skip cells covered by merged cells.  Seems like a reasonable thing to
        // check for as part of this function, even though it doesn't matter for
        // most callers because they have to check specifically for the kinds of
        // cells they support anyway.
        if (cell.value != null) {
          cells.push(cell);
        }
      }
      return cells;
    }

    private updateHighlights() {
      this.highlightReferent(this.template.selectedCell != null ?
        undefinedToNull(this.template.selectedCell.referent) : null);
      this.highlightObject(this.template.selectedCell != null && this.template.selectedCell.isObjectCell
        ? this.template.selectedCell.qCellId : null);
    }

    private highlightReferent(referent: null | QCellId) {
      let refc;
      $(".referent").removeClass("referent");
      $(".referent-object").removeClass("referent-object");
      if ((referent != null) && ((refc = this.rev.refId(referent)) != null)) {
        $(`.ref-${refc}`).addClass("referent");
        $(`.ancestor-${refc}`).addClass("referent-object");
      }
    }

    private highlightObject(obj: null | QCellId) {
      let refc;
      $(".selected-object").removeClass("selected-object");
      if ((obj != null) && ((refc = this.rev.refId(obj)) != null)) {
        $(`.ancestor-${refc}`).addClass("selected-object");
      }
    }

    public onSelection() {
      let ci;
      let selection = this.hot.getSelected();
      if (EJSON.equals(selection, this.savedSelection)) {
        return;
      }
      let actionBar = actionBarOfSheet(this.template);
      if (ActionBar.hasUnsavedData(actionBar)) {
        if (this.savedSelection != null) {
          this.hot.selectCell(...this.savedSelection);
        } else {
          // I don't think this should happen, but don't crash. ~ Matt
          this.hot.deselectCell();
        }
        return;
      }
      this.savedSelection = selection;
      this.template.selectedCell = this.getSingleSelectedCell();
      this.updateHighlights();
      actionBar.fullTextToShow.set(
        this.template.selectedCell != null ? this.template.selectedCell.fullText : null);
      // _id: Hacks to get the #each to clear the forms when the cell changes.
      actionBar.changeColumnArgs.set(
        (this.template.selectedCell != null) &&
          ((ci = this.template.selectedCell.columnId) != null) && ci !== rootColumnId ? [{
          _id: ci,
          columnId: ci,
          onObjectHeader: this.template.selectedCell.isObjectHeader
        }] : []);
    }

    // get*CommandForCell return a context menu item, but onKeyDown also uses
    // just the callback, so we maintain consistency in what command is offered.

    public getAddCommandsForCell(c: ViewCell): AddCommands {
      // Note: these commands intentially do not honor "add to object above".
      let cmds: AddCommands = {commands: [], allowEnter: false};
      if (c.addColumnId == null) return cmds;
      let col = getExistingColumn(c.addColumnId);
      // Editability check for all commands.  Spare columns bypass this check.
      // When can c.ancestorQCellId be null?  Root cell.  We should probably
      // just change c.addColumnId to be null in that case.
      if (c.ancestorQCellId == null || !StateEdit.canAdd(c.addColumnId, c.ancestorQCellId))
        return cmds;

      if (!c.isObjectCell) {
        // Add placeholder or data.
        if (getColumnType(col) === SpecialType.UNIT) {
          // Adding a duplicate value has no effect, but disallow it as a
          // hint to the user.
          let values: null | OSValue[];
          if (!(col.parent == c.ancestorQCellId.columnId &&
                (values = new FamilyHandle({
                  columnId: <NonRootColumnId>col._id,
                  parentCellId: c.ancestorQCellId.cellId
                }).values()) != null && values.length > 0)) {
            cmds.commands.push({
              name: "Add X here",
              callback: () => {
                StateEdit.addCell(this.template,
                  c.addColumnId!, c.ancestorQCellId!, "X", null, this.view.id, standardServerCallback);
              }
            });
            cmds.allowEnter = true;
          }
        } else {
          // Editable data types (should align with StateEdit.canEdit).
          //
          // Reserve Enter for editing.  (Exception: keys of existing keyed
          // objects don't support editing, so Enter will do nothing at all,
          // which is probably the least surprising behavior.)

          // Shachar and Matt agreed on 2017-04-11 that the new "add value"
          // flow would be supported only on existing families, to avoid
          // spending work on annoying and unimportant special cases.
          if (col.parent == c.ancestorQCellId!.columnId) {
            cmds.commands.push({
              name: "Add value here",
              callback: () => {
                this.doAddValueHere(c);
              }
            });
          }
        }
      }

      // "New" operation.
      //
      // If col.type === SpecialType.UNIT and there's already an X, it's going to be
      // pretty hard for a "new" trigger to generate a new value, but I'm
      // not going to violate abstraction by adding code just to check for
      // this case. ~ Matt 2017-02-17
      if (col.isObject) {
        if (col.formula != null || getColumnType(col) == SpecialType.TOKEN) {
          let objectName = objectNameForUI(col);
          cmds.commands.push({
            name: (col.formula != null ? "Generate" : "Add") + ` '${objectName}' object here`,
            callback: () => {
              StateEdit.addCell(this.template,
                c.addColumnId!, c.ancestorQCellId!, null, null, this.view.id, standardServerCallback);
            }
          });
          if (c.isObjectCell) cmds.allowEnter = true;
        }
        // Else, no "new" for state keyed objects.
      } else if (col.formula != null) {
        // It seems weird to offer "Generate value here" but not "Add value
        // here" for cells 2 or more levels below the existing ancestor.
        // Let's just not offer it. ~ Matt 2017-05-09
        if (col.parent == c.ancestorQCellId.columnId) {
          cmds.commands.push({
            name: "Generate value here",
            callback: () => {
              StateEdit.addCell(this.template,
                c.addColumnId!, c.ancestorQCellId!, null, null, this.view.id, standardServerCallback);
            }
          });
        }
      }

      return cmds;
    }

    // Search for an empty cell in the family given by addColumnId and
    // ancestorQCellId (precondition: ancestorQCellId.columnId is parent of
    // addColumnId) that can be used to add a value to the family.  If found,
    // start editing the cell and return true, otherwise return false.
    //
    // This is called once from doAddValueHere when the "Add value here" command
    // is invoked, and if necessary, it is called again from rebuildView after
    // the view has been rebuilt with a placeholder.  In the latter case, we
    // make sure the placeholder gets cleaned up after the edit.
    public startEditForAddValue(addColumnId: ColumnId, ancestorQCellId: QCellId) {
      let coords = this.findMatchingCell((c) =>
        EJSON.equals(c.ancestorQCellId, ancestorQCellId) &&
        c.addColumnId == addColumnId &&
        c.qCellId == null &&
        !c.isObjectCell);
      if (coords == null)
        return false;
      // We support the "add value" command only on existing families, and "add
      // to object above" should never trigger on them.
      assert(this.rev.grid[coords.row][coords.col].ancestorQCellIdForAdd == null);
      this.selectSingleCell(coords);
      // https://github.com/handsontable/handsontable/issues/2675#issuecomment-134887851
      let editor = this.hot.getActiveEditor();
      editor.beginEditing();
      // Unclear if we want this.
      editor.enableFullEditMode();
      if (this.template.placeholderFamily.get() != null) {
        // XXX API to manage multiple callbacks?  As of 2017-05-08, this is the
        // only code that sets _closeCallback (Handsontable itself doesn't set
        // it).
        editor._closeCallback = (result) => {
          // If the user saves the edit, then beforeChange will be called before
          // this callback, and if we make it as far as issuing the server call,
          // then StateEdit.addCell will set placeholderSaving.  If that hasn't
          // happened, then remove the placeholder immediately.
          if (!this.template.placeholderSaving)
            this.template.placeholderFamily.set(null);
        };
      }
      return true;
    }

    private doAddValueHere(c: ViewCell) {
      // Without Meteor.defer, sometimes the editing doesn't start properly.
      // My best guess is that the event that was used to invoke the add value
      // command (Ctrl-Enter, or click or Enter on the context menu) is still
      // propagating and somehow confuses the editor.  Meteor.defer seems to
      // allow the event to run its course before we try to start editing.  I
      // don't want to spend the time to really understand what's going on when
      // this code may be obsolete with the new table renderer.
      // ~ Matt 2017-05-08
      Meteor.defer(() => {
        // If the view already contains an empty cell in the requested family,
        // then just start editing it.
        let addColumnId = fixmeAssertNotNull(c.addColumnId);
        let ancestorQCellId = fixmeAssertNotNull(c.ancestorQCellId);
        if (!this.startEditForAddValue(addColumnId, ancestorQCellId)) {
          // If not, then trigger a rebuild of the sheet with a placeholder in
          // the requested family.  rebuildView will then call
          // startEditForAddValue again.
          this.template.placeholderFamily.set({columnId: addColumnId, parentCellId: ancestorQCellId.cellId});
          this.template.placeholderSaving = false;
        }
      });
    }

    public getDeleteCommandForCell(c: ViewCell): null | ContextMenuCommand {
      if ((c.qCellId != null) && c.qCellId.columnId != rootColumnId &&
        StateEdit.canModifyOrRemove(c.qCellId)) {
        let col = getExistingColumn(c.qCellId.columnId);
        return {
          // This currently gives 'Delete object' for the key of a keyed object
          // (deprecated).  If we wanted that case to say 'Delete cell', we
          // would test c.isObjectCell instead.
          name: col.isObject ? "Delete object" : "Delete cell",
          callback: () => {
            StateEdit.removeCell(c.qCellId!, standardServerCallback);
          }
        };
      } else {
        return null;
      }
    }

    public getDemoteCommandForColumn(col: Column): null | ContextMenuCommand {
      if (col._id !== rootColumnId && col.isObject &&
        col.children.length === (getColumnType(col) === SpecialType.TOKEN ? 1 : 0)) {
        let objectName = objectNameForUI(col);
        return {
          name: getColumnType(col) === SpecialType.TOKEN
            ? `Flatten out '${objectName}' objects`
            : `Remove generated '${objectName}' objects`,
          callback: () => {
            MeteorMethods.changeColumnIsObject.call($$.id, col._id, false,
              $$client.wrapCallback(standardServerCallback));
          }
        };
      }
      return null;
    }

    public onKeyDown(event: fixmeAny) {
      let ci, col: undefined | Column, parentCol: undefined | Column;
      if (ActionBar.hasUnsavedData(actionBarOfSheet(this.template))) {
        return;
      }
      this.template.selectedCell = this.getSingleSelectedCell();
      if (event.altKey && event.metaKey) {
        Handsontable.Dom.stopImmediatePropagation(event);
      } else if (!event.altKey && !event.ctrlKey && !event.metaKey) {
        if (event.which === 13) {  // Enter
          if (this.template.selectedCell != null) {
            let cmds = this.getAddCommandsForCell(this.template.selectedCell);
            if (cmds.commands.length > 0 && cmds.allowEnter) {
              Handsontable.Dom.stopImmediatePropagation(event);
              cmds.commands[0].callback();
            }
          }
        } else if (event.which === 46 || event.which === 8) {  // Delete / Backspace
          // Be careful not to hijack focus when an editor is open
          if (this.hot.getActiveEditor().state !== Handsontable.EditorState.EDITING) {
            Handsontable.Dom.stopImmediatePropagation(event);
            for (let cell of this.getMultipleSelectedCells()) {
              let cmd = this.getDeleteCommandForCell(cell);
              if (cmd != null) {
                cmd.callback();
              }
            }
          }
        }
      } else if (event.ctrlKey && !event.altKey && !event.metaKey) {
        if (event.which === 13) {  // Ctrl+Enter
          Handsontable.Dom.stopImmediatePropagation(event);
          if (this.template.selectedCell != null) {
            let cmds = this.getAddCommandsForCell(this.template.selectedCell);
            if (cmds.commands.length > 0) {
              cmds.commands[0].callback();
            }
          }
        } else if (event.which === 89) {  // Ctrl+Y
          MeteorMethods.redo.call($$.id, $$client.wrapCallback(standardServerCallback));
        } else if (event.which === 90) {  // Ctrl+Z
          MeteorMethods.undo.call($$.id, $$client.wrapCallback(standardServerCallback));
        }
      } else if (event.altKey && !event.ctrlKey && !event.metaKey) {
        // Use Alt + Left/Right to reorder columns inside parent
        //     Alt + Up/Down to make column into object/value
        if (event.which === 37 || event.which === 39 || event.which === 38 || event.which === 40) {
          Handsontable.Dom.stopImmediatePropagation(event);
          event.stopPropagation();
          event.preventDefault();

          if ((this.template.selectedCell != null) && ((ci = this.template.selectedCell.columnId) != null) &&
            ((col = getColumn(ci)) != null) && (col.parent != null) &&
            (parentCol = getColumn(col.parent))) {
            if (this.view.id != null) {
              let t = this.view.def().layout;
              let parentNode = assertNotNull(t.find(col.parent));
              let n = parentNode.subtrees.length;
              let index = parentNode.subtrees.indexOf(assertNotNull(t.find(ci)));
              if (event.which === 37 && index > 0) {  // Left
                MeteorMethods.viewReorderColumn.call($$.id, this.view.id, ci, index - 1,
                  $$client.wrapCallback(standardServerCallback));
              } else if (event.which === 39 && index < n - 1) {  // Right
                MeteorMethods.viewReorderColumn.call($$.id, this.view.id, ci, index + 1,
                  $$client.wrapCallback(standardServerCallback));
              }
            } else {
              let n = parentCol.children.length;
              let index = indexOfSupertype(parentCol.children, ci);
              if (event.which === 37 && index > 0) {  // Left
                // BUG: This will have no visible effect if the adjacent column
                // is not in the main view.  Probably the best way to fix this
                // is to have a "default" ViewDoc and not use the order of
                // Column.children.
                MeteorMethods.reorderColumn.call($$.id, ci, index - 1,
                  $$client.wrapCallback(standardServerCallback));
              } else if (event.which === 39 && index < n - 1) {  // Right
                MeteorMethods.reorderColumn.call($$.id, ci, index + 1,
                  $$client.wrapCallback(standardServerCallback));
              } else if (event.which === 38 && !col.isObject) {  // Up
                MeteorMethods.changeColumnIsObject.call($$.id, ci, true,
                  $$client.wrapCallback(standardServerCallback));
              } else if (event.which === 40) {  // Down
                // Check whether this should be possible (i.e., right children)
                // before attempting it so we can detect real errors from the server.
                let cmd = this.getDemoteCommandForColumn(col);
                if (cmd != null) {
                  cmd.callback();
                }
              }
            }
          }
        }
      }
    }

    public selectSingleCell(coords: GridCoords) {
      let {row: r1, col: c1} = coords;
      let cell = this.rev.grid[r1][c1];
      this.hot.selectCell(r1, c1, r1 + cell.rowspan - 1, c1 + cell.colspan - 1);
    }

    private findMatchingCell(predicate: CellPredicate): null | GridCoords {
      for (let i = 0; i < this.rev.grid.length; i++) {
        for (let j = 0; j < this.rev.grid[i].length; j++) {
          if (predicate(this.rev.grid[i][j])) {
            return {row: i, col: j};
          }
        }
      }
      return null;
    }

    public selectMatchingCell(predicate: CellPredicate) {
      let coords = this.findMatchingCell(predicate);
      if (coords != null) {
        this.selectSingleCell(coords);
        return true;
      } else {
        return false;
      }
    }

    public headerExpanded: ReactiveVar<boolean>;
    public toggleHeaderExpanded() {
      this.headerExpanded.set(!this.headerExpanded.get());
    }

  }

  export function rebuildView(template: SpreadsheetTemplateInstance,
    viewId: null | string, sheetOptions: SheetOptions = defaultSheetOptions) {
    if (template.placeholderSaving) {
      template.placeholderSaving = false;
      // As long as the current computation hasn't taken a dependency on
      // placeholderFamily yet, this won't invalidate it and it will just depend
      // on the new value.
      template.placeholderFamily.set(null);
    }

    if (!template.sheetView || !template.sheetView.hot ||
        // XXX: ClientView should just let us change the view after construction.
        template.sheetView.view.id != viewId) {
      if (template.sheetView != null) {
        template.sheetView.hot.destroy();
      }
      template.sheetView = new ClientView(template, new View(viewId), sheetOptions, $("#View")[0]);
    } else {
      template.sheetView.rebuild();
    }
    let sheetView = template.sheetView;

    Tracker.nonreactive(() => {
      // Nothing below should trigger rebuilding of the view if it reads reactive
      // data sources.  (Ouch!)

      let pf = template.placeholderFamily.get();
      if (pf != null) {
        // We can only do one thing at a time.  If both are set, something's wrong.
        assert(template.pendingSelectionPredicate == null);
        // Complete the pending "Add value here" command using the placeholder
        // that was added.
        if (!sheetView.startEditForAddValue(pf.columnId, new FamilyHandle(pf).parent())) {
          console.warn("Failed to add a blank cell to the requested family.  " +
            "This can happen if the family was concurrently deleted.");
          template.placeholderFamily.set(null);
        }
      } else if (template.pendingSelectionPredicate != null &&
        sheetView.selectMatchingCell(template.pendingSelectionPredicate)) {
        template.pendingSelectionPredicate = null;
      } else if (template.selectedCell != null) {
        let selectedCell = template.selectedCell;
        // Try to select a cell similar to the one previously selected.
        let cases: [boolean, CellPredicate][] = [
          [selectedCell.qCellId != null,
          (newCell) => EJSON.equals(selectedCell.qCellId, newCell.qCellId) &&
            selectedCell.isObjectCell === newCell.isObjectCell],
          [selectedCell.addColumnId != null,
          (newCell) => selectedCell.addColumnId == newCell.addColumnId &&
            EJSON.equals(selectedCell.ancestorQCellId, newCell.ancestorQCellId)],
          [selectedCell.addColumnId != null,
          (newCell) => (newCell.kind === "below" || newCell.kind === "tokenObject-below") &&
            EJSON.equals(selectedCell.addColumnId, newCell.columnId)],
          [selectedCell.kind != null,
          (newCell) => selectedCell.kind === newCell.kind &&
            selectedCell.columnId === newCell.columnId],
        ];
        findSimilarCell: {
          for (let [guard, predicate] of cases) {
            if (guard && sheetView.selectMatchingCell(predicate))
              break findSimilarCell;
          }
          sheetView.hot.deselectCell();
        }
      }
      // Make sure various things are consistent with change in table data or
      // selection (view.selectMatchingCell doesn't always seem to trigger this).
      sheetView.onSelection();
      actionBarOfSheet(template).isLoading.set(false);
    });
  }

  function stamp() {
    let d = new Date();
    return d.toString("HH:mm:ss.") + ("000" + d.getMilliseconds()).slice(-3);
  }

  function redrawSheetIfExists(template: SpreadsheetTemplateInstance) {
    if (template.sheetView != null)
      template.sheetView.redraw();
  }

  interface SpreadsheetTemplateInstance extends Blaze.TemplateInstance {
    sheetView: fixmeNullable<ClientView>;
    // Must be a ReactiveVar so that the spreadsheetTemplate helpers get
    // reevaluated after the action bar is first found.
    actionBar: ReactiveVar<fixmeNullable<ActionBar.ActionBarTemplateInstance>>;

    // This may hold a reference to a ViewCell object from an old View.  Weird but
    // shouldn't cause any problem and not worth doing differently.
    selectedCell: null | ViewCell;
    pendingSelectionPredicate: null | CellPredicate;

    placeholderFamily: ReactiveVar<null | QFamilyId>;
    // True means we are saving an addition made using the placeholder and the
    // placeholder will be kept until the save completes or we rebuild the table
    // for another reason (similar to pending changes in beforeChange).  False (in
    // combination with non-null placeholderFamily) means we are still editing or
    // need to start editing.
    placeholderSaving: boolean;

    resizeHandlerId: string;
  }
  /*let spreadsheetTemplate: Blaze.Template1<SpreadsheetTemplateInstance> = Template["Spreadsheet"];
  spreadsheetTemplate.created = function() {
    this.sheetView = null;
    this.actionBar = new ReactiveVar(null);
    this.selectedCell = null;
    this.pendingSelectionPredicate = null;
    this.placeholderFamily = new ReactiveVar(null);
    this.placeholderSaving = false;
  };
  spreadsheetTemplate.rendered = function() {
    // Hack: Find the action bar template instance.
    this.actionBar.set(<ActionBar.ActionBarTemplateInstance>findAncestorTemplateInstanceOfView(
      Blaze.getView(this.find("#ActionBar > *")), ActionBar.actionBarTemplateName));

    this.autorun(() => {
      let data: SpreadsheetTemplateData = Template.currentData();
      // $('body').addClass("present")   # uncomment for presentation mode (read from query string?)
      document.title = data.sheet;
      if (ClientAppHelpers.openOrReload(data.sheet)) {
        let sheetOptions = EJSON.clone(defaultSheetOptions);
        if (data.printable) {
          sheetOptions.rootSpareRows = 0;
          sheetOptions.spareColumns = false;
        }
        ClientAppHelpers.onOpen(() => {
          this.autorun(() => {
            rebuildView(this, undefinedToNull(data.viewId), sheetOptions);
          });
        });
      }
    });*/

    // Resize the sheet when the available space changes.  I don't know of a way
    // to listen for this directly, so listen for the two known causes.  This
    // slows down the action bar expand/collapse a bit, but I think that's less
    // bad than glitchy scrollbars. ~ Matt 2017-03-08
   /* this.autorun(() => {
      ActionBar.isExpanded(actionBarOfSheet(this));
      redrawSheetIfExists(this);
    });
    this.resizeHandlerId = Random.id();
    $(window).on(`resize.${this.resizeHandlerId}`, () => redrawSheetIfExists(this));
  };
  spreadsheetTemplate.destroyed = function() {
    $(window).off(`resize.${this.resizeHandlerId}`);
    document.title = "";
    if (this.sheetView != null) {
      this.sheetView.hot.destroy();
    }
  };

  spreadsheetTemplate.events<SpreadsheetTemplateData>({
    "click .toggleHeaderExpanded": (event, template) => {
      fixmeAssertNotNull(template.sheetView).toggleHeaderExpanded();
    }
  });

  spreadsheetTemplate.helpers<SpreadsheetTemplateData>({
    // TODO: Find a less hacky way to make this happen? ~ Matt 2015-10-01
    actionBarClass: function() {
      let ab = Template.instance(this).actionBar.get();
      return this.printable ? "actionBarHidden" :
        (ab && ActionBar.isExpanded(ab)) ? "actionBarExpanded" : "";
    },
    selectionLockClass: function() {
      let ab = Template.instance(this).actionBar.get();
      return (ab && ActionBar.hasUnsavedData(ab)) ? "selectionLock" : "";
    }
  });
*/
  function actionBarOfSheet(sheet: SpreadsheetTemplateInstance) {
    return fixmeAssertNotNull(sheet.actionBar.get());
  }


export { defaultSheetOptions }