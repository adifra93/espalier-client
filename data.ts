import { ColumnId, NonRootColumnId, rootColumnId, OSType, SpecialType, commonSupertype, getExistingColumn, getColumn, getColumnType, getEditabilityColumn, Column } from "./schema";
import { JSONSet, JSONSetJSON, set, Tree } from "./lib/util";
import {EJSON} from 'ejson';
import { StateFamilies, SheetCollection, AllFamilies, TablespaceId } from "./tablespace";
import { assertNotNull, assert } from "./assert";
import { ReadableModel, readFamilyForFormula, FormulaInternals } from "./formulas";
import { objectOrFieldName, stringifyType, stringifyColumnRef } from "./schema-ui";
import { valueToTextIgnoreErrors } from "./data-ui";
import { arrayContainsGuard } from "./lib/typescript";
import { RuntimeError, runtimeCheck, StaticError } from "./misc";
import { Layout } from "./operations";

enum SpecialType {
  // The empty type, subtype of all other types, used for literal empty sets, etc.
  // Not allowed for state columns.
  EMPTY = "empty",

  ERROR = "error",

  // Main primitive types
  TEXT = "text",
  NUMBER = "number", 
  BOOL = "bool",
  DATE = "date",

  TOKEN = "_token",  // special primitive, should never be user-visible
  UNIT = "_unit",  // primitive, deprecated but still used in ptc
}

// Multisets unsupported for now: twindex removed.

export class Table {
  public Columns: Tree<Column>;
};

var tmp: Column =
{
  _id: rootColumnId,
  parent: null,
  children: null,
  fieldName: null,
  specifiedType: SpecialType.TOKEN,
  isObject: true,
  objectName: null,
  formula: null,
  referenceDisplayColumn: null,
  view: null,
  singular: false,
  isViewObjectRoot: false,
  viewObjectType: null,
  userVisible: true,
  cachedType: SpecialType.TOKEN,
  cachedTypecheckError: null,
  cachedIsReferenced: false,
};

export var $$: Table = new Table();
$$.Columns = new Tree(tmp);

export type ViewObjectRootKey = {viewObjectRoot: true};
export type OSValue = boolean | number | string | CellId | ViewObjectRootKey;
// Dates are represented as Date.getTime values so that OSValue can be
// JSONable, which allows us to use JSONKeyedMap instead of EJSONKeyedMap in
// many places, which is (as of 2017-12-15) a significant performance
// improvement.
export function packDate(value: Date) {
  return value.getTime();
}
export function unpackDate(value: OSValue) {
  return new Date(<number>value);
}
// Direct recursion (type CellId = OSValue[]) is not allowed.  But because
// subtyping is structural, this seems to be equivalent!
export interface CellId extends Array<OSValue> {}

// Qualified cell id
export type QCellId = { columnId: ColumnId, cellId: CellId };
export type QFamilyId = { columnId: NonRootColumnId, parentCellId: CellId };

// OSValue with some or all (depending on context) dates unpacked.
export type OldOSValue = boolean | number | string | Date | OldCellId | ViewObjectRootKey;
export interface OldCellId extends Array<OldOSValue> {}
export type OldQFamilyId = { columnId: NonRootColumnId, parentCellId: OldCellId };

// Const is shallow... still, might as well.  Someday if we want, we can
// freeze all of these data structures. ~ Matt 2017-01-25
export const rootCellId: CellId = [];
export let rootQCellId: QCellId;
//Meteor.startup(() => {  // load order for rootColumnId
//  rootQCellId = {columnId: rootColumnId, cellId: rootCellId};
//});

export function cellIdParent(cellId: CellId): CellId {
  return cellId.slice(0, -1);
}
export function cellIdChild(cellId: CellId, value: OSValue): CellId {
  return cellId.concat([value]);
}
export function cellIdLastStep(cellId: CellId): OSValue {
  return cellId[cellId.length - 1];
}

export class TypedSet {

  constructor(public type: OSType = SpecialType.EMPTY,
    public set: JSONSet<OSValue> = new JSONSet<OSValue>()) {}

  // Note, these can make a meaningless mess if the types are mixed.  The caller
  // has to check @type afterwards.

  public add(xType: OSType, x: OSValue): void {
    this.type = commonSupertype(this.type, xType);
    this.set.add(x);
  }

  public addAll(tset: TypedSet): void {
    this.type = commonSupertype(this.type, tset.type);
    for (let e of tset.set.elements()) {
      this.set.add(e);
    }
  }

  public elements(): OSValue[] {
    return this.set.elements();
  }

  public typeName(): string {
    return "TypedSet";
  }

  public toJSONValue(): TypedSetJSON {
    return {
      type: this.type,
      set: this.set.toJSONValue()
    };
  }

  public static fromJSONValue(json: TypedSetJSON): TypedSet {
    return new TypedSet(json.type, JSONSet.fromJSONValue(json.set));
  }
}
export type TypedSetJSON = {type: OSType, set: JSONSetJSON};
//EJSON.addType("TypedSet", TypedSet.fromJSONValue);

export type FamilyDoc = {
  _id: string;  // as encoded by FamilyHandle.docId
  // Either values or error must be non-null, except during evaluation.  If
  // you insert a document with neither, things will break.  My weird
  // decision.  Change it? ~ Matt 2016-12-12
  values: null /* error */ | OSValue[] /* no duplicates */;
  error: null | string;
};

// This little bit of cleverness avoids us having to store a duplicate
// "column" field in FamilyDoc.  Just hope callers have the sense not to do
// {...familySelectorForColumn(columnId), _id: ...}.  (We don't have a good
// way to catch this at compile time.)
export function familySelectorForColumn(columnId: NonRootColumnId): Mongo.Selector<FamilyDoc> {
  // Or we could use $gt/$lt if the performance is better.
  return {_id: new RegExp("^" + getViewObjectPrefix(columnId) + columnId + ":")};
}

// Maybe there's another way to do this like "values[1] exists".
export const pluralFamilySelector = {
  $nor: [
    {values: {$size: 0}},
    {values: {$size: 1}},
  ]
};

export function pluralFamiliesInStateColumn(columnId: NonRootColumnId) {
  return StateFamilies.find({
    ...familySelectorForColumn(columnId),
    ...pluralFamilySelector
  }).fetch();
}

export type ViewObjectId = string;
export type ViewObjectContext = {
  type: NonRootColumnId;
  id: ViewObjectId;
  queryParams: Layout.ViewObjectQueryParams;
};
// Because view objects are ephemeral, we don't want their IDs to enter
// the computational model.  Since a given computation takes place in
// context of at most one view object, we just have this environment
// variable (on client and server) for the current view object.
//export let currentViewObject = new Meteor.EnvironmentVariable<null | ViewObjectContext>();
export function getViewObjectContext(viewObjectType: NonRootColumnId) {
  let ctx = assertNotNull(currentViewObject.get());
  assert(viewObjectType == ctx.type);
  return ctx;
}
export function getViewObjectPrefix(columnId: NonRootColumnId) {
  let col = getExistingColumn(columnId);
  if (col.viewObjectType != null && !col.isViewObjectRoot) {
    return getViewObjectContext(col.viewObjectType).id + "/";
  } else {
    return "";
  }
}

// This is what we are going to use for now; subject to change.  It takes some
// inspiration from ObjectDiff but is more low-level in some ways and
// friendlier in others. ~ Matt 2018-05-01
export type ViewObjectDump = {
  [columnId: string]: ViewObjectFamilyDump
};
export type ViewObjectFamilyDump =
  OSValue[] |  // fields
  {[stringifiedKey: string]: ViewObjectDump};  // object families

export type ViewObjectRequest = {
  type: NonRootColumnId;
  stateDump: ViewObjectDump;
  queryParams: Layout.ViewObjectQueryParams;
};

// On the server, returns $$server.model for on-demand type checking and
// evaluation.  On the client, returns an object (previously called liteModel)
// that assumes the subscribed tablespace is fully evaluated and just reads
// data from it.
//
// When liteModel was in a common file, we had too many bugs where it got
// unintentionally used on the server with a partially evaluated sheet.  We
// could move it to a client-only file and require all relevant functions in
// common files to take a model parameter, but it's easier to just do this.
// The model parameters could probably now be removed from many existing
// functions, but I'm not doing it now.
//
// XXX This relates to both schema and data, but I don't want to create a
// bunch of new files just to follow the naming conventions for this function.
// ~ Matt 2017-04-11
//
// Without `declare`, TypeScript (reasonably) complains about load order of
// the assignments in `src/{client,server}/data.ts`.  Since this is just a
// declaration and doesn't emit anything, we can use `declare` to suppress
// this error.
//export var getReadableModel: () => ReadableModel;

// Write operations for CellHandle and FamilyHandle differ between client and
// server both in API (callback vs. synchronous) and implementation (Meteor
// method call or real implementation).  They are in src/client/data.ts and
// src/server/data.ts.

// All methods declared in this file that depend on sheet data now perform
// evaluation on demand on the server.  This means that we should no longer
// have bugs where code uses incomplete data on the server, but we can have
// bugs if server code that is making low-level changes to the sheet calls a
// method and triggers evaluation at a bad time.  I think the latter is the
// lesser evil, given that the affected code has to be pretty careful anyway.
// ~ Matt 2017-04-11

export class CellHandle {
  public columnId: ColumnId;
  public cellId: CellId;

  constructor({
      columnId: columnId,
      cellId: cellId
    }: QCellId) {
    this.columnId = columnId;
    this.cellId = cellId;
  }

  public id(): QCellId {
    return {
      columnId: this.columnId,
      cellId: this.cellId
    };
  }

  public parent() {
    let c = getColumn(this.columnId);
    return c && (c.parent != null) ? new CellHandle({
      columnId: c.parent,
      cellId: cellIdParent(this.cellId)
    }) : null;
  }

  public ancestors() {
    let c: null | CellHandle = this;
    let ancestors: CellHandle[] = [];
    while (c != null) {
      ancestors.push(c);
      c = c.parent();
    }
    return ancestors;
  }

  public value() {
    return cellIdLastStep(this.cellId);
  }

  public childFamily(columnId: NonRootColumnId) {
    return new FamilyHandle({
      columnId: columnId,
      parentCellId: this.cellId
    });
  }

  // In principle this belongs in data-ui.ts, but it isn't important.
  // "Name" here is as per `objectOrFieldName`.
  public childFamilyByName(name: string) {
    let lookup = getExistingColumn(this.columnId).children.filter((childId) =>
      objectOrFieldName(getExistingColumn(childId)) == name);
    if (lookup.length != 1) {
      let problem = (lookup.length == 0) ? "not found" : "is ambiguous";
      throw new Meteor.Error("childFamilyByName-name-lookup",
        `field/object name ${problem}: '${name}' (of ${stringifyType(this.columnId)})`);
    }
    return new FamilyHandle({
      columnId: lookup[0],
      parentCellId: this.cellId
    });
  }

  public containingFamily() {
    if (this.columnId === rootColumnId)
      throw new Error("containingFamily on root cell");
    return new FamilyHandle({
      columnId: this.columnId,
      parentCellId: cellIdParent(this.cellId)
    });
  }

  public childFamilies() {
    return getExistingColumn(this.columnId).children.map((childId) => this.childFamily(childId));
  }

  public ref() {
    return new TypedSet(this.columnId, set([this.cellId]));
  }
  public refToTextIgnoreErrors() {
    return valueToTextIgnoreErrors(getReadableModel(), this.columnId, this.cellId);
  }

  // Throws EvaluationError if the containing family failed to evaluate or
  // doesn't exist (see FamilyHandle.read).
  public existsPropagateErrors() {
    if (this.columnId == rootColumnId) return true;
    let col = getExistingColumn(this.columnId);
    if (col.isViewObjectRoot) {
      getViewObjectContext(col._id);  // validation only
      return true;
    }
    let tset = readFamilyForFormula(getReadableModel(), this.containingFamily().id());
    return tset.set.has(cellIdLastStep(this.cellId));
  }

  public existsIgnoreErrors() {
    try {
      return this.existsPropagateErrors();
    } catch (e) {
      if (e instanceof RuntimeError)
        return false;
      else
        throw e;
    }
  }

  // Throws EvaluationError if the cell doesn't exist.  Intended for objects,
  // hence the wording of the error message.
  public checkObjectExists() {
    runtimeCheck(this.existsPropagateErrors(), () => "Object does not exist");
  }
}

export class FamilyHandle {
  public columnId: NonRootColumnId;
  public parentCellId: CellId;

  constructor({
      columnId: columnId,
      parentCellId: parentCellId
    }: QFamilyId) {
    this.columnId = columnId;
    this.parentCellId = parentCellId;
  }

  public id(): QFamilyId {
    return {
      columnId: this.columnId,
      parentCellId: this.parentCellId
    };
  }

  public parent() {
    let c = getExistingColumn(this.columnId);
    return new CellHandle({
      columnId: c.parent,
      cellId: this.parentCellId
    });
  }  // returns a qCellId

  // Returns null if /this/ family failed to evaluate.  Throws an exception if
  // the family doesn't exist for any other reason (see FamilyHandle.read).
  public values() {
    return this.read().values;
  }

  public type() {
    return getColumnType(getExistingColumn(this.columnId));
  }

  // Returns null if /this/ family failed to evaluate, which aligns with the
  // behavior of Model.evaluateFamily.  Throws an exception if the family
  // doesn't exist for any other reason (see FamilyHandle.read).
  public typedValues() {
    let values = this.values();
    return (values != null) ? new TypedSet(this.type(), set(values)) : null;
  }

  public cell(value: OSValue) {
    return new CellHandle({
      columnId: this.columnId,
      cellId: cellIdChild(this.parentCellId, value)
    });
  }

  public docId() {
    // EJSON.stringify(this.id()) would have worked, but this looks nicer in
    // dump files. ~ Matt 2017-01-25
    return getViewObjectPrefix(this.columnId) +
      this.columnId + ":" + EJSON.stringify(this.parentCellId);
  }

  public editabilityFamily(): null | FamilyHandle {
    let col = getExistingColumn(this.columnId);
    let editabilityCol = getEditabilityColumn(col);
    return editabilityCol == null ? null :
      new FamilyHandle({columnId: editabilityCol._id, parentCellId: this.parentCellId});
  }

  // Keep consistent with checkEditable_Server.
  public isEditable() {
    let col = getExistingColumn(this.columnId);
    if (col.formula == null) return !col.isViewObjectRoot;
    if (this.values() == null) return false;
    let efh = this.editabilityFamily();
    if (efh == null) return false;
    let vals = efh.values();
    // Don't throw.
    return vals != null && vals.length == 1 && vals[0] === true;
  }
}

export interface FamilyHandle {
  // Throws an exception if the family doesn't exist because an ancestor
  // object doesn't exist or a proper ancestor family failed to evaluate.  (If
  // this family failed to evaluate, the document containing the error will be
  // returned.)
  read(): FamilyDoc;
}

// Rough inverse of FamilyHandle.docId.
export function qFamilyIdOfFamilyDoc(fam: FamilyDoc): QFamilyId {
  // fam._id.split(":", 2) discards a second colon and anything following it,
  // rather than just not splitting like in every other mainstream programming
  // language. *grumble*
  // C.f. http://stackoverflow.com/questions/4607745/split-string-only-on-first-instance-of-specified-character
  let [, viewObjectPrefix, columnId_, cellIdStr] = assertNotNull(/^((?:[^:/]*\/)?)([^:/]*):(.*)$/.exec(fam._id));
  let columnId = <NonRootColumnId>columnId_;
  assert(viewObjectPrefix == getViewObjectPrefix(columnId), () => "Wrong view object prefix: " + fam._id);
  return {columnId: columnId, parentCellId: EJSON.parse(cellIdStr)};
}

export function stateColumnHasValues(columnId: NonRootColumnId) {
  return StateFamilies.find({
    ...familySelectorForColumn(columnId),
    values: {
      $not: {
        $size: 0
      }
    }
  }).count() > 0;
}

export function allCellIdsInColumnIgnoreErrorsGeneric(
  columnId: ColumnId, collection: SheetCollection<FamilyDoc>): CellId[] {
  if (columnId === rootColumnId) {
    return [rootCellId];
  }
  if (getExistingColumn(columnId).isViewObjectRoot) {
    return [[{viewObjectRoot: true}]];
  }
  let cellIds: CellId[] = [];
  for (let family of collection.find(familySelectorForColumn(columnId)).fetch()) {
    if (family.values != null) {
      for (let v of family.values) {
        cellIds.push(cellIdChild(qFamilyIdOfFamilyDoc(family).parentCellId, v));
      }
    }
  }
  return cellIds;
}

export function allCellIdsInColumnIgnoreErrors(columnId: ColumnId) {
  getReadableModel().evaluateAll();
  return allCellIdsInColumnIgnoreErrorsGeneric(columnId, AllFamilies);
}

// This was pulled out of valueToText so that the 2D layout code could use it
// to bind to either keys or real fields, but the design needs some more
// thought.  (For comparison, the standard nested table layout renderer just
// uses FamilyHandle.read and has separate code for keys.)
export type FieldReader = (model: ReadableModel, value: OSValue) => TypedSet;
export function getFieldReader(col: Column, field: ColumnId): FieldReader {
  if (field == col._id && getColumnType(col) != SpecialType.TOKEN) {
    // The key of a keyed object.
    return (model, value) =>
      FormulaInternals.goUp(model, null, new TypedSet(col._id, set([value])), col._id, true);
  } else if (arrayContainsGuard(col.children, field) && !getExistingColumn(field).isObject) {
    return (model, value) =>
      FormulaInternals.goDown(model, null, new TypedSet(col._id, set([value])), field, null, true);
  } else {
    throw new StaticError(`'${stringifyColumnRef([field, true])}' is not a field of '${stringifyType(col._id)}'`);
  }
}

export function readField(model: ReadableModel, col: Column, value: OSValue, field: ColumnId) {
  return getFieldReader(col, field)(model, value);
}

//export namespace MeteorMethods {
  // Data edits
  //export let replaceCell = new MeteorMethod5<TablespaceId, QCellId, OSValue,
    /*clientViewObjectId*/ //null | string, /*cleanup*/ boolean, void>("replaceCell");
  //export let recursiveDeleteCell = new MeteorMethod4<TablespaceId, QCellId,
    /*clientViewObjectId*/ //null | string, /*cleanup*/ boolean, void>("recursiveDeleteCell");
//}

