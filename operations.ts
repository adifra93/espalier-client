import { Fun } from "./lib/type-funcs";
import { SheetCollection } from "./tablespace";
import { giRun } from "./generic-index";

export const F_LayoutsCollection = Symbol();
export type F_LayoutsCollection = Fun<typeof F_LayoutsCollection, never>;
export const F_OrigLayoutTemplate = Symbol();
export type F_OrigLayoutTemplate = Fun<typeof F_OrigLayoutTemplate, never>;
export const F_GenericIndexLayoutMethods = Symbol();
export type F_GenericIndexLayoutMethods = Fun<typeof F_GenericIndexLayoutMethods, never>;
export interface TypeFuncs<C, X> {
  [F_LayoutsCollection]: SheetCollection<Layout.LayoutDoc<X>>;
  [F_OrigLayoutTemplate]: Layout.LayoutTemplate<X, Layout.OrigB>;
  [F_GenericIndexLayoutMethods]: Layout.GenericIndexLayoutMethods<X>;
}

export namespace Layout {

  // General caveats about the structure editing code:
  // 1. The server operations assume they are called by
  //    src/layout/client/commands.ts after passing the checks there.  We
  //    could duplicate the checks in the server operations if we cared
  //    enough.
  // 2. The code does not defend against concurrent structure edits.  Common
  //    places they can occur include between building the context menu and
  //    choosing a command, between client-side command checks and the server
  //    operation (see #1), between client-side command initiation and the
  //    attempt to update the selection after the command, and while a move is
  //    pending.
  // 3. The code leaves some errors that are rare and tedious to check for to
  //    be caught by layout template validation in defineLayoutMethod, which
  //    will roll back the transaction.  Currently, I believe the only such
  //    error is "Allocation covers split end cap".  (However, subject to all
  //    other documented caveats, the code is not supposed to successfully
  //    execute an unexpected edit.)
  //
  // Structure editing is very complex as it is and I just don't think it's
  // worth attempting a more robust implementation at this stage of the
  // project.  Even if I tried to add more checks, it would be additional work
  // to test them and I'd be likely to miss something anyway.
  //
  // ~ Matt 2018-03-26

  export type LayoutDoc<_> = LayoutTemplateSpec<_, OrigB>;
  export function getLayoutsCollection<_>(giWrapper: GIWrapper<_>) {
    return giWrapper.unwrap<F_LayoutsCollection>(neverGIWrapper.wrap<F_LayoutsCollection>(Layouts));
  }

  export let axisSpanNames: IndependentPerAxis<string>;
  export let axisToCollectionDirection: IndependentPerAxis<string>;
  export let sideNames: IndependentPerAxis<PerExtreme<string>>;
  //Meteor.startup(() => {
  //  axisSpanNames = {[Axis.ROW]: "row", [Axis.COL]: "column"};
  //  axisToCollectionDirection = {[Axis.ROW]: "vertical", [Axis.COL]: "horizontal"};
  //  sideNames = {
   //   [Axis.ROW]: {[Extreme.FIRST]: "above", [Extreme.LAST]: "below"},
   //   [Axis.COL]: {[Extreme.FIRST]: "to the left", [Extreme.LAST]: "to the right"},
   // };
  //});
  export function uiTermForSplit(splitAxis: Axis, isVariable: boolean) {
    return isVariable ? "collection" : axisSpanNames[splitAxis] + " group";
  }

  // "Good" = can contain fields and splits, i.e., bound and not extra space.
  export function getTemplateRectBindingIfGood<_>(
    layoutTemplate: LayoutTemplate<_, OrigB>, rect: RectangleId<_, TemplateS, OrigB>):
    null | TemplateRectBinding<_, OrigB> {
    let binding = layoutTemplate.getExistingRectBinding(rect);
    if (binding.trp != null &&
      axesEvery((a) => !layoutTemplate.extraSpaceSpans[a].has(rect[a]))) {
      return binding;
    } else {
      return null;
    }
  }

  export type AddAllocationCreateFieldCallback = (parent: ColumnId) => NonRootColumnId;

  // Eagerly create a real allocation for every leaf rectangle (including a real
  // field for non-extra-space allocations).  Obviously this doesn't scale well,
  // but for the moment we can afford it and it saves us complexity throughout
  // the custom layout UI and API.  Eventually, we'll probably move to some
  // "spare allocations" scheme.
  //
  // Returns the new layout template spec, but adds fields to the schema as a
  // side effect (unless createFieldCB = null)!
  export function coverLeavesGeneric<_>(layoutTemplate: LayoutTemplate<_, OrigB>,
    createFieldCB: null | AddAllocationCreateFieldCallback): LayoutTemplateSpec<_, OrigB> {
    let uncoveredLeaves = fixmeAssertNotNull(layoutTemplate.archetype.uncoveredLeaves);
    if (uncoveredLeaves.length == 0)
      return layoutTemplate.spec;
    let newSpec = shallowishCloneLayoutTemplateSpec(layoutTemplate.spec);
    for (let rect of uncoveredLeaves) {
      addAllocationGeneric(layoutTemplate, newSpec, perAxis((a) => mergedSpanOfSingleSpan(rect[a])), createFieldCB);
    }
    return newSpec;
  }

  // Assumption: prevLayoutTemplate has the same splits as spec.
  export function addAllocationGeneric<_>(
    prevLayoutTemplate: LayoutTemplate<_, OrigB>, spec: LayoutTemplateSpec<_, OrigB>,
    mergedRect: MergedRectId<_, TemplateS, OrigB>, createFieldCB: null | AddAllocationCreateFieldCallback) {
    let container: RectangleId<_, TemplateS, OrigB> = perAxis((a) =>
      prevLayoutTemplate.archetype.containerOfMergedSpan(a, mergedRect[a]));
    let binding = getTemplateRectBindingIfGood(prevLayoutTemplate, container);
    let allocDefCommon: AllocationDefCommon<_, TemplateS, OrigB> = {
      mergedSpans: mergedRect,
      style: {},
      backgroundAllocationInfo: null,
    };
    let alloc: TemplateAllocationDef<_, OrigB>;
    if (binding != null && createFieldCB != null) {
      let field = createFieldCB(fixmeAssertNotNull(binding.trp).type);
      alloc = {...allocDefCommon, kind: TemplateAllocationDefKind.FIELD, field: field};
    } else {
      alloc = {...allocDefCommon, kind: TemplateAllocationDefKind.LABEL, text: ""};
    }
    spec.allocations.set(randomTemplateAllocationId(), alloc);
  }
  export type AddSplitParams<A extends AxisL> = {
    splitAxis: A;
    parentSpan: TemplateSpanId<OrigB, A>;
    mergedScope: PerExtreme<TemplateSpanId<OrigB, OtherAxis<A>>>;
    // TODO: Want to replace with TemplateSplitDefKind now?
    isVariable: boolean;
  };

  export type AddSplitResponse<A extends AxisL> = {
    newSplitId: TemplateSplitId<OrigB, A>;
  };

  export type DeleteSplitParams<A extends AxisL> = {
    splitAxis: A;
    splitId: TemplateSplitId<OrigB, A>;
  };

  export type MoveAllocationParams<_> = {
    // Should be ID of a non-label allocation.  We currently don't support
    // moving label allocations in unbound or extra-space areas.
    allocationId: TemplateAllocationId<OrigB>;
    // Must be in same object type as before.
    newMergedSpans: MergedRectId<_, TemplateS, OrigB>;
  };

  export type ChangeAllocationKindParams = {
    allocationId: TemplateAllocationId<OrigB>;  // kind not LABEL
    newKind: TemplateAllocationDefKind;  // not LABEL
  };

  export type ChangeAllocationButtonProcedureParams = {
    allocationId: TemplateAllocationId<OrigB>;  // kind BUTTON
    newProcedureName: null | string;
  };

  export type MoveSplitParams<A extends AxisL> = {
    splitAxis: A;
    splitId: TemplateSplitId<OrigB, A>;
    // Must be in same object type as before.
    newParentSpan: TemplateSpanId<OrigB, A>;
    newMergedScope: MergedSpanId<TemplateS, OrigB, OtherAxis<A>>;
  };

  export type InsertSpanParams<A extends AxisL> = {
    splitAxis: A;
    neighbor: TemplateSpanId<OrigB, A>;
    insertOnSide: Extreme;
  };

  export type DeleteSpanParams<A extends AxisL> = {
    splitAxis: A;
    spanToDelete: TemplateSpanId<OrigB, A>;
  };

  export type ChangeStyleToValueParams = {
    templateAllocId: TemplateAllocationId<OrigB>;
    value: string | boolean;
  };
  export type ChangeBorderStyleParams = {
    templateAllocId: TemplateAllocationId<OrigB>;
    // set is not ejsonable, so using array representation here
    borderSelected: string;
    value: string | boolean;
  };
  export type ChangeSpanHidingParams<A extends AxisL> = {
    splitAxis: A;
    spanId: TemplateSpanId<OrigB, A>;
    hide: boolean;
  };

  export type PromoteParams<A> = {
    // The allocation must be a field (so, not extra space) and its span on
    // `axis` must be a leaf.
    allocationId: TemplateAllocationId<OrigB>;
    axis: A;
  };

  export type DemoteParams<A extends AxisL> = {
    splitAxis: A;
    // Must be a variable split with no descendants in the main child except a
    // single allocation that covers the entire thing.  For a state
    // collection, the server will check that no instance of the split
    // contains more than one element.
    splitId: TemplateSplitId<OrigB, A>;
  };

  export type ColumnIdAndFormula = {
    columnId: NonRootColumnId;
    formula: null | Formula;
  };

  export function fieldIsUsed(col: NonRootColumn) {
    return col.isObject /* key fields are always used */ ||
      col.fieldName != null ||
      col.specifiedType != (col.formula != null ? null : DEFAULT_STATE_FIELD_TYPE) ||
      (col.formula != null ? !EJSON.equals(col.formula, DUMMY_FORMULA) :
        // For view objects: Model.addViewObject does not allow data in
        // unnamed state fields, and named state fields are covered by the
        // `col.isObject` and `col.fieldName != null` cases above.
        col.viewObjectType == null && stateColumnHasValues(col._id)) ||
      getColumnIsReferenced(col);
  }
  export function allocationContentIsUsed<_>(def: TemplateAllocationDef<_, OrigB>) {
    switch (def.kind) {
      case TemplateAllocationDefKind.FIELD:
        return fieldIsUsed(getExistingColumn(def.field));
      case TemplateAllocationDefKind.HYPERLINK:
      case TemplateAllocationDefKind.BUTTON:
        return true;
      case TemplateAllocationDefKind.LABEL:
        // Should be true for layouts built in the UI.  Structure editing is
        // disabled for hard-coded layouts, so they won't get here.
        assert(def.text == "");
        return false;
    }
  }
  // An allocation is non-leaf iff its container is non-leaf.
  export function allocationIsNonLeaf<_>(
    container: RectangleId<_, TemplateS, OrigB>, layoutTemplate: LayoutTemplate<_, OrigB>) {
    let ri = layoutTemplate.archetype.getRectInfo(container);
    // https://github.com/palantir/tslint/issues/3667
    // tslint:disable-next-line:strict-type-predicates
    return axesSome((a) => ri.coveringSplits[a] != null);
  }
  // For now, an allocation is considered "used" if its content is used or it
  // is non-leaf.
  export function allocationIsKeyField<_>(def: TemplateAllocationDef<_, OrigB>) {
    if (def.kind != TemplateAllocationDefKind.FIELD) return false;
    return getExistingColumn(def.field).isObject;
  }

  export function traverseMergedRect<_>(layoutTemplate: LayoutTemplate<_, OrigB>,
    visitor: GeneralLayoutVisitor<_, TemplateS, OrigB>, start: MergedRectId<_, TemplateS, OrigB>) {
    for (let rect of layoutTemplate.archetype.getRectsInMergedRect(start)) {
      layoutTemplate.archetype.traverseAll(visitor, rect);
    }
  }

  export function requireNoSplits<_, A extends AxisG<_>>(layoutTemplate: LayoutTemplate<_, OrigB>,
    splitAxis: A, parentSpan: TemplateSpanId<OrigB, A>, mergedScope: MergedSpanId<TemplateS, OrigB, OtherAxis<A>>) {
    let fail = false;
    traverseMergedRect(layoutTemplate, {
      preRect: (rect) => {
        let ri = layoutTemplate.archetype.getRectInfo(rect);
        if (ri.coveringSplits[splitAxis]) {
          fail = true;
        }
      }
    }, axisCons(splitAxis, mergedSpanOfSingleSpan(parentSpan), mergedScope));
    return !fail;
  }

  export type PrepareToClearAreaResult = {
    unusedAllocationIds: Set<TemplateAllocationId<OrigB>>;
    usedAllocationIds: Set<TemplateAllocationId<OrigB>>;
  };
  export function prepareToClearArea<_>(layoutTemplate: LayoutTemplate<_, OrigB>,
    mergedRect: MergedRectId<_, TemplateS, OrigB>): PrepareToClearAreaResult {
    let result: PrepareToClearAreaResult = {
      unusedAllocationIds: new Set(),
      usedAllocationIds: new Set()
    };
    let allocationsSeen = new Set();
    traverseMergedRect(layoutTemplate, {
      preRect: (rect) => {
        let alloc = layoutTemplate.archetype.getRectInfo(rect).coveringAllocation;
        if (alloc != null && !allocationsSeen.has(alloc.def.templateAllocationId)) {
          allocationsSeen.add(alloc.def.templateAllocationId);
          let used = allocationIsNonLeaf(alloc.container, layoutTemplate) ||
            allocationContentIsUsed(layoutTemplate.templateAllocationOfArchetypalAllocation(alloc));
          (used ? result.usedAllocationIds : result.unusedAllocationIds)
            .add(alloc.def.templateAllocationId);
        }
      }
    }, mergedRect);
    return result;
  }

  export type PrepareToDeleteSpanResult<_, A extends Axis> = {
    fail: boolean;
    deletingUsedAllocations: boolean;
    allocationIdsToDelete: Set<TemplateAllocationId<OrigB>>;
    allocationsToResize: Map<TemplateAllocationId<OrigB>, MergedRectId<_, TemplateS, OrigB>>;
    // Note: these are only splits on otherAxis(splitAxis).
    splitsToResize: Map<TemplateSplitId<OrigB, OtherAxis<A>>, MergedSpanId<TemplateS, OrigB, A>>;
  };
  export function prepareToDeleteSpan<_, A extends Axis>(layoutTemplate: LayoutTemplate<_, OrigB>,
    {splitAxis, spanToDelete}: DeleteSpanParams<A>) {
    let scopeAxis = otherAxis(splitAxis);
    let splitDef = layoutTemplate.parentSplitDefOfSpan(splitAxis, spanToDelete);
    if (splitDef == null || splitDef.kind != TemplateSplitDefKind.FIXED)  // narrowing
      throw new StaticError("Can only delete a child of a fixed split");
    let fixedSplitDef = splitDef;
    let spanIndex = splitDef.children.indexOf(spanToDelete);

    let result: PrepareToDeleteSpanResult<_, A> = {
      fail: false,
      deletingUsedAllocations: false,
      allocationIdsToDelete: new Set(),
      allocationsToResize: new Map(),
      splitsToResize: new Map()
    };

    let adjustMergedSpan = (origMergedSpan: MergedSpanId<TemplateS, OrigB, A>, callbacks: {
      delete(): void;
      resize(newMergedSpan: MergedSpanId<TemplateS, OrigB, A>): void;
    }) => {
      if (extremes.every((e) => origMergedSpan[e] == spanToDelete)) {
        callbacks.delete();
      } else {
        for (let e of extremes) {
          if (origMergedSpan[e] == spanToDelete) {
            let newMergedSpan = EJSON.clone(origMergedSpan);
            newMergedSpan[e] = fixedSplitDef.children[
              spanIndex + {[Extreme.FIRST]: 1, [Extreme.LAST]: -1}[e]];
            callbacks.resize(newMergedSpan);
          }
        }
      }
    };

    for (let [allocId, allocDef] of layoutTemplate.spec.allocations.entries()) {
      adjustMergedSpan(allocDef.mergedSpans[splitAxis], {
        delete: () => {
          if (allocationIsKeyField(allocDef)) {
            result.fail = true;
          } else {
            result.allocationIdsToDelete.add(allocId);
            let container = layoutTemplate.containerOfTemplateAlloc(allocDef);
            if (allocationContentIsUsed(allocDef) || allocationIsNonLeaf(container, layoutTemplate)) {
              result.deletingUsedAllocations = true;
            }
          }
        },
        resize: (newMergedSpan) => {
          result.allocationsToResize.set(allocId,
            axisCons(splitAxis, newMergedSpan, allocDef.mergedSpans[scopeAxis]));
        }
      });
    }

    for (let otherSplitDef of layoutTemplate.spec.splits[splitAxis].values()) {
      if (otherSplitDef.parentSpan == spanToDelete) {
        result.fail = true;
      }
    }
    for (let [otherSplitId, otherSplitDef] of layoutTemplate.spec.splits[scopeAxis].entries()) {
      let otherMergedScope = unwrapDoubleOtherAxis<_, MergedRectId<_, TemplateS, OrigB>, A>
        (otherSplitDef.mergedScope);
      adjustMergedSpan(otherMergedScope, {
        delete: () => {
          result.fail = true;
        },
        resize: (newMergedSpan) => {
          result.splitsToResize.set(otherSplitId, newMergedSpan);
        }
      });
    }

    return result;
  }

  export type SplitDescendants<_, A extends Axis> = {
    sameAxisSplitIds: TemplateSplitId<OrigB, A>[];  // Does not include self
    otherAxisSplitIds: TemplateSplitId<OrigB, OtherAxis<A>>[];
    allocationIds: TemplateAllocationId<OrigB>[];  // May include extra-space allocations!
    crossBindingRects: RectangleId<_, TemplateS, OrigB>[];
  };
  export function findSplitDescendants<_, A extends Axis>(layoutTemplate: LayoutTemplate<_, OrigB>,
    splitAxis: A, splitId: TemplateSplitId<OrigB, A>): SplitDescendants<_, A> {
    let splitDef = assertNotNull(layoutTemplate.spec.splits[splitAxis].get(splitId));
    let scopeAxis = otherAxis(splitAxis);
    // A split or allocation is a descendant of the given split iff it (1)
    // covers some rectangle in the given split's area and it does /not/ (2)
    // cover the parent span of the given split.  Make sets of splits and
    // allocations that satisfy #1 and #2 and take the difference.  Another
    // approach I considered was, when we detect that a construct covers
    // consRect(splitAxis, x, y), to check whether it also covers
    // consRect(splitAxis, splitDef.parentSpan, y), but the latter might not
    // be a valid rectangle.
    let sameAxisSplitIds = new Set<TemplateSplitId<OrigB, A>>();
    let otherAxisSplitIds = new Set<TemplateSplitId<OrigB, OtherAxis<A>>>();
    let inheritedOtherAxisSplitIds = new Set<TemplateSplitId<OrigB, OtherAxis<A>>>();
    let allocationIds = new Set<TemplateAllocationId<OrigB>>();
    let inheritedAllocationIds = new Set<TemplateAllocationId<OrigB>>();
    let crossBindingRects: RectangleId<_, TemplateS, OrigB>[] = [];
    traverseMergedRect(layoutTemplate, {
      preRect: (rect) => {
        let ri = layoutTemplate.archetype.getRectInfo(rect);
        let atParentSpan = (rect[splitAxis] == splitDef.parentSpan);
        let sameAxisSplit = ri.coveringSplits[splitAxis], otherAxisSplit = ri.coveringSplits[scopeAxis];
        // https://github.com/palantir/tslint/issues/3667
        // tslint:disable-next-line:strict-type-predicates
        if (!atParentSpan && sameAxisSplit != null) {
          sameAxisSplitIds.add(sameAxisSplit.def.templateSplitId);
        }
        // https://github.com/palantir/tslint/issues/3667
        // tslint:disable-next-line:strict-type-predicates
        if (otherAxisSplit != null) {
          otherAxisSplitIds.add(otherAxisSplit.def.templateSplitId);
          if (atParentSpan) {
            inheritedOtherAxisSplitIds.add(otherAxisSplit.def.templateSplitId);
          }
        }
        if (ri.coveringAllocation != null) {
          allocationIds.add(ri.coveringAllocation.def.templateAllocationId);
          if (atParentSpan) {
            inheritedAllocationIds.add(ri.coveringAllocation.def.templateAllocationId);
          }
        }
        if (!atParentSpan && layoutTemplate.spec.crossBindings.get(rect) != null) {
          crossBindingRects.push(rect);
        }
      }
    }, axisCons(splitAxis, mergedSpanOfSingleSpan(splitDef.parentSpan), splitDef.mergedScope));
    for (let otherSplitId of inheritedOtherAxisSplitIds) {
      otherAxisSplitIds.delete(otherSplitId);
    }
    for (let allocId of inheritedAllocationIds) {
      allocationIds.delete(allocId);
    }
    return {
      sameAxisSplitIds: Array.from(sameAxisSplitIds),
      otherAxisSplitIds: Array.from(otherAxisSplitIds),
      allocationIds: Array.from(allocationIds),
      crossBindingRects
    };
  }

  export function previewCutSplit<_, A extends Axis>(layoutTemplate: LayoutTemplate<_, OrigB>,
    splitAxis: A, splitId: TemplateSplitId<OrigB, A>, descendants: SplitDescendants<_, A>):
    LayoutTemplateSpec<_, OrigB> {
    let scopeAxis = otherAxis(splitAxis);
    let newSpec = shallowishCloneLayoutTemplateSpec(layoutTemplate.spec);
    newSpec.splits[splitAxis].delete(splitId);
    for (let otherSplitId of descendants.sameAxisSplitIds) {
      newSpec.splits[splitAxis].delete(otherSplitId);
    }
    for (let otherSplitId of descendants.otherAxisSplitIds) {
      newSpec.splits[scopeAxis].delete(otherSplitId);
    }
    for (let allocId of descendants.allocationIds) {
      newSpec.allocations.delete(allocId);
    }
    for (let crossBindingRect of descendants.crossBindingRects) {
      newSpec.crossBindings.delete(crossBindingRect);
    }
    return newSpec;
  }

  export class MeteorAxisGenericLayoutMethod<_, A3 extends {[A in AxisG<_>]: EJSONable},
    R extends {[A in AxisG<_>]: EJSONable | void},
    //G extends Do_not_mess_with_this_type_parameter = never> extends MeteorMethodBase {
    //constructor(name: string & Check_<_, G>) {
    //  super(name);
    //}

    //define(func: <A extends Axis>(this: Meteor.MethodInvocation,
    //  a1: TablespaceId, a2: string, a3: A3[A]) => R[A]) {
    //  super.define(func);
   // }
   // call<A extends Axis = never>(a1: TablespaceId, a2: string, a3: A3[A],
    //  callback: MeteorCallback<R[A]> & Check_<_, G>) {
    //  return super.call(a1, a2, a3, callback);
   // }
  }

  export class GenericIndexLayoutMethods<_> {
    constructor(public giWrapper: GIWrapper<_>) {}
    //layoutAddSplit =
      //new MeteorAxisGenericLayoutMethod<_,
       // {[A in AxisG<_>]: AddSplitParams<A>},
        //{[A in AxisG<_>]: AddSplitResponse<A>}>("layoutAddSplit");
    /*layoutDeleteSplit =
      new MeteorAxisGenericLayoutMethod<_,
        {[A in AxisG<_>]: DeleteSplitParams<A>},
        {[A in AxisG<_>]: void}>("layoutDeleteSplit");
    layoutMoveSplit =
      new MeteorAxisGenericLayoutMethod<_,
        {[A in AxisG<_>]: MoveSplitParams<A>},
        {[A in AxisG<_>]: void}>("layoutMoveSplit");
    layoutMakeAllocation =
      new MeteorMethod3<TablespaceId, string,
        MergedRectId<_, TemplateS, OrigB>, void>("layoutMakeAllocation");
    layoutPurgeAllocation =
      new MeteorMethod3<TablespaceId, string, TemplateAllocationId<OrigB>, void>("layoutPurgeAllocation");
    layoutMoveAllocation =
      new MeteorMethod3<TablespaceId, string,
        MoveAllocationParams<_>, void>("layoutMoveAllocation");
    layoutChangeAllocationKind =
      new MeteorMethod3<TablespaceId, string, ChangeAllocationKindParams, void>("layoutChangeAllocationKind");
    layoutChangeAllocationButtonProcedure =
      new MeteorMethod3<TablespaceId, string, ChangeAllocationButtonProcedureParams, void>
        ("layoutChangeAllocationButtonProcedure");
    layoutInsertSpan =
      new MeteorAxisGenericLayoutMethod<_,
        {[A in AxisG<_>]: InsertSpanParams<A>},
        {[A in AxisG<_>]: TemplateSpanId<OrigB, A>}>("layoutInsertSpan");
    layoutDeleteSpan =
      new MeteorAxisGenericLayoutMethod<_,
        {[A in AxisG<_>]: DeleteSpanParams<A>},
        {[A in AxisG<_>]: void}>("layoutDeleteSpan");
    layoutPromote =
      new MeteorAxisGenericLayoutMethod<_,
        {[A in AxisG<_>]: PromoteParams<A>},
        {[A in AxisG<_>]: void}>("layoutPromote");
    layoutDemote =
      new MeteorAxisGenericLayoutMethod<_,
        {[A in AxisG<_>]: DemoteParams<A>},
        {[A in AxisG<_>]: void}>("layoutDemote");
    layoutChangeSpanHiding =
      new MeteorAxisGenericLayoutMethod<_,
        {[A in AxisG<_>]: ChangeSpanHidingParams<A>},
        {[A in AxisG<_>]: void}>("layoutChangeSpanHiding"); */
  }
  const genericIndexLayoutMethods = giRun((w) =>
    w.wrap<F_GenericIndexLayoutMethods>(new GenericIndexLayoutMethods(w)));
  export function getGenericIndexLayoutMethods<_>(w: GIWrapper<_>) {
    return w.unwrap<F_GenericIndexLayoutMethods>(genericIndexLayoutMethods);
  }

}

export namespace MeteorMethods {
  /* export let layoutInit = new MeteorMethod1<TablespaceId, void>("layoutInit");
  export let layoutCreateViewObjectType =
    new MeteorMethod1<TablespaceId, NonRootColumnId>("layoutCreateViewObjectType");
  export let layoutDeleteViewObjectType =
    new MeteorMethod2<TablespaceId, NonRootColumnId, void>("layoutDeleteViewObjectType");
  export let layoutChangeColumnFieldNameAndFormula =
    new MeteorMethod4<TablespaceId, ColumnId, null | string, null | Formula, void>
      ("layoutChangeColumnFieldNameAndFormula");
  // Currently used for hyperlink text and URL formulas.
  export let layoutChangeMultipleColumnFormulas =
    new MeteorMethod2<TablespaceId, Layout.ColumnIdAndFormula[], void>
      ("layoutChangeMultipleColumnFormulas");
  export let layoutBoldText =
    new MeteorMethod3<TablespaceId, string, Layout.TemplateAllocationId<Layout.OrigB>, void>("layoutBoldText");
  export let layoutItalicizeText =
    new MeteorMethod3<TablespaceId, string, Layout.TemplateAllocationId<Layout.OrigB>, void>("layoutItalicizeText");
  export let layoutUnderlineText =
    new MeteorMethod3<TablespaceId, string, Layout.TemplateAllocationId<Layout.OrigB>, void>("layoutUnderlineText");
  export let layoutChangeBackgroundColor =
    new MeteorMethod3<TablespaceId, string, Layout.ChangeStyleToValueParams, void>("layoutChangeBackgroundColor");
  export let layoutChangeTextFont =
    new MeteorMethod3<TablespaceId, string, Layout.ChangeStyleToValueParams, void>("layoutChangeTextFont");
  export let layoutChangeTextAlignment =
    new MeteorMethod3<TablespaceId, string, Layout.ChangeStyleToValueParams, void>("layoutChangeTextAlignment");
  export let layoutChangeFontSize =
    new MeteorMethod3<TablespaceId, string, Layout.ChangeStyleToValueParams, void>("layoutChangeFontSize");
  export let layoutChangeTextColor =
    new MeteorMethod3<TablespaceId, string, Layout.ChangeStyleToValueParams, void>("layoutChangeTextColor");
  export let layoutChangeBorderVisibility =
    new MeteorMethod3<TablespaceId, string, Layout.ChangeBorderStyleParams, void>("layoutChangeBorderVisibility");
  export let layoutChangeBorderColor =
    new MeteorMethod3<TablespaceId, string, Layout.ChangeBorderStyleParams, void>("layoutChangeBorderColor"); */
}


