
// Utility function to translate CoffeeScript "a ? b".
// ~ Matt 2016-01-31
export function fallback<T>(a: T | null, b: T): T;
export function fallback<T>(a: T | undefined, b: T): T;
export function fallback<T>(a: T | null | undefined, b: T): T;
export function fallback<T>(a: T | null | undefined, b: T): T {
  return (a != null) ? a : b;
}


