// Utility definitions not specific to Object Spreadsheets.
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Objsheets;
(function (Objsheets) {
    Objsheets.equalByIdentityBrand = Symbol();
    // A JSONableInternPool can intern any JSONable object by its JSON
    // representation (key order sensitive).  Interning applies only to the entire
    // objects, not their sub-objects.
    var JSONableInternPool = /** @class */ (function () {
        function JSONableInternPool() {
            this.set = new Set();
            this.map = new Map();
        }
        JSONableInternPool.prototype.intern = function (x) {
            // XXX This cast might not be necessary with smarter narrowing.
            if (x == null)
                return x;
            if (this.set.has(x)) {
                return x;
            }
            else {
                var str = JSON.stringify(x);
                var xi = this.map.get(str);
                // https://github.com/palantir/tslint/issues/3222
                // tslint:disable-next-line:strict-type-predicates
                if (xi == null) {
                    xi = x;
                    // XXX: Why is this cast needed??
                    this.map.set(str, xi);
                }
                return xi;
            }
        };
        return JSONableInternPool;
    }());
    Objsheets.JSONableInternPool = JSONableInternPool;
    // FIXME: EJSONKeyedMap is effectively using "key order sensitive"
    // EJSON.equals comparison of keys, though we use regular EJSON.equals
    // throughout the codebase.  Apparently we've been consistent enough with key
    // order to never notice until now. ~ Matt 2017-12-07
    var EJSONKeyedMap = /** @class */ (function () {
        function EJSONKeyedMap(ents) {
            if (ents === void 0) { ents = []; }
            // XXX: Switch to ECMAScript 6 Map?  It makes toJSONValue/fromJSONValue a
            // little more complicated.
            this.obj = Object.create(null);
            for (var _i = 0, ents_1 = ents; _i < ents_1.length; _i++) {
                var _a = ents_1[_i], k = _a[0], v = _a[1];
                if (this.get(k) !== undefined)
                    throw new Error("Duplicate key during EJSONKeyedMap construction: " + this.stringifyKey(k));
                this.set(k, v);
            }
        }
        EJSONKeyedMap.prototype.stringifyKey = function (k) {
            return EJSON.stringify(k);
        };
        EJSONKeyedMap.prototype.parseKey = function (k) {
            return EJSON.parse(k);
        };
        EJSONKeyedMap.prototype.get = function (k) {
            return this.obj[this.stringifyKey(k)];
        };
        EJSONKeyedMap.prototype.set = function (k, v) {
            this.obj[this.stringifyKey(k)] = v;
        };
        EJSONKeyedMap.prototype["delete"] = function (k) {
            delete this.obj[this.stringifyKey(k)];
        };
        EJSONKeyedMap.prototype.size = function () {
            return _.size(this.obj);
        };
        EJSONKeyedMap.prototype.keys = function () {
            var keys = [];
            for (var wk in this.obj) {
                keys.push(this.parseKey(wk));
            }
            return keys;
        };
        EJSONKeyedMap.prototype.values = function () {
            var values = [];
            for (var wk in this.obj) {
                values.push(this.obj[wk]);
            }
            return values;
        };
        EJSONKeyedMap.prototype.entries = function () {
            var entries = [];
            for (var wk in this.obj) {
                var v = this.obj[wk];
                entries.push([this.parseKey(wk), v]);
            }
            return entries;
        };
        EJSONKeyedMap.prototype.shallowClone = function () {
            return new EJSONKeyedMap(this.entries());
        };
        EJSONKeyedMap.prototype.typeName = function () {
            return "EJSONKeyedMap";
        };
        EJSONKeyedMap.prototype.toJSONValue = function () {
            return EJSON.toJSONValue(this.obj);
        };
        EJSONKeyedMap.fromJSONValue = function (json) {
            var m = new EJSONKeyedMap();
            m.obj = Object.assign(Object.create(null), EJSON.fromJSONValue(json));
            return m;
        };
        return EJSONKeyedMap;
    }());
    Objsheets.EJSONKeyedMap = EJSONKeyedMap;
    EJSON.addType("EJSONKeyedMap", EJSONKeyedMap.fromJSONValue);
    // When the keys are JSONable, JSONKeyedMap can be used instead of
    // EJSONKeyedMap and avoids the significant overhead of scanning the keys for
    // items that need EJSON conversion (even if none are found).
    //
    // JSONKeyedMap assumes the keys do not contain JSONable data that has special
    // meaning to EJSON as a representation of an EJSON special type.
    //
    // FIXME: Keys are key order sensitive (see EJSONKeyedMap)
    var JSONKeyedMap = /** @class */ (function (_super) {
        __extends(JSONKeyedMap, _super);
        function JSONKeyedMap() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        JSONKeyedMap.prototype.stringifyKey = function (k) {
            return JSON.stringify(k);
        };
        JSONKeyedMap.prototype.parseKey = function (k) {
            return JSON.parse(k);
        };
        // Some boilerplate, but it seems unlikely that separating out the repeated
        // parts would actually make the code shorter.
        JSONKeyedMap.prototype.shallowClone = function () {
            return new JSONKeyedMap(this.entries());
        };
        JSONKeyedMap.prototype.typeName = function () {
            return "JSONKeyedMap";
        };
        // Note for toJSONValue (inherited) and fromJSONValue: EJSON conversion of
        // keys is assumed to have no effect, but values may need conversion.
        JSONKeyedMap.fromJSONValue = function (json) {
            var m = new JSONKeyedMap();
            m.obj = EJSON.fromJSONValue(json);
            return m;
        };
        return JSONKeyedMap;
    }(EJSONKeyedMap));
    Objsheets.JSONKeyedMap = JSONKeyedMap;
    EJSON.addType("JSONKeyedMap", JSONKeyedMap.fromJSONValue);
    // FIXME: Keys are key order sensitive (see EJSONKeyedMap)
    var EJSONSet = /** @class */ (function () {
        function EJSONSet(els) {
            if (els === void 0) { els = []; }
            this.map = this.createMap();
            for (var _i = 0, els_1 = els; _i < els_1.length; _i++) {
                var x = els_1[_i];
                this.add(x);
            }
        }
        EJSONSet.prototype.createMap = function () {
            return new EJSONKeyedMap();
        };
        EJSONSet.prototype.has = function (x) {
            return !!this.map.get(x);
        };
        EJSONSet.prototype.hasAll = function (s) {
            var _this = this;
            return forall(s.elements(), function (x) { return _this.has(x); });
        };
        EJSONSet.prototype.add = function (x) {
            this.map.set(x, true);
        };
        EJSONSet.prototype["delete"] = function (x) {
            this.map["delete"](x);
        };
        EJSONSet.prototype.size = function () {
            return this.map.size();
        };
        EJSONSet.prototype.elements = function () {
            return this.map.keys();
        };
        EJSONSet.prototype.shallowClone = function () {
            return new EJSONSet(this.elements());
        };
        EJSONSet.prototype.typeName = function () {
            return "EJSONSet";
        };
        EJSONSet.prototype.toJSONValue = function () {
            return this.map.toJSONValue();
        };
        EJSONSet.fromJSONValue = function (json) {
            var s = new EJSONSet();
            s.map = EJSONKeyedMap.fromJSONValue(json);
            return s;
        };
        return EJSONSet;
    }());
    Objsheets.EJSONSet = EJSONSet;
    EJSON.addType("EJSONSet", EJSONSet.fromJSONValue);
    // See comments on JSONKeyedMap about purpose and assumptions.
    //
    // FIXME: Keys are key order sensitive (see EJSONKeyedMap)
    var JSONSet = /** @class */ (function (_super) {
        __extends(JSONSet, _super);
        function JSONSet() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        JSONSet.prototype.createMap = function () {
            return new JSONKeyedMap();
        };
        JSONSet.prototype.shallowClone = function () {
            return new JSONSet(this.elements());
        };
        JSONSet.prototype.typeName = function () {
            return "JSONSet";
        };
        JSONSet.fromJSONValue = function (json) {
            var s = new JSONSet();
            s.map = JSONKeyedMap.fromJSONValue(json);
            return s;
        };
        return JSONSet;
    }(EJSONSet));
    Objsheets.JSONSet = JSONSet;
    EJSON.addType("JSONSet", JSONSet.fromJSONValue);
    var Tree = /** @class */ (function () {
        function Tree(root, subtrees) {
            if (subtrees === void 0) { subtrees = []; }
            this.root = root;
            this.subtrees = subtrees;
        }
        Tree.prototype.size = function () {
            return this.subtrees.reduce(function (x, y) { return x + y.size(); }, 1);
        };
        Tree.prototype.forEach = function (op) {
            op(this.root);
            this.subtrees.forEach(function (s) { return s.forEach(op); });
        };
        /**
         * applies op to the root of each subtree
         */
        Tree.prototype.map = function (op) {
            return new Tree(op(this.root), this.subtrees.map(function (s) { return s.map(op); }));
        };
        Tree.prototype.filter = function (pred) {
            return pred(this.root)
                ? new Tree(this.root, (this.subtrees.map(function (s) { return s.filter(pred); })).filter(isNotNull))
                : null;
        };
        Tree.prototype.find = function (value) {
            return this.findT(function (n) { return n.root === value; });
        };
        Tree.prototype.findT = function (pred) {
            if (pred(this)) {
                return this;
            }
            else {
                for (var _i = 0, _a = this.subtrees; _i < _a.length; _i++) {
                    var s = _a[_i];
                    var n = s.findT(pred);
                    if (n != null) {
                        return n;
                    }
                }
                return null;
            }
        };
        Tree.prototype.toString = function () {
            if (this.subtrees.length > 0)
                return this.root.toString() + "{" + this.subtrees.map(function (s) { return s.toString(); }).join(", ") + "}";
            else
                return this.root.toString();
        };
        Tree.prototype.typeName = function () {
            return "Tree";
        };
        Tree.prototype.toJSONValue = function () {
            return {
                root: EJSON.toJSONValue(this.root),
                subtrees: this.subtrees.map(function (s) { return s.toJSONValue(); })
            };
        };
        Tree.fromJSONValue = function (json) {
            return new Tree(json.root, json.subtrees.map(function (s) { return Tree.fromJSONValue(s); }));
        };
        return Tree;
    }());
    Objsheets.Tree = Tree;
    EJSON.addType("Tree", Tree.fromJSONValue);
    var Memo = /** @class */ (function () {
        function Memo(recompute) {
            this.recompute = recompute;
            this.clear();
        }
        Memo.prototype.clear = function () {
            this.values = new EJSONKeyedMap();
        };
        Memo.prototype.get = function (k) {
            var v = this.values.get(k);
            if (v == null) {
                v = this.recompute(k);
                this.values.set(k, v);
            }
            return v;
        };
        return Memo;
    }());
    Objsheets.Memo = Memo;
    // helper functions
    function forall(list, pred) {
        for (var _i = 0, list_1 = list; _i < list_1.length; _i++) {
            var x = list_1[_i];
            if (!pred(x)) {
                return false;
            }
        }
        return true;
    }
    Objsheets.forall = forall;
    function exists(list, pred) {
        for (var _i = 0, list_2 = list; _i < list_2.length; _i++) {
            var x = list_2[_i];
            if (pred(x)) {
                return true;
            }
        }
        return false;
    }
    Objsheets.exists = exists;
    function without(list, item) {
        return list.filter(function (x) { return x !== item; });
    }
    Objsheets.without = without;
    // We only call this with two arguments, so let's use the simpler
    // implementation for now. ~ Matt 2016-03-14
    function zip(arr1, arr2) {
        var length = Math.min(arr1.length, arr2.length);
        // TypeScript does not infer the type argument of "map" from the contextual
        // return type.
        // https://github.com/Microsoft/TypeScript/issues/11152
        return _.range(0, length).map(function (i) { return [arr1[i], arr2[i]]; });
    }
    Objsheets.zip = zip;
    function ejsonIndexOf(haystack, needle) {
        return haystack.findIndex(function (it) { return EJSON.equals(it, needle); });
    }
    Objsheets.ejsonIndexOf = ejsonIndexOf;
    function set(x) {
        return new JSONSet(x);
    }
    Objsheets.set = set;
    function tree(root, subtrees) {
        return new Tree(root, subtrees);
    }
    Objsheets.tree = tree;
})(Objsheets || (Objsheets = {}));
