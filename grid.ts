import { QCellId } from "./data";
import { ColumnId, NonRootColumnId } from "./schema";
import { ViewValue } from "./sheet";
import { assert } from "./assert";
import _ from "underscore";

// Grid utilities

export class ViewCell {
  public qCellId: null | QCellId;
  public columnId: null | ColumnId;
  public kind: null | string;
  public fullText: null | string;
  public isObjectHeader: null | boolean;
  public isObjectCell: null | boolean;
  // This field means two things: the nearest ancestor that the cell should be
  // highlighted as belonging to (if it doesn't have its own qCellId) and the
  // nearest existing ancestor to which data would be added in the absence of
  // the "add to object above" feature (if addColumnId is not set).  At some
  // point, we may need to make two fields, but for now, using
  // ancestorQCellIdForAdd seems to be good enough and saves us some churn. :/
  // ~ Matt 2016-10-05
  public ancestorQCellId: null | QCellId;
  // Overrides ancestorQCellId for data addition in the case of "add to object
  // above".
  public ancestorQCellIdForAdd: null | QCellId;
  // If set to a state column, then data can be added to this cell, and
  // any missing ancestors will be automatically created up to
  // ancestorQCellId.
  public addColumnId: null | NonRootColumnId;
  // For data cells in spare columns: set to column id where the new column
  // should be added as a child.  Notice that only one of addColumnId,
  // parentColumnId may be non-null on a given cell.
  public parentColumnId: null | NonRootColumnId;

  // Extracted from value (if it's a CellReference) in ClientView.hotConfig.
  // XXX: We could probably replace these with accessors.
  public display: null | string;
  public referent: null | QCellId;

  // value should be null /only/ for cells that are not rendered because they
  // are covered by merged cells.
  constructor(public value: null | ViewValue = "", public rowspan = 1, public colspan = 1,
    public cssClasses: string[] = []) {
    this.qCellId = null;
    this.columnId = null;
    this.kind = null;
    this.fullText = null;
    this.isObjectHeader = null;
    this.isObjectCell = null;
    this.ancestorQCellId = null;
    this.ancestorQCellIdForAdd = null;
    this.addColumnId = null;
    this.parentColumnId = null;
    this.display = null;
    this.referent = null;
  }
}

export type ViewGrid = ViewCell[][];

// Mutate "orig" by adding "extension" at the bottom.
// This would be a good place to add some assertions...
export function gridVertExtend(orig: ViewGrid, extension: ViewGrid) {
  if (orig.length > 0) assert(extension.every((x) => x.length == orig[0].length));
  for (let row of extension) {
    orig.push(row);
  }
}

// Mutate "orig" by adding "extension" at the right.
export function gridHorizExtend(orig: ViewGrid, extension: ViewGrid) {
  for (let i = 0; i < orig.length; i++) {
    for (let cell of extension[i]) {
      orig[i].push(cell);
    }
  }
}

export function gridVertStretch(orig: ViewGrid, height: number) {
  assert(0 < orig.length && orig.length <= height);
  if (orig.length < height) {
    let width = orig[0].length;
    let deltaHeight = height - orig.length;
    gridBottomRow(orig).forEach((cell) => cell.rowspan += deltaHeight);
    for (let i = orig.length; i < height; i++) {
      orig.push(_.range(0, width).map(() => new ViewCell(null)));
    }
  }
}

// Return a grid consisting of one "height x width" merged cell and enough dummy
// 1x1 cells.  You can mutate the upper-left cell as desired.
export function gridMergedCell(height: number, width: number, value: ViewValue = "", cssClasses: string[] = []) {
  // assert that extension has compatible width
  let grid = _.range(0, height).map((i) => _.range(0, width).map((j) => new ViewCell(null)));
  if (width > 0) {
    grid[0][0].rowspan = height;
    grid[0][0].colspan = width;
    grid[0][0].value = value;
    grid[0][0].cssClasses = cssClasses;
  }
  return grid;
}

export function gridRepeatedCell(height: number, width: number, value = "",
  cssClasses: string[] = [], attrs: {} = {}): ViewGrid {
  return _.range(0, height).map((i) => _.range(0, width).map((j) =>
    _.extend(new ViewCell(value, 1, 1, cssClasses), attrs)));
}

export function gridGetCell(grid: ViewGrid, row: number, col: number) {
  for (let i = 0; i <= row; i++) {
    for (let j = 0; j <= col; j++) {
      let cell = grid[i][j];
      if (i + cell.rowspan > row && j + cell.colspan > col) {
        return cell;
      }
    }
  }
  throw new Error(`cell (${row},${col}) does not exist in grid`);
}

export function gridBottomRowWithColNums(grid: ViewGrid) {
  let _results: [ViewCell, number][];
  if (grid.length === 0) {
    return [];
  } else {
    let i = grid.length - 1;
    let j = 0;
    _results = [];
    while (j < grid[i].length) {
      let cell = gridGetCell(grid, i, j);
      _results.push([cell, j]);
      j += cell.colspan;
    }
    return _results;
  }
}

export function gridBottomRow(grid: ViewGrid) {
  return gridBottomRowWithColNums(grid).map(([cell, j]) => cell);
}

